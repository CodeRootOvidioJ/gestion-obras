<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <title>Sistema de Inventario</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Compiled and minified CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css">
        <!--<link rel="stylesheet" href="<c:url value="/public/css/materialize.min.css" />"> -->
        <link href="<c:url value="/public/css/main.css" />" rel="stylesheet" >

         <!--Import Google Icon Font-->
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      
    </head>
    <body>
    
    
    <nav class="nav-extended">
    <div class="nav-wrapper  #1e88e5 blue darken-3">
        <a href="#" class="brand-logo">Solusystec</a>
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="https://ull-esit-sytw-1617.github.io/presentaciones-todos/">Usuario</a></li>
            <li><a href="http://alu0100767421.github.io/">Cerrar Sesion</a></li>
        </ul>
        <!-- Este es el menú que se desplega para los moviles-->
        <ul class="side-nav" id="mobile-demo">
           
            <li><a href="https://ull-esit-sytw-1617.github.io/presentaciones-todos/">Usuario</a></li>
            <li><a href="http://alu0100767421.github.io/">Cerrar Sesion</a></li>
            <li><a href="irCompras.html">compras</a></li>
            <li><a href="http://alu0100767421.github.io/">Cerrar Sesion</a></li>
            <li><a href="https://ull-esit-sytw-1617.github.io/presentaciones-todos/">Usuario</a></li>
            <li><a href="http://alu0100767421.github.io/">Cerrar Sesion</a></li>
        </ul>
        <!-- Mi menú de abajo -->
      
    </div>
</nav>

  <nav>
    <div class="nav-wrapper  #000000 black ">
      <a href="#" class="brand-logo"></a>
      <ul id="nav-mobile" class="center hide-on-med-and-down">
     <li class="tab"><a href="irHola.html">Menu</a></li>
            <li class="tab"><a href="irCompras.html">Compras</a></li>
              <li class="tab"><a href="irCompras.html">Ventas</a></li>
            <li class="tab"><a href="/compras.jsp">Clientes</a></li>
            <li class="tab"><a href="irProductos.html">Productos</a></li>
      </ul>
    </div>
  </nav>
        
    
        
