<%@include file="/WEB-INF/include/header.jsp"%>




<a href="irForm.html"
	class="btn-floating btn-large waves-effect waves-light red"><i
	class="material-icons">add</i></a>


<div class="container">
	<div class="row">

		<h1 class="clase1" align="center">Control de Compras</h1>


		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>COMPRA</th>
					<th>producto</th>
					<th>proveedor</th>
					<th>cantidad</th>
					<th>precio</th>
					<th>Fecha de compra</th>
					<th>Modificar</th>
					<th>Eliminar</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lstcompras}" var="lstcompras">
					<tr>
						<td><c:out value="${lstcompras.compraid}" /></td>
						<td><c:out value="${lstcompras.productoid}" /></td>
						<td><c:out value="${lstcompras.proveedorid}" /></td>
						<td><c:out value="${lstcompras.cantidad}" /></td>
						<td><c:out value="${lstcompras.precio}" /></td>

						<td><c:out value="${lstcompras.fechadecompra}" /></td>



						<td><a
							href="<c:out value="irForm2.html?id=${lstcompras.compraid}"></c:out>"><span
								class="card-title black"><c:out value="Editar"></c:out></span></a></td>

						<td><a
							href="<c:out value="irEliminarCompras.html?id=${lstcompras.compraid}" />"><span
								class="glyphicon glyphicon-trash" aria-hidden="true"> <c:out
										value="Eliminar"></c:out></span></a></td>










					</tr>

				</c:forEach>
				<label>Browser Select</label>
				<select class="browser-default">
					<option value="lstcompras" disabled selected>Seleccione</option>
					<c:forEach items="${lstcompras}" var="lstcompras">
						<option value="precio"><c:out
								value="${lstcompras.precio}" /></option>
					</c:forEach>
				</select>


			</tbody>
		</table>
	</div>

</div>
<%@include file="/WEB-INF/include/footer.jsp"%>
