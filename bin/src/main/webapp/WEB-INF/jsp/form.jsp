<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="row ">
	<div class="row">

		<h1 class="color #f1f8e9 light-green lighten-5 col s6 offset-s3"
			th:text="${titulo}" align="center"></h1>
	</div>

	<div class="container">
		<ol class="breadcrumb">

			<li class="active">Agregar</li>
		</ol>
		<div class="panel panel-primary">
			<div class="panel-heading">Formulario</div>
			<div class="panel-body">

				<form:form method="post" commandName="compras">
					<h1>Complete el formulario</h1>

					<form:errors path="*" element="div" cssClass="alert alert-danger" />
					<div class="input-field">
						<i class="material-icons prefix white-text">perm_identity</i>
						<form:input path="productoid" cssClass="validate black-text"
							required="true" />
						<form:label path="productoid">producto:</form:label>
					</div>


					<div class="input-field">
						<i class="material-icons prefix white-text">lock_outline</i>
						<form:label path="compraid">compra: </form:label>
						<form:input path="compraid" cssClass="validate black-text"
							required="true" />
					</div>



					<div class="input-field">
						<i class="material-icons prefix white-text">lock_outline</i>
						<form:label path="cantidad">cantidad: </form:label>
						<form:input path="cantidad" cssClass="validate black-text"
							required="true" />
					</div>


					<form:label path="precio">Browser Select</form:label>
					<form:select path="precio" class="browser-default">
						<c:forEach items="${lstcompras}" var="lstcompras">

							<form:option path="precio" value="${lstcompras.precio}"></form:option>

						</c:forEach>
					</form:select>


					<div class="row">

						<div class="col s12 m6">
							<input type="submit" value="Enviar" class="btn btn-danger green" />
						</div>
					</div>





				</form:form>






			</div>
		</div>
	</div>
</div><%@include file="/WEB-INF/include/footer.jsp"%>
<script type="text/javascript"
	src="<c:url value="/public/inicializadores/init.js" />"></script>