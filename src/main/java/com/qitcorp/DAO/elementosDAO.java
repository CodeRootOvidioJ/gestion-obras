package com.qitcorp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.elementosModel;
import com.qitcorp.model.subRubrosModel;

@Repository
public class elementosDAO {

	@Autowired
	private DataSource dataSource;

	public List<subRubrosModel> obtenerSubRubros() {
		List<subRubrosModel> list = new ArrayList<subRubrosModel>();
		subRubrosModel objModel = new subRubrosModel();
		String query = "select sub_rubro_id, codigo, descripcion, rubro_id, desplegable from sub_Rubro";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {

			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new subRubrosModel();
				objModel.setSub_rubro_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2).trim());
				objModel.setDescripcion(rs.getString(3).trim());
				objModel.setRubro_id(rs.getInt(4));
				// objModel.setCuentacontable(rs.getString(2));
				// objModel.setTipodetalle(rs.getString(1));
				objModel.setDesplegable(rs.getString(5).trim());

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public List<elementosModel> obtenerElementos() {
		List<elementosModel> list = new ArrayList<elementosModel>();
		elementosModel objModel = new elementosModel();
		String query = "select codigo, descripcion, sub_rubro_id, unidad, costo1 from elementos";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new elementosModel();
				objModel.setCodigo(rs.getString(1).trim());
				objModel.setDescripcion(rs.getString(2).trim());
				objModel.setSubrubro(rs.getInt(3));
				objModel.setUnidad(rs.getString(4).trim());
				objModel.setCosto(rs.getString(5).trim());

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertElementos(elementosModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra DAO");

		try {
			con = dataSource.getConnection();
			String query = "insert into elementos (codigo, descripcion, sub_rubro_id, unidad )values (?,?, ?, ? )";
			System.out.println("query:" + query);

			cmd = con.prepareStatement(query);

			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());
			cmd.setInt(3, objModel.getSubrubro());
			// cmd.setString(4, objModel.getCuentacontable());
			// cmd.setString(5, objModel.getTipodetalle());
			cmd.setString(4, objModel.getUnidad().trim());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public subRubrosModel obtenersubrubro(int id) {

		subRubrosModel objModel = new subRubrosModel();
		String query = "select codigo, descripcion, sub_rubro_id, cuentaContable, tipo detalle, desplegable from Sub_Rubro where sub_rubro_id = "
				+ id;

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new subRubrosModel();
				objModel.setCodigo(rs.getString(1).trim());
				objModel.setDescripcion(rs.getString(2).trim());
				objModel.setRubro_id(rs.getInt(3));
				objModel.setCuentacontable(rs.getString(4).trim());
				objModel.setTipodetalle(rs.getString(5).trim());
				objModel.setDesplegable(rs.getString(6).trim());

				// list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return objModel;

	}

	public ArrayList<elementosModel> obtenerElementosId(int id) {
		ArrayList<elementosModel> list = new ArrayList<elementosModel>();

		elementosModel objModel = new elementosModel();
		String query = "select a.elemento_id, a.codigo, a.descripcion, a.sub_rubro_id, a.unidad, a.costo1, b.descripcion nombre_subrubro from elementos a , sub_rubro b where a.sub_rubro_id= b.sub_rubro_id and  b.sub_rubro_id = " + id;

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new elementosModel();
				objModel.setElemento_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2).trim());
				objModel.setDescripcion(rs.getString(3).trim());
				objModel.setSubrubro(rs.getInt(4));
				objModel.setUnidad(rs.getString(5).trim());
				objModel.setCosto(rs.getString(6));
				objModel.setNombre_subrubro(rs.getString(7).trim());

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	// metodo modificar
	public boolean modificaElementos(elementosModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE elementos SET codigo= ?, descripcion = ?, sub_rubro_id = ?  WHERE elemento_id = ?";
			System.out.println("query:" + query);
			cmd = con.prepareStatement(query);
			Logger.getLogger(elementosDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());
			cmd.setInt(3, objModel.getSubrubro());
			cmd.setInt(4, objModel.getElemento_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean eliminarElemento(int idrubro) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			String query = "DELETE FROM elementos WHERE elemento_id = ?";
			System.out.println("query eliminar:" + query);
			ps = con.prepareStatement(query);

			ps.setInt(1, idrubro);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(elementosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}

}
