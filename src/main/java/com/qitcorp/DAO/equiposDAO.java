package com.qitcorp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.categoriaPersonalModel;
import com.qitcorp.model.equiposModel;
import com.qitcorp.model.tiposObrasModel;
import com.qitcorp.model.zonasModel;

@Repository
public class equiposDAO {

	@Autowired
	private DataSource dataSource;

	public List<equiposModel> obtenerEquipos() {
		List<equiposModel> list = new ArrayList<equiposModel>();
		equiposModel objModel = new equiposModel();
		String query = "select a.equipos_id, a.codigo, a.nombre, a.categorias_equipos_id, a.unidad, a.t1, a.pgr1, a.t2, a.pgr2, a.t3, a.pgr3, b.descripcion from equipos a , Categorias_Equipos b where a.categorias_equipos_id = b.categorias_equipos_id";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new equiposModel();
				objModel.setEquipos_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2).trim());
				objModel.setNombre(rs.getString(3).trim());
				objModel.setCategorias_equipos_id(rs.getInt(4));
				objModel.setUnidad(rs.getString(5).trim());
				objModel.setT1(rs.getDouble(6));
				objModel.setPgr1(rs.getDouble(7));
				objModel.setT2(rs.getDouble(8));
				objModel.setPgr2(rs.getDouble(9));
				objModel.setT3(rs.getDouble(10));
				objModel.setPgr3(rs.getDouble(11));
				objModel.setNombrecategoria(rs.getString(12).trim());

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(equiposDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(equiposDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(equiposDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(equiposDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertEquipos(equiposModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra DAO");

		try {
			con = dataSource.getConnection();
			String query = "insert into equipos ( codigo, nombre, categorias_equipos_id, unidad, t1, pgr1, t2, pgr2, t3, pgr3)values (?,?,?,?,?,?,?,?,?,?)";
			System.out.println("query:" + query);

			System.out.println("codigo:" + objModel.getCategorias_equipos_id());

			cmd = con.prepareStatement(query);
			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getNombre().trim());
			cmd.setInt(3, objModel.getCategorias_equipos_id());
			cmd.setString(4, objModel.getUnidad().trim());
			cmd.setDouble(5, objModel.getT1());
			cmd.setDouble(6, objModel.getPgr1());
			cmd.setDouble(7, objModel.getT2());
			cmd.setDouble(8, objModel.getPgr2());
			cmd.setDouble(9, objModel.getT3());
			cmd.setDouble(10, objModel.getPgr3());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(equiposDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(equiposDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean modificarEquipos(equiposModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE equipos SET codigo = ?, nombre = ?, categorias_equipos_id = ?, unidad = ?, t1=? , pgr1=? , t2=?, pgr2=?, t3=?, pgr3=? WHERE equipos_id = ?";
			System.out.println("query:" + query);
			cmd = con.prepareStatement(query);
			Logger.getLogger(zonasDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getNombre().trim());
			cmd.setInt(3, objModel.getCategorias_equipos_id());
			cmd.setString(4, objModel.getUnidad().trim());
			cmd.setDouble(5, objModel.getT1());
			cmd.setDouble(6, objModel.getPgr1());
			cmd.setDouble(7, objModel.getT2());
			cmd.setDouble(8, objModel.getPgr2());
			cmd.setDouble(9, objModel.getT3());
			cmd.setDouble(10, objModel.getPgr3());

			cmd.setInt(11, objModel.getEquipos_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean eliminarEquipos(int idzona) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {

			String query = ("delete  from equipos where equipos_id = ? ");
			System.out.println("query:" + idzona);
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);

			ps.setInt(1, idzona);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}

}
