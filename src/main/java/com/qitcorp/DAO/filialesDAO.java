package com.qitcorp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.filialesModel;
import com.qitcorp.model.zonasModel;

@Repository
public class filialesDAO {

	@Autowired
	private DataSource dataSource;

	public List<filialesModel> obtenerFiliales() {
		List<filialesModel> list = new ArrayList<filialesModel>();
		filialesModel objModel = new filialesModel();
		String query = "select codigo, descripcion, moneda, zonas_id from filiales";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new filialesModel();
				objModel.setCodigo(rs.getString(1));
				objModel.setDescripcion(rs.getString(2));
				objModel.setMoneda(rs.getString(3));
				objModel.setZonas_id(rs.getInt(4));

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(filialesDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(filialesDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(filialesDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(filialesDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertfiliales(filialesModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra DAO");

		try {
			con = dataSource.getConnection();
String query = "insert into filiales (codigo, descripcion, moneda, zonas_id )values (?,?,?,?)";
System.out.println("query:" + query);

System.out.println("codigo:" + objModel.getCodigo());

System.out.println("moneda:" + objModel.getMoneda());
System.out.println("Zona:" + objModel.getZonas_id());
			cmd = con.prepareStatement( query);

			cmd.setString(1, objModel.getCodigo());
			cmd.setString(2, objModel.getDescripcion());
			cmd.setString(3, objModel.getMoneda());
			cmd.setInt(4, objModel.getZonas_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(filialesDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(filialesDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	
	
	// metodo modificar
			public boolean modificarfiliales(filialesModel objModel) {
				boolean result = false;
				Connection con = null;
				PreparedStatement cmd = null;
				try {
					con = dataSource.getConnection();
					String query = "UPDATE filiales SET codigo= ?, descripcion = ? , moneda=?, zonas_id=? WHERE filiales_id = ?";
					System.out.println("query:" + query);
					cmd = con.prepareStatement(query);
					Logger.getLogger(zonasDAO.class.getName()).log(Level.INFO, "update  " + query);
					cmd.setString(1, objModel.getCodigo());
					cmd.setString(2, objModel.getDescripcion());
					cmd.setString(3, objModel.getMoneda());
					cmd.setInt(4, objModel.getZonas_id());
					cmd.setInt(5, objModel.getFiliales_id());

					if (cmd.executeUpdate() > 0) {
						result = true;
					}
				} catch (Exception e) {
					Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
				} finally {
					try {
						if (cmd != null)
							cmd.close();
						if (con != null)
							con.close();
					} catch (Exception e) {
						Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
					}
				}
				return result;
			}
			
			
			public boolean eliminarfiliales(int idzona) {
				boolean result = false;
				PreparedStatement ps = null;
				Connection con = null;
				try {

					String query = ("delete  from filiales where filiales_id = ? ");
					System.out.println("id_zona:" + idzona);
					con = dataSource.getConnection();
					ps = con.prepareStatement(query);

					ps.setInt(1, idzona);
					if (ps.executeUpdate() > 0) {
						result = true;
					}
				} catch (Exception e) {
					Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
				} finally {
					try {
						if (ps != null)
							ps.close();
						if (con != null)
							con.close();
					} catch (Exception e) {
						Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
					}
				}

				return result;
			}

			
}
