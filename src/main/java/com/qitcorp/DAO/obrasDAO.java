package com.qitcorp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.categoriaEquipoModel;
import com.qitcorp.model.categoriaPersonalModel;
import com.qitcorp.model.estadoObraModel;
import com.qitcorp.model.obrasModel;
import com.qitcorp.model.obrasPrevisionModel;
import com.qitcorp.model.siProductosModel;
import com.qitcorp.model.subRubrosModel;
import com.qitcorp.model.zonasModel;

@Repository
public class obrasDAO {

	@Autowired
	private DataSource dataSource;

	public List<obrasModel> obtenerEstadoObras() {
		List<obrasModel> list = new ArrayList<obrasModel>();
		obrasModel objModel = new obrasModel();
		String query = "select obras_id, codigo,Denominacion,Nombre_corto,Direccion,Coordenadas,estadosdeobras_id,Filiales_id,Ingeniero,Director,tiposdeobra_id,tiposdecontrato_id,tiposdefinanciamiento_id,tiposdemercado_id,tiposdecliente_id,Inicio , fin  ,Duracion,turnosdetrabajo_id,Finalidad,Tipo_terreno,Dificultades_tecnicas,Nombre_cliente,NIT,Direcion_cliente,Telefonos,Contacto_tecnico,Contacto_cobro,Anticipo,Dia_corte,Dias_credito  from Obras";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new obrasModel();
				objModel.setObras_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2).trim());
				objModel.setDenominacion(rs.getString(3).trim());
				objModel.setNombre_corto(rs.getString(4).trim());
				objModel.setDireccion(rs.getString(5).trim());
				objModel.setCoordenadas(rs.getString(6).trim());
				objModel.setEstadosdeobras_id(rs.getInt(7));
				objModel.setFiliales_id(rs.getInt(8));
				objModel.setIngeniero(rs.getString(9).trim());
				objModel.setDirector(rs.getString(10).trim());
				objModel.setTiposdeobra_id(rs.getInt(11));
				objModel.setTiposdecontrato_id(rs.getInt(12));
				objModel.setTiposdefinanciamiento_id(rs.getInt(13));
				objModel.setTiposdemercado_id(rs.getInt(14));
				objModel.setTiposdecliente_id(rs.getInt(15));

				objModel.setInicio(rs.getString(16));
				objModel.setFin(rs.getString(17));
				objModel.setDuracion(rs.getInt(18));
				objModel.setTurnosdetrabajo_id(rs.getInt(19));

				objModel.setFinalidad(rs.getString(20).trim());
				objModel.setTipo_terreno(rs.getString(21).trim());
				objModel.setDificultades_tecnicas(rs.getString(22).trim());
				objModel.setNombre_cliente(rs.getString(23).trim());
				objModel.setNit(rs.getString(24).trim());
				objModel.setDirecion_cliente(rs.getString(25).trim());
				objModel.setTelefonos(rs.getString(26).trim());
				objModel.setContacto_tecnico(rs.getString(27).trim());
				objModel.setContacto_cobro(rs.getString(28).trim());
				objModel.setAnticipo(rs.getDouble(29));
				objModel.setDia_corte(rs.getInt(30));
				objModel.setDias_credito(rs.getInt(31));

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(obrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(obrasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(obrasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(obrasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertObras(obrasModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra DAO" + objModel.getInicio());

		try {
			con = dataSource.getConnection();
			String query = "insert into Obras (codigo,Denominacion,Nombre_corto,Direccion,Coordenadas,estadosdeobras_id,Ingeniero,Director,tiposdeobra_id,tiposdecontrato_id,tiposdefinanciamiento_id,tiposdemercado_id,tiposdecliente_id,turnosdetrabajo_id,Filiales_id,Duracion,Finalidad,Tipo_terreno,Dificultades_tecnicas,Nombre_cliente,NIT,Direcion_cliente,Telefonos,Contacto_tecnico,Contacto_cobro,Anticipo,Dia_corte,Dias_credito,inicio, fin  )"
					+ "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,try_convert(date, ?, 103),try_convert(date, ?, 103) )";

			System.out.println("query:" + query);

			cmd = con.prepareStatement(query);

			cmd.setString(1, objModel.getCodigo().trim());

			cmd.setString(2, objModel.getDenominacion().trim());
			cmd.setString(3, objModel.getNombre_corto().trim());
			cmd.setString(4, objModel.getDireccion().trim());
			cmd.setString(5, objModel.getCoordenadas().trim());
			cmd.setInt(6, objModel.getEstadosdeobras_id());

			cmd.setString(7, objModel.getIngeniero().trim());
			cmd.setString(8, objModel.getDirector().trim());
			cmd.setInt(9, objModel.getTiposdeobra_id());
			cmd.setInt(10, objModel.getTiposdecontrato_id());
			cmd.setInt(11, objModel.getTiposdefinanciamiento_id());
			cmd.setInt(12, objModel.getTiposdemercado_id());
			cmd.setInt(13, objModel.getTiposdecliente_id());
			cmd.setInt(14, objModel.getFiliales_id());

			cmd.setInt(15, objModel.getDuracion());
			cmd.setInt(16, objModel.getTurnosdetrabajo_id());
			cmd.setString(17, objModel.getFinalidad().trim());
			cmd.setString(18, objModel.getTipo_terreno().trim());
			cmd.setString(19, objModel.getDificultades_tecnicas().trim());
			cmd.setString(20, objModel.getNombre_cliente().trim());
			cmd.setString(21, objModel.getNit().trim());
			cmd.setString(22, objModel.getDirecion_cliente().trim());
			cmd.setString(23, objModel.getTelefonos().trim());
			cmd.setString(24, objModel.getContacto_tecnico().trim());

			cmd.setString(25, objModel.getContacto_cobro().trim());
			cmd.setDouble(26, objModel.getAnticipo());
			cmd.setInt(27, objModel.getDia_corte());
			cmd.setInt(28, objModel.getDias_credito());
			cmd.setString(29, objModel.getInicio());
			cmd.setString(30, objModel.getFin());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(obrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(obrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean modificarEstadoObra(obrasModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE Obras SET codigo= ?, denominacion = ? " + "WHERE Obras_id = ?";
			System.out.println("query:" + query);
			cmd = con.prepareStatement(query);
			Logger.getLogger(zonasDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDenominacion().trim());
			cmd.setInt(3, objModel.getObras_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean eliminarObra(int idEliminar) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {

			String query = ("delete  from Obras where Obras_id = ? ");
			System.out.println("query:" + idEliminar);
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);

			ps.setInt(1, idEliminar);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}

	public obrasModel obtenerObrasId(int id) {

		obrasModel objModel = new obrasModel();

		String query = "select obras_id, codigo,Denominacion,Nombre_corto,Direccion,Coordenadas,estadosdeobras_id,Filiales_id,Ingeniero,Director,tiposdeobra_id,tiposdecontrato_id,tiposdefinanciamiento_id,tiposdemercado_id,tiposdecliente_id,Inicio , fin  ,Duracion,turnosdetrabajo_id,Finalidad,Tipo_terreno,Dificultades_tecnicas,Nombre_cliente,NIT,Direcion_cliente,Telefonos,Contacto_tecnico,Contacto_cobro,"
				+ "Anticipo,Dia_corte,Dias_credito  from Obras where obras_id = " + id;

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new obrasModel();
				objModel.setObras_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2).trim());
				objModel.setDenominacion(rs.getString(3).trim());
				objModel.setNombre_corto(rs.getString(4).trim());
				objModel.setDireccion(rs.getString(5).trim());
				objModel.setCoordenadas(rs.getString(6).trim());
				objModel.setEstadosdeobras_id(rs.getInt(7));
				objModel.setFiliales_id(rs.getInt(8));
				objModel.setIngeniero(rs.getString(9).trim());
				objModel.setDirector(rs.getString(10).trim());
				objModel.setTiposdeobra_id(rs.getInt(11));
				objModel.setTiposdecontrato_id(rs.getInt(12));
				objModel.setTiposdefinanciamiento_id(rs.getInt(13));
				objModel.setTiposdemercado_id(rs.getInt(14));
				objModel.setTiposdecliente_id(rs.getInt(15));

				objModel.setInicio(rs.getString(16));
				objModel.setFin(rs.getString(17));
				objModel.setDuracion(rs.getInt(18));
				objModel.setTurnosdetrabajo_id(rs.getInt(19));

				objModel.setFinalidad(rs.getString(20).trim());
				objModel.setTipo_terreno(rs.getString(21).trim());
				objModel.setDificultades_tecnicas(rs.getString(22).trim());
				objModel.setNombre_cliente(rs.getString(23).trim());
				objModel.setNit(rs.getString(24).trim());
				objModel.setDirecion_cliente(rs.getString(25).trim());
				objModel.setTelefonos(rs.getString(26).trim());
				objModel.setContacto_tecnico(rs.getString(27).trim());
				objModel.setContacto_cobro(rs.getString(28).trim());
				objModel.setAnticipo(rs.getDouble(29));
				objModel.setDia_corte(rs.getInt(30));
				objModel.setDias_credito(rs.getInt(31));

				// list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return objModel;

	}

	public List<obrasModel> obtenerEstadoObrasId(int id) {
		List<obrasModel> list = new ArrayList<obrasModel>();
		obrasModel objModel = new obrasModel();
		String query = "select obras_id, codigo,Denominacion,Nombre_corto,Direccion,Coordenadas,estadosdeobras_id,Filiales_id,Ingeniero,Director,tiposdeobra_id,tiposdecontrato_id,tiposdefinanciamiento_id,tiposdemercado_id,tiposdecliente_id,Inicio , fin  ,Duracion,turnosdetrabajo_id,Finalidad,Tipo_terreno,Dificultades_tecnicas,Nombre_cliente,NIT,Direcion_cliente,Telefonos,Contacto_tecnico,Contacto_cobro,Anticipo,Dia_corte,Dias_credito  from Obras where obras_id = "
				+ id;

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new obrasModel();
				objModel.setObras_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2).trim());
				objModel.setDenominacion(rs.getString(3).trim());
				objModel.setNombre_corto(rs.getString(4).trim());
				objModel.setDireccion(rs.getString(5).trim());
				objModel.setCoordenadas(rs.getString(6).trim());
				objModel.setEstadosdeobras_id(rs.getInt(7));
				objModel.setFiliales_id(rs.getInt(8));
				objModel.setIngeniero(rs.getString(9).trim());
				objModel.setDirector(rs.getString(10).trim());
				objModel.setTiposdeobra_id(rs.getInt(11));
				objModel.setTiposdecontrato_id(rs.getInt(12));
				objModel.setTiposdefinanciamiento_id(rs.getInt(13));
				objModel.setTiposdemercado_id(rs.getInt(14));
				objModel.setTiposdecliente_id(rs.getInt(15));

				objModel.setInicio(rs.getString(16));
				objModel.setFin(rs.getString(17));
				objModel.setDuracion(rs.getInt(18));
				objModel.setTurnosdetrabajo_id(rs.getInt(19));

				objModel.setFinalidad(rs.getString(20).trim());
				objModel.setTipo_terreno(rs.getString(21).trim());
				objModel.setDificultades_tecnicas(rs.getString(22).trim());
				objModel.setNombre_cliente(rs.getString(23).trim());
				objModel.setNit(rs.getString(24).trim());
				objModel.setDirecion_cliente(rs.getString(25).trim());
				objModel.setTelefonos(rs.getString(26).trim());
				objModel.setContacto_tecnico(rs.getString(27).trim());
				objModel.setContacto_cobro(rs.getString(28).trim());
				objModel.setAnticipo(rs.getDouble(29));
				objModel.setDia_corte(rs.getInt(30));
				objModel.setDias_credito(rs.getInt(31));

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(obrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(obrasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(obrasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(obrasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public ArrayList<obrasPrevisionModel> obtenerSubRubrosId(int id) {
		ArrayList<obrasPrevisionModel> list = new ArrayList<obrasPrevisionModel>();
		obrasPrevisionModel objModel = new obrasPrevisionModel();
	
		String query = "select a.obras_prevision_id,a.codigo,   a. periodos_id, a.rubro_id, a.sub_rubro_id,a.elemento_id, a.obras_id, a.descripcion, a.medida,a.Costo_sugerido,a.Unidades,a.Costo , b.codigo, c.inicio, d.codigo from obras_prevision a , rubro b, periodos c, obras d where a.rubro_id= b.rubro_id and a.periodos_id= c.periodos_id  and a.obras_id = d.obras_id  and a.obras_id = " + id;
		
		
		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new obrasPrevisionModel();
				objModel.setObrasprevision_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2));
				objModel.setPeriodos_id(rs.getInt(3));
				objModel.setRubro_id(rs.getInt(4));
				objModel.setSub_rubro_id(rs.getInt(5));
				objModel.setElemento_id(rs.getInt(6));
				objModel.setObras_id(rs.getInt(7));
				objModel.setDescripcion(rs.getString(8));
				objModel.setMedida(rs.getString(9));
				objModel.setCostosugerido(rs.getDouble(10));
				objModel.setUnidades(rs.getDouble(11));
				objModel.setCosto(rs.getDouble(12));
				objModel.setNombrerubro(rs.getString(13));
				objModel.setNombreperiodo(rs.getString(14));
				objModel.setNombreobra(rs.getString(15));

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	

}
