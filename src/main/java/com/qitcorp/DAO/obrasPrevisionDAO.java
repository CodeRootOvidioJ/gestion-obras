package com.qitcorp.DAO;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.obrasModel;
import com.qitcorp.model.obrasPrevisionModel;
import com.qitcorp.model.semanasModel;
import com.qitcorp.model.subRubrosModel;

@Repository
public class obrasPrevisionDAO {

	@Autowired
	private DataSource dataSource;

	public String isStringHora(String cadena) {
		System.out.println("** Hora recibida parseado = " + cadena);
		String hora = null;
		Date horas = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:");
			hora = (sdf.parse(cadena).toLocaleString());

			System.out.println("** Hora parseado = " + horas);
		} catch (Exception nfe) {
			System.out.println("** Hora parseado = " + horas);
			return hora;

		}
		System.out.println("** Hora parseado = " + horas);
		return hora;
	}

	public List<obrasPrevisionModel> obtenerSemanas() {
		List<obrasPrevisionModel> list = new ArrayList<obrasPrevisionModel>();
		obrasPrevisionModel objModel = new obrasPrevisionModel();
		String query = "select a.obras_prevision_id,a.codigo,   a. periodos_id, a.rubro_id, a.sub_rubro_id,a.elemento_id, a.descripcion, a.medida,a.Costo_sugerido,a.Unidades,a.Costo , b.codigo, c.inicio from obras_prevision a , rubro b, periodos c where a.rubro_id= b.rubro_id and a.periodos_id= c.periodos_id";
		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			con = dataSource.getConnection();
			System.out.println("dts:" + con);
			cmd = con.createStatement();
			rs = cmd.executeQuery(query);
			while (rs.next()) {
				objModel = new obrasPrevisionModel();
				objModel.setObrasprevision_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2));
				objModel.setPeriodos_id(rs.getInt(3));
				objModel.setRubro_id(rs.getInt(4));
				objModel.setSub_rubro_id(rs.getInt(5));
				objModel.setElemento_id(rs.getInt(6));
				objModel.setDescripcion(rs.getString(7));
				objModel.setMedida(rs.getString(8));
				objModel.setCostosugerido(rs.getDouble(9));
				objModel.setUnidades(rs.getDouble(10));
				objModel.setCosto(rs.getDouble(11));
				objModel.setNombrerubro(rs.getString(12));
				objModel.setNombreperiodo(rs.getString(13));

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(obrasPrevisionDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(obrasPrevisionDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(obrasPrevisionDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(obrasPrevisionDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertSemanas(obrasPrevisionModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra horast" + objModel.getPeriodos_id());

		try {
			con = dataSource.getConnection();
			String query = "insert into obras_prevision (codigo, obras_id,  periodos_id , rubro_id ,sub_rubro_id,elemento_id, descripcion, medida, Costo_sugerido, Unidades,costo )values ( ? ,?,? ,?,?,?,?,?,?,?,?)";
			System.out.println("query:" + query);

			System.out.println("codigo:" + objModel.getCodigo());

			cmd = con.prepareStatement(query);

			cmd.setString(1, objModel.getCodigo());
			cmd.setInt(2, objModel.getObras_id());
			cmd.setInt(3, objModel.getPeriodos_id());
			cmd.setInt(4, objModel.getRubro_id());
			cmd.setInt(5, objModel.getSub_rubro_id());
			cmd.setInt(6, objModel.getElemento_id());
			cmd.setString(7, objModel.getDescripcion());
			cmd.setString(8, objModel.getMedida());
			cmd.setDouble(9, objModel.getCostosugerido());
			cmd.setDouble(10, objModel.getUnidades());
			cmd.setDouble(11, objModel.getCosto());
			

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(obrasPrevisionDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(obrasPrevisionDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean modificarSemanas(obrasPrevisionModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE obras_prevision SET  codigo=?,  periodos_id=? , rubro_id=? ,sub_rubro_id =? , elemento_id=?, descripcion=?, medida=?, Costo_sugerido=?, Unidades=?,costo=?   WHERE obras_prevision_id = ?";
			System.out.println("query:" + query);
			cmd = con.prepareStatement(query);
			Logger.getLogger(zonasDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setString(1, objModel.getCodigo());
			cmd.setInt(2, objModel.getPeriodos_id());
			cmd.setInt(3, objModel.getRubro_id());
			cmd.setInt(4, objModel.getSub_rubro_id());
			cmd.setInt(5, objModel.getElemento_id());
			cmd.setString(6, objModel.getDescripcion());
			cmd.setString(7, objModel.getMedida());
			cmd.setDouble(8, objModel.getCostosugerido());
			cmd.setDouble(9, objModel.getUnidades());
			cmd.setDouble(10, objModel.getCosto());
			cmd.setInt(11, objModel.getObrasprevision_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean eliminarSemanas(int idsemanas) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {

			String query = ("delete  from obras_prevision where obras_prevision_id = ? ");
			System.out.println("query:" + idsemanas);
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);

			ps.setInt(1, idsemanas);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}
	

	public ArrayList<subRubrosModel> obtenerSubRubrosId(int id) {
		ArrayList<subRubrosModel> list = new ArrayList<subRubrosModel>();
		subRubrosModel objModel = new subRubrosModel();
		String query = "select a.sub_rubro_id, a.codigo, a.descripcion, a.rubro_id, a.desplegable, b.descripcion nombrerubro  from sub_Rubro a, rubro b where a.rubro_id = b.rubro_id and a.rubro_id = " + id;

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new subRubrosModel();
				objModel.setSub_rubro_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2).trim());
				objModel.setDescripcion(rs.getString(3).trim());
				objModel.setRubro_id(rs.getInt(4));
				objModel.setDesplegable(rs.getString(5).trim());
				objModel.setNombresubrubro(rs.getString(6).trim());

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}
	
	public obrasPrevisionModel obtenerObrasId(int id) {

		obrasPrevisionModel objModel = new obrasPrevisionModel();

	
		String query = "select a.obras_prevision_id,a.codigo,   a. periodos_id, a.rubro_id, a.sub_rubro_id,a.elemento_id, a.descripcion, a.medida,a.Costo_sugerido,a.Unidades,a.Costo , b.codigo, c.inicio from obras_prevision a , rubro b, periodos c where a.rubro_id= b.rubro_id and a.periodos_id= c.periodos_id   and  obras_prevision_id = " + id;
		
		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new obrasPrevisionModel();
				objModel.setObrasprevision_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2));
				objModel.setPeriodos_id(rs.getInt(3));
				objModel.setRubro_id(rs.getInt(4));
				objModel.setSub_rubro_id(rs.getInt(5));
				objModel.setElemento_id(rs.getInt(6));
				objModel.setDescripcion(rs.getString(7));
				objModel.setMedida(rs.getString(8));
				objModel.setCostosugerido(rs.getDouble(9));
				objModel.setUnidades(rs.getDouble(10));
				objModel.setCosto(rs.getDouble(11));
				objModel.setNombrerubro(rs.getString(12));
				objModel.setNombreperiodo(rs.getString(13));

				// list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return objModel;

	}

}
