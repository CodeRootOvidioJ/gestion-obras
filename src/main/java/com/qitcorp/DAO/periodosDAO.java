package com.qitcorp.DAO;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.categoriaPersonalModel;
import com.qitcorp.model.estadoObraModel;
import com.qitcorp.model.periodosModel;
import com.qitcorp.model.tipoContratoModel;
import com.qitcorp.model.tipoGarantiaModel;
import com.qitcorp.model.tiposFinanciamientoModel;
import com.qitcorp.model.turnosTrabajoModel;
import com.qitcorp.model.zonasModel;

@Repository
public class periodosDAO {

	@Autowired
	private DataSource dataSource;

	public String isStringHora(String cadena) {
		System.out.println("** Hora recibida parseado = " + cadena);
		String hora = null;
		Date horas = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:");
			hora = (sdf.parse(cadena).toLocaleString());

			System.out.println("** Hora parseado = " + horas);
		} catch (Exception nfe) {
			System.out.println("** Hora parseado = " + horas);
			return hora;

		}
		System.out.println("** Hora parseado = " + horas);
		return hora;
	}

	public List<periodosModel> obtenerPeriodos() {
		List<periodosModel> list = new ArrayList<periodosModel>();
		periodosModel objModel = new periodosModel();
		String query = "select periodos_id, codigo, descripcion, nombre_corto, inicio, fin, generales,estado, IIF(estado='1', 'Abierto', 'Cerrado') estado  from periodos";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);
			while (rs.next()) {
				objModel = new periodosModel();
				objModel.setPeriodos_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2));
				objModel.setDescripcion(rs.getString(3));

				objModel.setNombre(rs.getString(4));
				objModel.setFecha_inicio(rs.getString(5));

				objModel.setFecha_fin(rs.getString(6));
				objModel.setGenerales(rs.getString(7));
				objModel.setEstado(rs.getInt(8));
				objModel.setNombreestado(rs.getString(9));

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(periodosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(periodosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(periodosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(periodosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertPeriodos(periodosModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra horast" + objModel.getPeriodos_id());

		try {
			con = dataSource.getConnection();
			String query = "insert into periodos (codigo, descripcion, nombre_corto, inicio, fin, generales, estado )values (?,?, ? , try_convert(date, ?, 103) ,try_convert(date, ?, 103) ,?,? )";
			System.out.println("query:" + query);

			System.out.println("getFecha_fin:" + objModel.getFecha_fin());
			
			System.out.println("getFecha_inicio:" + objModel.getFecha_inicio());

			cmd = con.prepareStatement(query);

			cmd.setString(1, objModel.getCodigo());
			cmd.setString(2, objModel.getDescripcion());

			cmd.setString(3, objModel.getNombre());
			cmd.setString(4, objModel.getFecha_inicio());
			cmd.setString(5, objModel.getFecha_fin());
			cmd.setString(6, objModel.getGenerales());
			cmd.setInt(7, objModel.getEstado());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(periodosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(periodosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean modificarPeriodos(periodosModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE periodos SET codigo= ?, descripcion = ?,nombre_corto =? , inicio= try_convert(date, ?, 103) , fin= try_convert(date, ?, 103), generales=? , estado=?  WHERE periodos_id = ?";
			System.out.println("query:" + query);
			cmd = con.prepareStatement(query);
			Logger.getLogger(zonasDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());
			cmd.setString(3, objModel.getNombre().trim());
			cmd.setString(4, objModel.getFecha_inicio().trim());
			cmd.setString(5, objModel.getFecha_fin().trim());
			cmd.setString(6, objModel.getGenerales().trim());
			cmd.setInt(7, objModel.getEstado());
			cmd.setInt(8, objModel.getPeriodos_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean eliminarPeriodos(int idzona) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {

			String query = ("delete  from periodos where periodos_id = ? ");
			System.out.println("query:" + idzona);
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);

			ps.setInt(1, idzona);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}

}
