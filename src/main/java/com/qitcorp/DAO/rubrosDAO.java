package com.qitcorp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.rubrosModel;

@Repository
public class rubrosDAO {

	@Autowired
	private DataSource dataSource;

	public List<rubrosModel> obtenerRubros() {
		List<rubrosModel> list = new ArrayList<rubrosModel>();
		rubrosModel objModel = new rubrosModel();
		String query = "select rubro_id, codigo, descripcion from Rubro";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new rubrosModel();
				objModel.setRubro_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2).trim());
				objModel.setDescripcion(rs.getString(3).trim());

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertRubros(rubrosModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		try {
			con = dataSource.getConnection();
			String query = "insert into Rubro (codigo, descripcion)values (?,?)";
			System.out.println("query:" + query);

			System.out.println("codigo:" + objModel.getCodigo());

			cmd = con.prepareStatement(query);

			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public List<rubrosModel> obtenerRubrosId(int id) {
		List<rubrosModel> list = new ArrayList<rubrosModel>();
		rubrosModel objModel = new rubrosModel();
		String query = "select codigo, descripcion from Rubro where rubro_id = " + id;

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new rubrosModel();
				objModel.setCodigo(rs.getString(1).trim());
				objModel.setDescripcion(rs.getString(2).trim());

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	// metodo modificar
	public boolean modificaRubros(rubrosModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE Rubro SET codigo= ?, descripcion = ? WHERE rubro_id = ?";
			System.out.println("query:" + query);
			cmd = con.prepareStatement(query);
			Logger.getLogger(rubrosDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());
			cmd.setInt(3, objModel.getRubro_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean eliminarRubro(int idrubro) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			String query = "DELETE FROM Rubro WHERE rubro_id = ?";
			System.out.println("query eliminar:" + query);
			ps = con.prepareStatement(query);

			ps.setInt(1, idrubro);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}
	
	public boolean eliminarSubRubro(int idrubro) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			String query = "DELETE FROM sub_Rubro WHERE rubro_id = ?";
			System.out.println("query eliminar:" + query);
			ps = con.prepareStatement(query);

			ps.setInt(1, idrubro);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}
	
	
	public boolean eliminarElemento(int idrubro) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			String query = "DELETE FROM elementos WHERE sub_rubro_id IN (SELECT sub_rubro_id from sub_rubro where rubro_id = ?)";
			
			System.out.println("query eliminar:" + query);
			ps = con.prepareStatement(query);

			ps.setInt(1, idrubro);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}

}
