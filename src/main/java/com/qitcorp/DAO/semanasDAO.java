package com.qitcorp.DAO;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.semanasModel;

@Repository
public class semanasDAO {

	@Autowired
	private DataSource dataSource;

	public String isStringHora(String cadena) {
		System.out.println("** Hora recibida parseado = " + cadena);
		String hora = null;
		Date horas = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:");
			hora = (sdf.parse(cadena).toLocaleString());

			System.out.println("** Hora parseado = " + horas);
		} catch (Exception nfe) {
			System.out.println("** Hora parseado = " + horas);
			return hora;

		}
		System.out.println("** Hora parseado = " + horas);
		return hora;
	}

	public List<semanasModel> obtenerSemanas() {
		List<semanasModel> list = new ArrayList<semanasModel>();
		semanasModel objModel = new semanasModel();
		String query = "select semanas_id, periodos_id, codigo,  inicio, fin, estado, IIF(estado='1', 'Abierto', 'Cerrado') estado  from semanas";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);
			while (rs.next()) {
				objModel = new semanasModel();
				objModel.setSemanas_id(rs.getInt(1));
				objModel.setPeriodos_id(rs.getInt(2));
				objModel.setCodigo(rs.getString(3));
				objModel.setFecha_inicio(rs.getString(4));

				objModel.setFecha_fin(rs.getString(5));
				objModel.setEstado(rs.getInt(6));
				objModel.setNombreestado(rs.getString(7));

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(semanasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(semanasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(semanasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(semanasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertSemanas(semanasModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra horast" + objModel.getPeriodos_id());

		try {
			con = dataSource.getConnection();
			String query = "insert into semanas (codigo,  inicio, fin, periodos_id, estado )values (?,try_convert(date, ?, 103), try_convert(date, ?, 103) , ? ,? )";
			System.out.println("query:" + query);

			System.out.println("codigo:" + objModel.getCodigo());

			cmd = con.prepareStatement(query);

			cmd.setString(1, objModel.getCodigo());
			cmd.setString(2, objModel.getFecha_inicio());
			cmd.setString(3, objModel.getFecha_fin());
			cmd.setInt(4, objModel.getPeriodos_id());
			cmd.setInt(5, objModel.getEstado());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(semanasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(semanasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean modificarSemanas(semanasModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE semanas SET codigo= ?, periodos_id = ?, inicio= try_convert(date, ?, 103) , fin= try_convert(date, ?, 103) ,  estado=?  WHERE semanas_id = ?";
			System.out.println("query:" + query);
			cmd = con.prepareStatement(query);
			Logger.getLogger(zonasDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setInt(2, objModel.getPeriodos_id());
			cmd.setString(3, objModel.getFecha_inicio().trim());
			cmd.setString(4, objModel.getFecha_fin().trim());
			cmd.setInt(5, objModel.getEstado());
			cmd.setInt(6, objModel.getSemanas_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean eliminarSemanas(int idsemanas) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {

			String query = ("delete  from semanas where semanas_id = ? ");
			System.out.println("query:" + idsemanas);
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);

			ps.setInt(1, idsemanas);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}

}
