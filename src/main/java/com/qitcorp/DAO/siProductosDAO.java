package com.qitcorp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.siProductosModel;
import com.qitcorp.model.zonasModel;

@Repository
public class siProductosDAO {

	@Autowired
	private DataSource dataSource;

	public List<siProductosModel> obtenerproductos() {
		List<siProductosModel> list = new ArrayList<siProductosModel>();
		siProductosModel objModel = new siProductosModel();
		String query = "select product_id,name,brand   ,model  ,quantity,price, category_id, to_char(created_date,'DD/MM/YYYY') created_date,updated_sate,"
				+ " created_by,updated_by from products ";
	
				
		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new siProductosModel();
				objModel.setProductoid(rs.getInt(1));
				objModel.setNombre(rs.getString(2));
				objModel.setMarca(rs.getString(3));
				objModel.setModelo(rs.getString(4));
				objModel.setCantidad(rs.getInt(5));
				objModel.setPrecio(rs.getDouble(6));
				objModel.setCategoraid(rs.getInt(5));
				objModel.setFechacreación(rs.getString(7));
				objModel.setFechaactualiza(rs.getString(8));
				objModel.setCreadopor(rs.getString(9));
				objModel.setActualizadopor(rs.getString(10));

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertProductos(siProductosModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra DAO");

		try {
			con = dataSource.getConnection();

			cmd = con.prepareStatement("insert into products (product_id,name,brand,model,quantity, price, category_id)values (?,?,?,? ,?,?,?  )");
			
			cmd.setInt(1, objModel.getProductoid());
			cmd.setString(2, objModel.getNombre());
			cmd.setString(3, objModel.getMarca());
			cmd.setString(4, objModel.getModelo());
			cmd.setInt(5, objModel.getCantidad());
			cmd.setDouble(6, objModel.getPrecio());
			cmd.setInt(7, objModel.getCategoraid());

			
			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public siProductosModel obtenerProductos(int id) {

		siProductosModel objModel = new siProductosModel();
	
		
		String query = "select product_id,name,brand   ,model  ,quantity,price, category_id, to_char(created_date,'DD/MM/YYYY') created_date,updated_sate,"
				+ " created_by,updated_by from products where product_id = " + id;

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new siProductosModel();
				objModel.setProductoid(rs.getInt(1));
				objModel.setNombre(rs.getString(2));
				objModel.setMarca(rs.getString(3));
				objModel.setModelo(rs.getString(4));
				objModel.setCantidad(rs.getInt(5));
				objModel.setPrecio(rs.getDouble(6));
				objModel.setCategoraid(rs.getInt(5));
				objModel.setFechacreación(rs.getString(7));
				objModel.setFechaactualiza(rs.getString(8));
				objModel.setCreadopor(rs.getString(9));
				objModel.setActualizadopor(rs.getString(10));

				// list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return objModel;

	}

	// metodo modificar
	public boolean modificaTipologiasModal(siProductosModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE PRODUCTS SET product_id= ?, name = ?, brand = ?, model= ? ,quantity = ?, price = ?, category_id = ?  WHERE product_id = ?";
			
		
			cmd = con.prepareStatement(query);
			Logger.getLogger(siProductosDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setInt(1, objModel.getProductoid());
			cmd.setString(2, objModel.getNombre());
			cmd.setString(3, objModel.getMarca());
			cmd.setString(4, objModel.getModelo());
			cmd.setInt(5, objModel.getCantidad());
			cmd.setDouble(6, objModel.getPrecio());
			cmd.setInt(7, objModel.getCategoraid());
			cmd.setInt(8, objModel.getProductoid());
			
			
			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}
	
	public boolean eliminarCompras(int idcompra){
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
			try {
				con = dataSource.getConnection();
				ps = con.prepareStatement("DELETE FROM buys WHERE buy_id = ?");

				ps.setInt(1, idcompra);
				if (ps.executeUpdate() > 0) {
					result = true;
				}
		}catch (Exception e) {
			Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
		}finally {
			try {
				if(ps != null)ps.close();
				if(con != null)con.close();
			}catch (Exception e) {
				Logger.getLogger(siProductosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}


}
