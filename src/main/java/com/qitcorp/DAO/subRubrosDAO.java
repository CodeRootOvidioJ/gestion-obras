package com.qitcorp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.subRubrosModel;

@Repository
public class subRubrosDAO {

	@Autowired
	private DataSource dataSource;

	public List<subRubrosModel> obtenerSubRubros() {
		List<subRubrosModel> list = new ArrayList<subRubrosModel>();
		subRubrosModel objModel = new subRubrosModel();
		String query = "select sub_rubro_id, codigo, descripcion, rubro_id, desplegable from sub_Rubro";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new subRubrosModel();
				objModel.setSub_rubro_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2).trim());
				objModel.setDescripcion(rs.getString(3).trim());
				objModel.setRubro_id(rs.getInt(4));
				// objModel.setCuentacontable(rs.getString(2));
				// objModel.setTipodetalle(rs.getString(1));
				objModel.setDesplegable(rs.getString(5).trim());

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public ArrayList<subRubrosModel> obtenerSubRubrosId(int id) {
		ArrayList<subRubrosModel> list = new ArrayList<subRubrosModel>();
		subRubrosModel objModel = new subRubrosModel();
		String query = "select a.sub_rubro_id, a.codigo, a.descripcion, a.rubro_id, a.desplegable, b.descripcion nombrerubro  from sub_Rubro a, rubro b where a.rubro_id = b.rubro_id and a.rubro_id = " + id;

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new subRubrosModel();
				objModel.setSub_rubro_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2).trim());
				objModel.setDescripcion(rs.getString(3).trim());
				objModel.setRubro_id(rs.getInt(4));
				objModel.setDesplegable(rs.getString(5).trim());
				objModel.setNombresubrubro(rs.getString(6).trim());

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertSubRubros(subRubrosModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra DAO");

		try {
			con = dataSource.getConnection();
			String query = "insert into Sub_Rubro (codigo, descripcion, rubro_id,  desplegable)values (?,?,?,?)";
			System.out.println("query:" + query);

			System.out.println("codigo:" + objModel.getCodigo());

			cmd = con.prepareStatement(query);

			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());
			cmd.setInt(3, objModel.getRubro_id());
			// cmd.setString(4, objModel.getCuentacontable());
			// cmd.setString(5, objModel.getTipodetalle());
			cmd.setString(4, objModel.getDesplegable().trim());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public subRubrosModel obtenersubrubros(int id) {

		subRubrosModel objModel = new subRubrosModel();
		String query = "select codigo, descripcion, rubro_id, cuentaContable, tipo detalle, desplegable from Sub_Rubro where sub_rubro_id = "
				+ id;

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new subRubrosModel();
				objModel.setCodigo(rs.getString(1).trim());
				objModel.setDescripcion(rs.getString(2).trim());
				objModel.setRubro_id(rs.getInt(3));
				objModel.setCuentacontable(rs.getString(4).trim());
				objModel.setTipodetalle(rs.getString(5).trim());
				objModel.setDesplegable(rs.getString(6).trim());

				// list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return objModel;

	}

	// metodo modificar
	public boolean modificaSubRubros(subRubrosModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE sub_Rubro SET codigo= ?, descripcion = ?, rubro_id= ?, desplegable= ?  WHERE sub_rubro_id = ?";
			System.out.println("query:" + query);
			cmd = con.prepareStatement(query);
			Logger.getLogger(subRubrosDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());
			cmd.setInt(3, objModel.getRubro_id());
			// cmd.setString(4, objModel.getCuentacontable().trim());
			cmd.setString(4, objModel.getDesplegable().trim());

			cmd.setInt(5, objModel.getSub_rubro_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean eliminarSubRubro(int idrubro) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			String query = "DELETE FROM sub_rubro WHERE sub_rubro_id = ?";
			System.out.println("query eliminar:" + query);
			ps = con.prepareStatement(query);

			ps.setInt(1, idrubro);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(subRubrosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}
	
	
	public boolean eliminarElemento(int idrubro) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			String query = "DELETE FROM elementos WHERE sub_rubro_id  = ?";
			
			System.out.println("query eliminar:" + query);
			ps = con.prepareStatement(query);

			ps.setInt(1, idrubro);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(rubrosDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}

}
