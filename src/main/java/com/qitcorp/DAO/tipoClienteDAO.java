package com.qitcorp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.categoriaPersonalModel;
import com.qitcorp.model.estadoObraModel;
import com.qitcorp.model.tipoClienteModel;
import com.qitcorp.model.tiposFinanciamientoModel;
import com.qitcorp.model.zonasModel;

@Repository
public class tipoClienteDAO {

	@Autowired
	private DataSource dataSource;

	public List<tipoClienteModel> obtenerTipoCliente() {
		List<tipoClienteModel> list = new ArrayList<tipoClienteModel>();
		tipoClienteModel objModel = new tipoClienteModel();
		String query = "select tiposdecliente_id, codigo, descripcion from TiposDeCliente";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new tipoClienteModel();
				objModel.setTiposdecliente_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2).trim());
				objModel.setDescripcion(rs.getString(3).trim());

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(tipoClienteDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(tipoClienteDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(tipoClienteDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(tipoClienteDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertTipoCliente(tipoClienteModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra DAO");

		try {
			con = dataSource.getConnection();
String query = "insert into TiposDeCliente (codigo, descripcion)values (?,?)";
System.out.println("query:" + query);

System.out.println("codigo:" + objModel.getCodigo());


			cmd = con.prepareStatement( query);

			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(tipoClienteDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tipoClienteDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}



	// metodo modificar
	public boolean modificaTipoCliente(tipoClienteModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE TiposDeCliente SET codigo= ?, descripcion = ? WHERE tiposdecliente_id = ?";
			System.out.println("query:" + query);
			cmd = con.prepareStatement(query);
			Logger.getLogger(zonasDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());
			cmd.setInt(3, objModel.getTiposdecliente_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}
	public boolean eliminarEstadoObra(int idEliminar) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {

			String query = ("delete  from TiposDeCliente where tiposdecliente_id = ? ");
			System.out.println("query:" + idEliminar);
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);

			ps.setInt(1, idEliminar);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}



}
