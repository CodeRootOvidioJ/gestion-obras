package com.qitcorp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.categoriaPersonalModel;
import com.qitcorp.model.estadoObraModel;
import com.qitcorp.model.tipoMercadoModel;
import com.qitcorp.model.tiposFinanciamientoModel;
import com.qitcorp.model.zonasModel;

@Repository
public class tipoMercadoDAO {

	@Autowired
	private DataSource dataSource;

	public List<tipoMercadoModel> obtenerTipoMercado() {
		List<tipoMercadoModel> list = new ArrayList<tipoMercadoModel>();
		tipoMercadoModel objModel = new tipoMercadoModel();
		String query = "select a.tiposdemercado_id, a.codigo, a.tiposdefinanciamiento_id, a.descripcion, b.descripcion from TiposDeMercado a , tiposdefinanciamiento b where  a.tiposdefinanciamiento_id =  b.tiposdefinanciamiento_id ";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new tipoMercadoModel();
				objModel.setTiposdemercado_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2));
				objModel.setTiposdefinanciamiento_id(rs.getInt(3));
				objModel.setDescripcion(rs.getString(4));
				objModel.setNombre_financiamiento(rs.getString(5).trim());

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(tipoMercadoDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(tipoMercadoDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(tipoMercadoDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(tipoMercadoDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertTipoMercado(tipoMercadoModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra DAO");

		try {
			con = dataSource.getConnection();
String query = "insert into TiposDeMercado (codigo,tiposdefinanciamiento_id,descripcion)values (?,?,?)";
System.out.println("query:" + query);

System.out.println("codigo:" + objModel.getCodigo());


			cmd = con.prepareStatement( query);

			cmd.setString(1, objModel.getCodigo());
			cmd.setInt(2, objModel.getTiposdefinanciamiento_id());
			cmd.setString(3, objModel.getDescripcion());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(tipoMercadoDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tipoMercadoDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}



	// metodo modificar
	public boolean modificaTipomercado(tipoMercadoModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE TiposDeMercado SET codigo= ?,tiposdefinanciamiento_id= ?, descripcion = ? WHERE tiposdemercado_id = ?";
			System.out.println("query:" + query);
			cmd = con.prepareStatement(query);
			Logger.getLogger(zonasDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setString(1, objModel.getCodigo());
			cmd.setInt(2, objModel.getTiposdefinanciamiento_id());
			cmd.setString(3, objModel.getDescripcion());
			cmd.setInt(4, objModel.getTiposdemercado_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}
	
	public boolean eliminarTipoMercado(int idtipomercado){
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
			try {
				con = dataSource.getConnection();
				String query = "DELETE FROM TiposDeMercado WHERE tiposdemercado_id = ?";
				System.out.println("query eliminar:" +  query);
				ps = con.prepareStatement(query);

				ps.setInt(1, idtipomercado);
				if (ps.executeUpdate() > 0) {
					result = true;
				}
		}catch (Exception e) {
			Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
		}finally {
			try {
				if(ps != null)ps.close();
				if(con != null)con.close();
			}catch (Exception e) {
				Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}


}
