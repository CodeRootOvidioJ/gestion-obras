package com.qitcorp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.qitcorp.model.tiposFinanciamientoModel;

@Repository
public class tiposFinanciamientoDAO {

	@Autowired
	private DataSource dataSource;

	public List<tiposFinanciamientoModel> obtenerTipoFinanciamiento() {
		List<tiposFinanciamientoModel> list = new ArrayList<tiposFinanciamientoModel>();
		tiposFinanciamientoModel objModel = new tiposFinanciamientoModel();
		String query = "select tiposdefinanciamiento_id, codigo, descripcion from TiposDeFinanciamiento";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new tiposFinanciamientoModel();
				objModel.setTiposdefinanciamiento_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2).trim());
				objModel.setDescripcion(rs.getString(3).trim());

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(tiposFinanciamientoDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(tiposFinanciamientoDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(tiposFinanciamientoDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(tiposFinanciamientoDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertTipoFinanciamiento(tiposFinanciamientoModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra DAO");

		try {
			con = dataSource.getConnection();
			String query = "insert into TiposDeFinanciamiento (codigo, descripcion)values (?,?)";
			System.out.println("query:" + query);

			System.out.println("codigo:" + objModel.getCodigo());

			cmd = con.prepareStatement(query);

			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(tiposFinanciamientoDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposFinanciamientoDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean modificarFinanciamiento(tiposFinanciamientoModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE TiposDeFinanciamiento SET codigo= ?, descripcion = ? WHERE TiposDeFinanciamiento_id = ?";
			System.out.println("query:" + query);
			cmd = con.prepareStatement(query);
			Logger.getLogger(zonasDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());
			cmd.setInt(3, objModel.getTiposdefinanciamiento_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean eliminarFinanciamiento(int idEliminar) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {

			String query = ("delete  from TiposDeFinanciamiento where TiposDeFinanciamiento_id = ? ");
			System.out.println("query:" + idEliminar);
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);

			ps.setInt(1, idEliminar);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}

}
