package com.qitcorp.DAO;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.categoriaPersonalModel;
import com.qitcorp.model.estadoObraModel;
import com.qitcorp.model.tipoContratoModel;
import com.qitcorp.model.tipoGarantiaModel;
import com.qitcorp.model.tiposFinanciamientoModel;
import com.qitcorp.model.turnosTrabajoModel;
import com.qitcorp.model.zonasModel;

@Repository
public class turnosTrabajoDAO {

	@Autowired
	private DataSource dataSource;

	public String isStringHora(String cadena) {
		System.out.println("** Hora recibida parseado = " + cadena);
		String hora = null;
		Date horas = null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:");
			hora = (sdf.parse(cadena).toLocaleString());

			System.out.println("** Hora parseado = " + horas);
		} catch (Exception nfe) {
			System.out.println("** Hora parseado = " + horas);
			return hora;

		}
		System.out.println("** Hora parseado = " + horas);
		return hora;
	}

	public List<turnosTrabajoModel> obtenerTurnosTrabajo() {
		List<turnosTrabajoModel> list = new ArrayList<turnosTrabajoModel>();
		turnosTrabajoModel objModel = new turnosTrabajoModel();
		String query = "select turnosdetrabajo_id ,codigo, descripcion , Format(Hora_inicio, 'hh\\:mm') Hora_inicio, Format(Hora_fin, 'hh\\:mm') Hora_fin, Horas_trabajadas  from TurnosDeTrabajo";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);
			while (rs.next()) {
				objModel = new turnosTrabajoModel();
				objModel.setTurnosdetrabajo_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2));
				objModel.setDescripcion(rs.getString(3));

				objModel.setHorainicio(rs.getString(4));
				objModel.setHorafin(rs.getString(5));
				objModel.setHorastrabajadas(rs.getDouble(6));

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(turnosTrabajoDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(turnosTrabajoDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(turnosTrabajoDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(turnosTrabajoDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertTurnosTrabajo(turnosTrabajoModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra horast" + objModel.getHorastrabajadas());

		try {
			con = dataSource.getConnection();
			String query = "insert into TurnosDeTrabajo (codigo, descripcion, Hora_inicio, Hora_fin  )values (?,?, CAST (? AS TIME), CAST (? AS TIME) )";
			System.out.println("query:" + query);

			System.out.println("codigo:" + objModel.getCodigo());

			cmd = con.prepareStatement(query);

			cmd.setString(1, objModel.getCodigo());
			cmd.setString(2, objModel.getDescripcion());

			cmd.setString(3, objModel.getHorainicio());
			cmd.setString(4, objModel.getHorafin());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(turnosTrabajoDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(turnosTrabajoDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean modificarTurnosTrabajo(turnosTrabajoModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE TurnosDeTrabajo SET codigo= ?, descripcion = ?,Hora_inicio= CAST (? AS TIME), Hora_fin= CAST (? AS TIME)  WHERE TurnosDeTrabajo_id = ?";
			System.out.println("query:" + query);
			cmd = con.prepareStatement(query);
			Logger.getLogger(zonasDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());
			cmd.setString(3, objModel.getHorainicio().trim());
			cmd.setString(4, objModel.getHorafin().trim());
			cmd.setInt(5, objModel.getTurnosdetrabajo_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public boolean eliminarTurnosTrabajo(int idzona) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {

			String query = ("delete  from TurnosDeTrabajo where TurnosDeTrabajo_id = ? ");
			System.out.println("query:" + idzona);
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);

			ps.setInt(1, idzona);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(tiposObrasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}

}
