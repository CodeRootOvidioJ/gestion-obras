package com.qitcorp.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.qitcorp.model.filialesModel;
import com.qitcorp.model.subRubrosModel;
import com.qitcorp.model.zonasModel;

@Repository
public class zonasDAO {

	@Autowired
	private DataSource dataSource;

	public List<zonasModel> obtenerZonas() {
		List<zonasModel> list = new ArrayList<zonasModel>();
		zonasModel objModel = new zonasModel();
		String query = "select zonas_id, codigo, descripcion, moneda from zonas";

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new zonasModel();
				objModel.setZonas_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2));
				objModel.setDescripcion(rs.getString(3));
				objModel.setMoneda(rs.getString(4));

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

	public boolean insertZonas(zonasModel objModel) {

		boolean result = false;
		PreparedStatement cmd = null;
		Connection con = null;

		System.out.println("entra DAO");

		try {
			con = dataSource.getConnection();
			String query = "insert into zonas (codigo, descripcion, moneda)values (?,?,?)";
			System.out.println("query:" + query);

			System.out.println("codigo:" + objModel.getCodigo());

			System.out.println("moneda:" + objModel.getMoneda());

			cmd = con.prepareStatement(query);

			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());
			cmd.setString(3, objModel.getMoneda().trim());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}

		} catch (Exception e) {
			Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	
	public boolean eliminarfiliales(int idzona) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {

			String query = ("delete  from filiales where zonas_id = ? ");
			System.out.println("id_zona:" + idzona);
			con = dataSource.getConnection();
			ps = con.prepareStatement(query);

			ps.setInt(1, idzona);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}

	public boolean eliminarZonas(int idzona) {
		boolean result = false;
		PreparedStatement ps = null;
		Connection con = null;
		try {
			con = dataSource.getConnection();
			String query = ("delete  from zonas  where zonas_id = ?");
			ps = con.prepareStatement(query);

			ps.setInt(1, idzona);
			if (ps.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (ps != null)
					ps.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}

		return result;
	}

	// metodo modificar
	public boolean modificaZonas(zonasModel objModel) {
		boolean result = false;
		Connection con = null;
		PreparedStatement cmd = null;
		try {
			con = dataSource.getConnection();
			String query = "UPDATE zonas SET codigo= ?, descripcion = ? WHERE zonas_id = ?";
			System.out.println("query:" + query);
			System.out.println("Id Modificar:" +  objModel.getZonas_id());
			cmd = con.prepareStatement(query);
			Logger.getLogger(zonasDAO.class.getName()).log(Level.INFO, "update  " + query);
			cmd.setString(1, objModel.getCodigo().trim());
			cmd.setString(2, objModel.getDescripcion().trim());
			cmd.setInt(3, objModel.getZonas_id());

			if (cmd.executeUpdate() > 0) {
				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				if (cmd != null)
					cmd.close();
				if (con != null)
					con.close();
			} catch (Exception e) {
				Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		return result;
	}

	public ArrayList<filialesModel> obtenerfilialesId(int id) {
		ArrayList<filialesModel> list = new ArrayList<filialesModel>();
		filialesModel objModel = new filialesModel();
		String query = "select b.filiales_id, b.codigo, b.descripcion, b.moneda, b.zonas_id , a.descripcion descripcion_zonas  from filiales b, zonas a where a.zonas_id = b.zonas_id and  b.zonas_id = " + id;

		Connection con = null;
		ResultSet rs = null;
		Statement cmd = null;
		System.out.println("query:" + query);
		try {
			System.out.println("dts1:" + con);
			con = dataSource.getConnection();

			System.out.println("dts:" + con);
			cmd = con.createStatement();

			rs = cmd.executeQuery(query);

			while (rs.next()) {
				objModel = new filialesModel();
				objModel.setFiliales_id(rs.getInt(1));
				objModel.setCodigo(rs.getString(2).trim());
				objModel.setDescripcion(rs.getString(3).trim());
				objModel.setMoneda(rs.getString(4).trim());
				objModel.setZonas_id(rs.getInt(5));
				objModel.setDescripcion_zonas(rs.getString(6).trim());

				list.add(objModel);

			}
		} catch (Exception e) {
			Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			if (rs != null)
				try {
					rs.close();
				} catch (SQLException e) {
					Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (cmd != null)
				try {
					cmd.close();
				} catch (SQLException e) {
					Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
			if (con != null)
				try {
					con.close();
				} catch (SQLException e) {
					Logger.getLogger(zonasDAO.class.getName()).log(Level.SEVERE, null, e);
				}
		}
		return list;

	}

}
