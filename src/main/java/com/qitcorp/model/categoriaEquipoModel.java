package com.qitcorp.model;

public class categoriaEquipoModel {
	
	
	
	String codigo;
	String descripcion;
	int categorias_equipos_id;
	
	
	
	public categoriaEquipoModel() {
		super();
	}




	public int getCategorias_equipos_id() {
		return categorias_equipos_id;
	}




	public void setCategorias_equipos_id(int categorias_equipos_id) {
		this.categorias_equipos_id = categorias_equipos_id;
	}




	public categoriaEquipoModel(String codigo, String descripcion, int categorias_equipos_id) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.categorias_equipos_id = categorias_equipos_id;
	}




	public String getCodigo() {
		return codigo;
	}



	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}



	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	
	
}
