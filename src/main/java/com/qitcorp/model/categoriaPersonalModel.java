package com.qitcorp.model;

public class categoriaPersonalModel {

	int categorias_personal_id;

	String codigo;
	String descripcion;

	public categoriaPersonalModel() {
		super();
	}

	public int getCategorias_personal_id() {
		return categorias_personal_id;
	}

	public void setCategorias_personal_id(int categorias_personal_id) {
		this.categorias_personal_id = categorias_personal_id;
	}

	public categoriaPersonalModel(int categorias_personal_id, String codigo, String descripcion) {
		super();
		this.categorias_personal_id = categorias_personal_id;
		this.codigo = codigo;
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
