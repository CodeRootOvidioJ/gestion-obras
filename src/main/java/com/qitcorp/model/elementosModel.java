package com.qitcorp.model;

public class elementosModel {
	
	
	int elemento_id;
	String codigo;
	String descripcion;
	int subrubro;
	String unidad;
	String costo;
	String nombre_subrubro;
	
	
	public elementosModel() {
		super();
		// TODO Auto-generated constructor stub
	}

	
	
	
	
	public elementosModel(int elemento_id, String codigo, String descripcion, int subrubro, String unidad, String costo,
			String nombre_subrubro) {
		super();
		this.elemento_id = elemento_id;
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.subrubro = subrubro;
		this.unidad = unidad;
		this.costo = costo;
		this.nombre_subrubro = nombre_subrubro;
	}





	public String getNombre_subrubro() {
		return nombre_subrubro;
	}





	public void setNombre_subrubro(String nombre_subrubro) {
		this.nombre_subrubro = nombre_subrubro;
	}





	public int getElemento_id() {
		return elemento_id;
	}
	public void setElemento_id(int elemento_id) {
		this.elemento_id = elemento_id;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getSubrubro() {
		return subrubro;
	}
	public void setSubrubro(int subrubro) {
		this.subrubro = subrubro;
	}
	public String getUnidad() {
		return unidad;
	}
	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}
	public String getCosto() {
		return costo;
	}
	public void setCosto(String costo) {
		this.costo = costo;
	}
	

	
	





	
	
}
