package com.qitcorp.model;

public class equiposModel {

	int equipos_id;

	String codigo;
	String nombre;
	int categorias_equipos_id;
	String unidad;
	Double t1;
	Double pgr1;
	Double t2;
	Double pgr2;
	Double t3;
	Double pgr3;
	String nombrecategoria;

	public equiposModel() {
		super();
	}

	public String getNombrecategoria() {
		return nombrecategoria;
	}

	public void setNombrecategoria(String nombrecategoria) {
		this.nombrecategoria = nombrecategoria;
	}

	public equiposModel(int equipos_id, String codigo, String nombre, int categorias_equipos_id, String unidad,
			Double t1, Double pgr1, Double t2, Double pgr2, Double t3, Double pgr3, String nombrecategoria) {
		super();
		this.equipos_id = equipos_id;
		this.codigo = codigo;
		this.nombre = nombre;
		this.categorias_equipos_id = categorias_equipos_id;
		this.unidad = unidad;
		this.t1 = t1;
		this.pgr1 = pgr1;
		this.t2 = t2;
		this.pgr2 = pgr2;
		this.t3 = t3;
		this.pgr3 = pgr3;
		this.nombrecategoria = nombrecategoria;
	}

	public int getEquipos_id() {
		return equipos_id;
	}

	public void setEquipos_id(int equipos_id) {
		this.equipos_id = equipos_id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCategorias_equipos_id() {
		return categorias_equipos_id;
	}

	public void setCategorias_equipos_id(int categorias_equipos_id) {
		this.categorias_equipos_id = categorias_equipos_id;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public Double getT1() {
		return t1;
	}

	public void setT1(Double t1) {
		this.t1 = t1;
	}

	public Double getPgr1() {
		return pgr1;
	}

	public void setPgr1(Double pgr1) {
		this.pgr1 = pgr1;
	}

	public Double getT2() {
		return t2;
	}

	public void setT2(Double t2) {
		this.t2 = t2;
	}

	public Double getPgr2() {
		return pgr2;
	}

	public void setPgr2(Double pgr2) {
		this.pgr2 = pgr2;
	}

	public Double getT3() {
		return t3;
	}

	public void setT3(Double t3) {
		this.t3 = t3;
	}

	public Double getPgr3() {
		return pgr3;
	}

	public void setPgr3(Double pgr3) {
		this.pgr3 = pgr3;
	}

}
