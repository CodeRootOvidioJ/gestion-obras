package com.qitcorp.model;

public class estadoObraModel {
	
	
	
	String codigo;
	String descripcion;
	int estadosdeobra_id;
	
	
	public estadoObraModel() {
		super();
	}



	public int getEstadosdeobra_id() {
		return estadosdeobra_id;
	}



	public void setEstadosdeobra_id(int estadosdeobra_id) {
		this.estadosdeobra_id = estadosdeobra_id;
	}





	public estadoObraModel(String codigo, String descripcion, int estadosdeobra_id) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.estadosdeobra_id = estadosdeobra_id;
	}











	public String getCodigo() {
		return codigo;
	}



	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}



	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	
	
}
