package com.qitcorp.model;

import java.util.Date;

public class obrasModel {

	String codigo;
	String denominacion;
	int obras_id;

	String nombre_corto;
	String direccion;
	String coordenadas;
	String ingeniero;
	String director;
	String finalidad;
	String tipo_terreno;
	String dificultades_tecnicas;
	String nombre_cliente;
	String nit;
	String direcion_cliente;
	String telefonos;
	String contacto_tecnico;
	String contacto_cobro;
	String inicio;
	String fin;
	int duracion;
	Double anticipo;
	int dia_corte;
	int dias_credito;
	int estadosdeobras_id;
	int filiales_id;
	int tiposdeobra_id;
	int tiposdecontrato_id;
	int tiposdefinanciamiento_id;
	int tiposdemercado_id;
	int tiposdecliente_id;
	int turnosdetrabajo_id;

	public obrasModel() {
		super();
	}

	
	
	
	public obrasModel(String codigo, String denominacion, int obras_id, String nombre_corto, String direccion,
			String coordenadas, String ingeniero, String director, String finalidad, String tipo_terreno,
			String dificultades_tecnicas, String nombre_cliente, String nit, String direcion_cliente, String telefonos,
			String contacto_tecnico, String contacto_cobro, String inicio, String fin, int duracion, Double anticipo,
			int dia_corte, int dias_credito, int estadosdeobras_id, int filiales_id, int tiposdeobra_id,
			int tiposdecontrato_id, int tiposdefinanciamiento_id, int tiposdemercado_id, int tiposdecliente_id,
			int turnosdetrabajo_id) {
		super();
		this.codigo = codigo;
		this.denominacion = denominacion;
		this.obras_id = obras_id;
		this.nombre_corto = nombre_corto;
		this.direccion = direccion;
		this.coordenadas = coordenadas;
		this.ingeniero = ingeniero;
		this.director = director;
		this.finalidad = finalidad;
		this.tipo_terreno = tipo_terreno;
		this.dificultades_tecnicas = dificultades_tecnicas;
		this.nombre_cliente = nombre_cliente;
		this.nit = nit;
		this.direcion_cliente = direcion_cliente;
		this.telefonos = telefonos;
		this.contacto_tecnico = contacto_tecnico;
		this.contacto_cobro = contacto_cobro;
		this.inicio = inicio;
		this.fin = fin;
		this.duracion = duracion;
		this.anticipo = anticipo;
		this.dia_corte = dia_corte;
		this.dias_credito = dias_credito;
		this.estadosdeobras_id = estadosdeobras_id;
		this.filiales_id = filiales_id;
		this.tiposdeobra_id = tiposdeobra_id;
		this.tiposdecontrato_id = tiposdecontrato_id;
		this.tiposdefinanciamiento_id = tiposdefinanciamiento_id;
		this.tiposdemercado_id = tiposdemercado_id;
		this.tiposdecliente_id = tiposdecliente_id;
		this.turnosdetrabajo_id = turnosdetrabajo_id;
	}




	public int getDuracion() {
		return duracion;
	}




	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}




	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDenominacion() {
		return denominacion;
	}

	public void setDenominacion(String denominacion) {
		this.denominacion = denominacion;
	}

	public int getObras_id() {
		return obras_id;
	}

	public void setObras_id(int obras_id) {
		this.obras_id = obras_id;
	}

	public String getNombre_corto() {
		return nombre_corto;
	}

	public void setNombre_corto(String nombre_corto) {
		this.nombre_corto = nombre_corto;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(String coordenadas) {
		this.coordenadas = coordenadas;
	}

	public String getIngeniero() {
		return ingeniero;
	}

	public void setIngeniero(String ingeniero) {
		this.ingeniero = ingeniero;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getFinalidad() {
		return finalidad;
	}

	public void setFinalidad(String finalidad) {
		this.finalidad = finalidad;
	}

	public String getTipo_terreno() {
		return tipo_terreno;
	}

	public void setTipo_terreno(String tipo_terreno) {
		this.tipo_terreno = tipo_terreno;
	}

	public String getDificultades_tecnicas() {
		return dificultades_tecnicas;
	}

	public void setDificultades_tecnicas(String dificultades_tecnicas) {
		this.dificultades_tecnicas = dificultades_tecnicas;
	}

	public String getNombre_cliente() {
		return nombre_cliente;
	}

	public void setNombre_cliente(String nombre_cliente) {
		this.nombre_cliente = nombre_cliente;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getDirecion_cliente() {
		return direcion_cliente;
	}

	public void setDirecion_cliente(String direcion_cliente) {
		this.direcion_cliente = direcion_cliente;
	}

	public String getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(String telefonos) {
		this.telefonos = telefonos;
	}

	public String getContacto_tecnico() {
		return contacto_tecnico;
	}

	public void setContacto_tecnico(String contacto_tecnico) {
		this.contacto_tecnico = contacto_tecnico;
	}

	public String getContacto_cobro() {
		return contacto_cobro;
	}

	public void setContacto_cobro(String contacto_cobro) {
		this.contacto_cobro = contacto_cobro;
	}

	public String getInicio() {
		return inicio;
	}

	public void setInicio(String inicio) {
		this.inicio = inicio;
	}

	public String getFin() {
		return fin;
	}

	public void setFin(String fin) {
		this.fin = fin;
	}

	

	public Double getAnticipo() {
		return anticipo;
	}

	public void setAnticipo(Double anticipo) {
		this.anticipo = anticipo;
	}

	public int getDia_corte() {
		return dia_corte;
	}

	public void setDia_corte(int dia_corte) {
		this.dia_corte = dia_corte;
	}

	public int getDias_credito() {
		return dias_credito;
	}

	public void setDias_credito(int dias_credito) {
		this.dias_credito = dias_credito;
	}

	public int getEstadosdeobras_id() {
		return estadosdeobras_id;
	}

	public void setEstadosdeobras_id(int estadosdeobras_id) {
		this.estadosdeobras_id = estadosdeobras_id;
	}

	public int getFiliales_id() {
		return filiales_id;
	}

	public void setFiliales_id(int filiales_id) {
		this.filiales_id = filiales_id;
	}

	public int getTiposdeobra_id() {
		return tiposdeobra_id;
	}

	public void setTiposdeobra_id(int tiposdeobra_id) {
		this.tiposdeobra_id = tiposdeobra_id;
	}

	public int getTiposdecontrato_id() {
		return tiposdecontrato_id;
	}

	public void setTiposdecontrato_id(int tiposdecontrato_id) {
		this.tiposdecontrato_id = tiposdecontrato_id;
	}

	public int getTiposdefinanciamiento_id() {
		return tiposdefinanciamiento_id;
	}

	public void setTiposdefinanciamiento_id(int tiposdefinanciamiento_id) {
		this.tiposdefinanciamiento_id = tiposdefinanciamiento_id;
	}

	public int getTiposdemercado_id() {
		return tiposdemercado_id;
	}

	public void setTiposdemercado_id(int tiposdemercado_id) {
		this.tiposdemercado_id = tiposdemercado_id;
	}

	public int getTiposdecliente_id() {
		return tiposdecliente_id;
	}

	public void setTiposdecliente_id(int tiposdecliente_id) {
		this.tiposdecliente_id = tiposdecliente_id;
	}

	public int getTurnosdetrabajo_id() {
		return turnosdetrabajo_id;
	}

	public void setTurnosdetrabajo_id(int turnosdetrabajo_id) {
		this.turnosdetrabajo_id = turnosdetrabajo_id;
	}

}
