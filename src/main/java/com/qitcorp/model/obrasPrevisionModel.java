package com.qitcorp.model;

public class obrasPrevisionModel {


	int obrasprevision_id;
	String codigo;
	int periodos_id;
	int rubro_id;
	int sub_rubro_id;
	int elemento_id;
	int obras_id;
	
	String descripcion;
	String medida;
	Double costosugerido;
	Double unidades;
	Double costo;
	String nombrerubro;
	String nombreperiodo;
	String nombreobra;

	public obrasPrevisionModel() {
		super();
	}









	public int getObras_id() {
		return obras_id;
	}









	public void setObras_id(int obras_id) {
		this.obras_id = obras_id;
	}



















	public obrasPrevisionModel(int obrasprevision_id, String codigo, int periodos_id, int rubro_id, int sub_rubro_id,
			int elemento_id, int obras_id, String descripcion, String medida, Double costosugerido, Double unidades,
			Double costo, String nombrerubro, String nombreperiodo, String nombreobra) {
		super();
		this.obrasprevision_id = obrasprevision_id;
		this.codigo = codigo;
		this.periodos_id = periodos_id;
		this.rubro_id = rubro_id;
		this.sub_rubro_id = sub_rubro_id;
		this.elemento_id = elemento_id;
		this.obras_id = obras_id;
		this.descripcion = descripcion;
		this.medida = medida;
		this.costosugerido = costosugerido;
		this.unidades = unidades;
		this.costo = costo;
		this.nombrerubro = nombrerubro;
		this.nombreperiodo = nombreperiodo;
		this.nombreobra = nombreobra;
	}









	public String getNombreobra() {
		return nombreobra;
	}









	public void setNombreobra(String nombreobra) {
		this.nombreobra = nombreobra;
	}









	public String getNombrerubro() {
		return nombrerubro;
	}




	public void setNombrerubro(String nombrerubro) {
		this.nombrerubro = nombrerubro;
	}




	public String getNombreperiodo() {
		return nombreperiodo;
	}




	public void setNombreperiodo(String nombreperiodo) {
		this.nombreperiodo = nombreperiodo;
	}




	public int getObrasprevision_id() {
		return obrasprevision_id;
	}


	public void setObrasprevision_id(int obrasprevision_id) {
		this.obrasprevision_id = obrasprevision_id;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public int getPeriodos_id() {
		return periodos_id;
	}


	public void setPeriodos_id(int periodos_id) {
		this.periodos_id = periodos_id;
	}


	public int getRubro_id() {
		return rubro_id;
	}


	public void setRubro_id(int rubro_id) {
		this.rubro_id = rubro_id;
	}


	public int getSub_rubro_id() {
		return sub_rubro_id;
	}


	public void setSub_rubro_id(int sub_rubro_id) {
		this.sub_rubro_id = sub_rubro_id;
	}


	public int getElemento_id() {
		return elemento_id;
	}


	public void setElemento_id(int elemento_id) {
		this.elemento_id = elemento_id;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public String getMedida() {
		return medida;
	}


	public void setMedida(String medida) {
		this.medida = medida;
	}


	public Double getCostosugerido() {
		return costosugerido;
	}


	public void setCostosugerido(Double costosugerido) {
		this.costosugerido = costosugerido;
	}


	public Double getUnidades() {
		return unidades;
	}


	public void setUnidades(Double unidades) {
		this.unidades = unidades;
	}


	public Double getCosto() {
		return costo;
	}


	public void setCosto(Double costo) {
		this.costo = costo;
	}



}
