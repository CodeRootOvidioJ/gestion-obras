package com.qitcorp.model;

public class periodosModel {

	String codigo;
	String descripcion;
	String nombre;
	String fecha_inicio;
	String fecha_fin;
	String generales;
	int estado;
	String nombreestado;
	

	public String getNombreestado() {
		return nombreestado;
	}

	public void setNombreestado(String nombreestado) {
		this.nombreestado = nombreestado;
	}

	public periodosModel(String codigo, String descripcion, String nombre, String fecha_inicio, String fecha_fin,
			String generales, int estado, String nombreestado, int periodos_id) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.nombre = nombre;
		this.fecha_inicio = fecha_inicio;
		this.fecha_fin = fecha_fin;
		this.generales = generales;
		this.estado = estado;
		this.nombreestado = nombreestado;
		this.periodos_id = periodos_id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getFecha_inicio() {
		return fecha_inicio;
	}

	public void setFecha_inicio(String fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}

	public String getFecha_fin() {
		return fecha_fin;
	}

	public void setFecha_fin(String fecha_fin) {
		this.fecha_fin = fecha_fin;
	}

	public String getGenerales() {
		return generales;
	}

	public void setGenerales(String generales) {
		this.generales = generales;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getPeriodos_id() {
		return periodos_id;
	}

	public void setPeriodos_id(int periodos_id) {
		this.periodos_id = periodos_id;
	}

	int periodos_id;

	public periodosModel() {
		super();
	}
	
	

}
