package com.qitcorp.model;

public class rubrosModel {
	
	
	
	String codigo;
	String descripcion;
	int rubro_id;
	
	
	
	public rubrosModel() {
		super();
	}



	public rubrosModel(String codigo, String descripcion, int rubro_id) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.rubro_id = rubro_id;
	}



	public String getCodigo() {
		return codigo;
	}



	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}



	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public int getRubro_id() {
		return rubro_id;
	}



	public void setRubro_id(int rubro_id) {
		this.rubro_id = rubro_id;
	}


	
	
}
