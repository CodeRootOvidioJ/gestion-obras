package com.qitcorp.model;

public class semanasModel {

	String codigo;
	int semanas_id;
	int periodos_id;
	String fecha_fin;
	String fecha_inicio;
	int estado;
	String nombreestado;
	

	public semanasModel() {
		super();
	}


	public semanasModel(String codigo, int semanas_id, int periodos_id, String fecha_fin, String fecha_inicio,
			int estado, String nombreestado) {
		super();
		this.codigo = codigo;
		this.semanas_id = semanas_id;
		this.periodos_id = periodos_id;
		this.fecha_fin = fecha_fin;
		this.fecha_inicio = fecha_inicio;
		this.estado = estado;
		this.nombreestado = nombreestado;
	}


	public String getCodigo() {
		return codigo;
	}


	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public int getSemanas_id() {
		return semanas_id;
	}


	public void setSemanas_id(int semanas_id) {
		this.semanas_id = semanas_id;
	}


	public int getPeriodos_id() {
		return periodos_id;
	}


	public void setPeriodos_id(int periodos_id) {
		this.periodos_id = periodos_id;
	}


	public String getFecha_fin() {
		return fecha_fin;
	}


	public void setFecha_fin(String fecha_fin) {
		this.fecha_fin = fecha_fin;
	}


	public String getFecha_inicio() {
		return fecha_inicio;
	}


	public void setFecha_inicio(String fecha_inicio) {
		this.fecha_inicio = fecha_inicio;
	}


	public int getEstado() {
		return estado;
	}


	public void setEstado(int estado) {
		this.estado = estado;
	}


	public String getNombreestado() {
		return nombreestado;
	}


	public void setNombreestado(String nombreestado) {
		this.nombreestado = nombreestado;
	}
	
	

}
