package com.qitcorp.model;

public class siProductosModel {
	
	
	int productoid;
	int categoraid  ;
	String nombre ;
	String marca  ;
	String modelo  ;
	double precio;
	int cantidad ;
	String Fechacreación;
	String fechaactualiza;
	String creadopor;
	String Actualizadopor;
	
	
	
	public siProductosModel() {
		super();
	}



	public siProductosModel(int productoid, String nombre, String marca, String modelo, double precio, int categoraid,
			int cantidad, String fechacreación, String fechaactualiza, String creadopor, String actualizadopor) {
		super();
		this.productoid = productoid;
		this.nombre = nombre;
		this.marca = marca;
		this.modelo = modelo;
		this.precio = precio;
		this.categoraid = categoraid;
		this.cantidad = cantidad;
		Fechacreación = fechacreación;
		this.fechaactualiza = fechaactualiza;
		this.creadopor = creadopor;
		Actualizadopor = actualizadopor;
	}



	public int getProductoid() {
		return productoid;
	}



	public void setProductoid(int productoid) {
		this.productoid = productoid;
	}



	public String getNombre() {
		return nombre;
	}



	public void setNombre(String nombre) {
		this.nombre = nombre;
	}



	public String getMarca() {
		return marca;
	}



	public void setMarca(String marca) {
		this.marca = marca;
	}



	public String getModelo() {
		return modelo;
	}



	public void setModelo(String modelo) {
		this.modelo = modelo;
	}



	public double getPrecio() {
		return precio;
	}



	public void setPrecio(double precio) {
		this.precio = precio;
	}



	public int getCategoraid() {
		return categoraid;
	}



	public void setCategoraid(int categoraid) {
		this.categoraid = categoraid;
	}



	public int getCantidad() {
		return cantidad;
	}



	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}



	public String getFechacreación() {
		return Fechacreación;
	}



	public void setFechacreación(String fechacreación) {
		Fechacreación = fechacreación;
	}



	public String getFechaactualiza() {
		return fechaactualiza;
	}



	public void setFechaactualiza(String fechaactualiza) {
		this.fechaactualiza = fechaactualiza;
	}



	public String getCreadopor() {
		return creadopor;
	}



	public void setCreadopor(String creadopor) {
		this.creadopor = creadopor;
	}



	public String getActualizadopor() {
		return Actualizadopor;
	}



	public void setActualizadopor(String actualizadopor) {
		Actualizadopor = actualizadopor;
	}
	
	
	

}
