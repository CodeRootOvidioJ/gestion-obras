package com.qitcorp.model;

public class siVentasModel {
	
	
	int compraid;
	int productoid;
	int proveedorid;
	int cantidad;
	double precio;
	String Fechadecompra;
	String Fechacreación;
	String fechaactualiza;
	String creadopor;
	String Actualizadopor;
	
	
	
	public siVentasModel() {
		super();
	}

	public siVentasModel(int compraid, int productoid, int proveedorid, int cantidad, double precio,
			String fechadecompra, String fechacreación, String fechaactualiza, String creadopor,
			String actualizadopor) {
		super();
		this.compraid = compraid;
		this.productoid = productoid;
		this.proveedorid = proveedorid;
		this.cantidad = cantidad;
		this.precio = precio;
		Fechadecompra = fechadecompra;
		Fechacreación = fechacreación;
		this.fechaactualiza = fechaactualiza;
		this.creadopor = creadopor;
		Actualizadopor = actualizadopor;
	}

	public int getCompraid() {
		return compraid;
	}
	public void setCompraid(int compraid) {
		this.compraid = compraid;
	}
	public int getProductoid() {
		return productoid;
	}
	public void setProductoid(int productoid) {
		this.productoid = productoid;
	}
	public int getProveedorid() {
		return proveedorid;
	}
	public void setProveedorid(int proveedorid) {
		this.proveedorid = proveedorid;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public String getFechadecompra() {
		return Fechadecompra;
	}
	public void setFechadecompra(String fechadecompra) {
		Fechadecompra = fechadecompra;
	}
	public String getFechacreación() {
		return Fechacreación;
	}
	public void setFechacreación(String fechacreación) {
		Fechacreación = fechacreación;
	}
	public String getFechaactualiza() {
		return fechaactualiza;
	}
	public void setFechaactualiza(String fechaactualiza) {
		this.fechaactualiza = fechaactualiza;
	}
	public String getCreadopor() {
		return creadopor;
	}
	public void setCreadopor(String creadopor) {
		this.creadopor = creadopor;
	}
	public String getActualizadopor() {
		return Actualizadopor;
	}
	public void setActualizadopor(String actualizadopor) {
		Actualizadopor = actualizadopor;
	}

}
