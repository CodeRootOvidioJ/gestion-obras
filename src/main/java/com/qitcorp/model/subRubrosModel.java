package com.qitcorp.model;

public class subRubrosModel {
	
	
	
	String codigo;
	String descripcion;
	int rubro_id;
	int sub_rubro_id;
	String cuentacontable;
	String tipodetalle;

	String desplegable;
	
	String nombresubrubro;
	
	
	public subRubrosModel() {
		super();
	}




	public String getCodigo() {
		return codigo;
	}


	public subRubrosModel(String codigo, String descripcion, int rubro_id, int sub_rubro_id, String cuentacontable,
			String tipodetalle, String desplegable, String nombresubrubro) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.rubro_id = rubro_id;
		this.sub_rubro_id = sub_rubro_id;
		this.cuentacontable = cuentacontable;
		this.tipodetalle = tipodetalle;
		this.desplegable = desplegable;
		this.nombresubrubro = nombresubrubro;
	}




	public String getNombresubrubro() {
		return nombresubrubro;
	}




	public void setNombresubrubro(String nombresubrubro) {
		this.nombresubrubro = nombresubrubro;
	}




	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public int getRubro_id() {
		return rubro_id;
	}


	public void setRubro_id(int rubro_id) {
		this.rubro_id = rubro_id;
	}


	public int getSub_rubro_id() {
		return sub_rubro_id;
	}


	public void setSub_rubro_id(int sub_rubro_id) {
		this.sub_rubro_id = sub_rubro_id;
	}


	public String getCuentacontable() {
		return cuentacontable;
	}


	public void setCuentacontable(String cuentacontable) {
		this.cuentacontable = cuentacontable;
	}


	public String getTipodetalle() {
		return tipodetalle;
	}


	public void setTipodetalle(String tipodetalle) {
		this.tipodetalle = tipodetalle;
	}


	public String getDesplegable() {
		return desplegable;
	}


	public void setDesplegable(String desplegable) {
		this.desplegable = desplegable;
	}


	
}
