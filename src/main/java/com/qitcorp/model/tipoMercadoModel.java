package com.qitcorp.model;

public class tipoMercadoModel {

	String codigo;
	String descripcion;
	int tiposdefinanciamiento_id;
	int tiposdemercado_id;
	String nombre_financiamiento;

	public tipoMercadoModel() {
		super();
	}
	
	
	

	public String getNombre_financiamiento() {
		return nombre_financiamiento;
	}




	public void setNombre_financiamiento(String nombre_financiamiento) {
		this.nombre_financiamiento = nombre_financiamiento;
	}




	public tipoMercadoModel(String codigo, String descripcion, int tiposdefinanciamiento_id, int tiposdemercado_id,
			String nombre_financiamiento) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.tiposdefinanciamiento_id = tiposdefinanciamiento_id;
		this.tiposdemercado_id = tiposdemercado_id;
		this.nombre_financiamiento = nombre_financiamiento;
	}




	public int getTiposdefinanciamiento_id() {
		return tiposdefinanciamiento_id;
	}

	public void setTiposdefinanciamiento_id(int tiposdefinanciamiento_id) {
		this.tiposdefinanciamiento_id = tiposdefinanciamiento_id;
	}

	public int getTiposdemercado_id() {
		return tiposdemercado_id;
	}

	public void setTiposdemercado_id(int tiposdemercado_id) {
		this.tiposdemercado_id = tiposdemercado_id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
