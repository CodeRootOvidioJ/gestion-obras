package com.qitcorp.model;

public class tiposFinanciamientoModel {
	
	
	int tiposdefinanciamiento_id;
	String codigo;
	String descripcion;
	
	
	
	public tiposFinanciamientoModel() {
		super();
	}

	public tiposFinanciamientoModel(int tiposdefinanciamiento_id, String codigo, String descripcion) {
		super();
		this.tiposdefinanciamiento_id = tiposdefinanciamiento_id;
		this.codigo = codigo;
		this.descripcion = descripcion;
	}






	public int getTiposdefinanciamiento_id() {
		return tiposdefinanciamiento_id;
	}






	public void setTiposdefinanciamiento_id(int tiposdefinanciamiento_id) {
		this.tiposdefinanciamiento_id = tiposdefinanciamiento_id;
	}






	public String getCodigo() {
		return codigo;
	}



	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}



	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	
	
}
