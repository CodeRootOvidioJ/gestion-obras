package com.qitcorp.model;

public class tiposObrasModel {
	
	
	int tiposdeobra_id;
	
	public int getTiposdeobra_id() {
		return tiposdeobra_id;
	}

	public tiposObrasModel(int tiposdeobra_id, String codigo, String descripcion) {
		super();
		this.tiposdeobra_id = tiposdeobra_id;
		this.codigo = codigo;
		this.descripcion = descripcion;
	}


	public void setTiposdeobra_id(int tiposdeobra_id) {
		this.tiposdeobra_id = tiposdeobra_id;
	}



	String codigo;
	String descripcion;
	
	
	
	public tiposObrasModel() {
		super();
	}


	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	
}
