package com.qitcorp.model;

public class turnosTrabajoModel {

	String codigo;
	String descripcion;
	String horainicio;
	String horafin;
	Double horastrabajadas;

	int turnosdetrabajo_id;

	public Double getHorastrabajadas() {
		return horastrabajadas;
	}

	public void setHorastrabajadas(Double horastrabajadas) {
		this.horastrabajadas = horastrabajadas;
	}

	public turnosTrabajoModel(String codigo, String descripcion, String horainicio, String horafin,
			Double horastrabajadas, int turnosdetrabajo_id) {
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.horainicio = horainicio;
		this.horafin = horafin;
		this.horastrabajadas = horastrabajadas;
		this.turnosdetrabajo_id = turnosdetrabajo_id;
	}

	public int getTurnosdetrabajo_id() {
		return turnosdetrabajo_id;
	}

	public void setTurnosdetrabajo_id(int turnosdetrabajo_id) {
		this.turnosdetrabajo_id = turnosdetrabajo_id;
	}

	public turnosTrabajoModel() {
		super();
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getHorainicio() {
		return horainicio;
	}

	public void setHorainicio(String horainicio) {
		this.horainicio = horainicio;
	}

	public String getHorafin() {
		return horafin;
	}

	public void setHorafin(String horafin) {
		this.horafin = horafin;
	}

}
