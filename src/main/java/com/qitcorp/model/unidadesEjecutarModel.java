package com.qitcorp.model;

public class unidadesEjecutarModel {

	int categorias_personal_id;

	String codigo;
	String nombre;
	String unidad;
	Double costo;
	int personal_id;
	String nombrecategoriapersonal;

	public unidadesEjecutarModel() {
		super();
	}

	public unidadesEjecutarModel(int categorias_personal_id, String codigo, String nombre, String unidad, Double costo,
			int personal_id, String nombrecategoriapersonal) {
		super();
		this.categorias_personal_id = categorias_personal_id;
		this.codigo = codigo;
		this.nombre = nombre;
		this.unidad = unidad;
		this.costo = costo;
		this.personal_id = personal_id;
		this.nombrecategoriapersonal = nombrecategoriapersonal;
	}

	public String getNombrecategoriapersonal() {
		return nombrecategoriapersonal;
	}

	public void setNombrecategoriapersonal(String nombrecategoriapersonal) {
		this.nombrecategoriapersonal = nombrecategoriapersonal;
	}

	public int getPersonal_id() {
		return personal_id;
	}

	public void setPersonal_id(int personal_id) {
		this.personal_id = personal_id;
	}

	public int getCategorias_personal_id() {
		return categorias_personal_id;
	}

	public void setCategorias_personal_id(int categorias_personal_id) {
		this.categorias_personal_id = categorias_personal_id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public Double getCosto() {
		return costo;
	}

	public void setCosto(Double costo) {
		this.costo = costo;
	}

}