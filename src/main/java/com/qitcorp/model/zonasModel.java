package com.qitcorp.model;

public class zonasModel {
	
	
	
	
	String codigo;
	String descripcion;
	String moneda;
	int zonas_id;
	
	
	
	public zonasModel() {
		super();
	}



	public zonasModel( String codigo, String descripcion, String moneda, int zonas_id) {
		super();
	
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.moneda = moneda;
		this.zonas_id = zonas_id;
	}









	public int getZonas_id() {
		return zonas_id;
	}



	public void setZonas_id(int zonas_id) {
		this.zonas_id = zonas_id;
	}



	public String getCodigo() {
		return codigo;
	}



	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}



	public String getDescripcion() {
		return descripcion;
	}



	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}



	public String getMoneda() {
		return moneda;
	}



	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	
}
