package com.qitcorp.spring;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.zonasDAO;

@Controller
public class HolaController {
	@Autowired
	zonasDAO dao = new zonasDAO();

	@RequestMapping("irHola")
	public ModelAndView redireccion() {
		ModelAndView MV = new ModelAndView();

		MV.setViewName("hola");
		return MV;

	}
}
