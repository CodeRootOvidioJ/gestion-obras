package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.categoriaEquipoDAO;
import com.qitcorp.DAO.categoriaPersonalDAO;
import com.qitcorp.DAO.siProductosDAO;
import com.qitcorp.DAO.zonasDAO;
import com.qitcorp.model.categoriaEquipoModel;
import com.qitcorp.model.categoriaPersonalModel;
import com.qitcorp.model.filialesModel;
import com.qitcorp.model.siProductosModel;
import com.qitcorp.model.zonasModel;

@Controller
// @SessionAttributes("zonas")
// @RequestMapping("irForm")
public class categoriaEquipoController {
	@Autowired
	categoriaEquipoDAO dao = new categoriaEquipoDAO();


	@RequestMapping("listcategoriaequipo")
	public ModelAndView redireccionListaCategoriasEquipos() {

		ModelAndView MV = new ModelAndView();

		List lstcategoriaequipos = dao.obtenerCategoriasEquipos();
		MV.addObject("categoriaequipo", new categoriaEquipoModel());
		System.out.println("total:" + lstcategoriaequipos.size());

		MV.setViewName("categoriaequipos");
		MV.addObject("lstcategoriaequipos", lstcategoriaequipos);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "irAgregaCategoriaequipo")
	public ModelAndView form(@ModelAttribute("categoriaequipo") categoriaEquipoModel u, BindingResult result, SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertCategoriaEquipo(u);

		if (result2) {
			return new ModelAndView("redirect:/listcategoriaequipo.html");

		} else {
			return new ModelAndView("redirect:/listcategoriaequipo.html");
		}

	}
	
	
	
	

	@RequestMapping(method = RequestMethod.POST, value = "modificarcategoriasequipos")
	public ResponseEntity formActualizaCategoriaEquipos(@ModelAttribute("categoriaequipo") categoriaEquipoModel u,
			BindingResult result

	) {
		//int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getCategorias_equipos_id());
		boolean result2 = dao.modificarCategoriaPersonal(u);
		return new ResponseEntity(result2, HttpStatus.OK);
	}

	
	
	@RequestMapping("ireliminarcategoriaequipos")
	public ModelAndView eliminarCategoriaEquipos(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		categoriaEquipoModel categoriaequipo = new categoriaEquipoModel();
		if (dao.eliminarCategoriapersonal(id)) {
			System.out.println("finalizo eliminar id:" + id);
			mav.addObject("categoriaequipo", categoriaequipo);
			mav.setViewName("redirect:/listcategoriaequipo.html");
		}

		return mav;
	}

}
