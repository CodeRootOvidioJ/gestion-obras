package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.categoriaPersonalDAO;
import com.qitcorp.DAO.siProductosDAO;
import com.qitcorp.DAO.zonasDAO;
import com.qitcorp.model.categoriaPersonalModel;
import com.qitcorp.model.filialesModel;
import com.qitcorp.model.siProductosModel;
import com.qitcorp.model.tiposObrasModel;
import com.qitcorp.model.zonasModel;

@Controller
public class categoriaPersonalController {
	@Autowired
	categoriaPersonalDAO dao = new categoriaPersonalDAO();

	@RequestMapping("listCategoriapersonal")
	public ModelAndView redireccionListaCategoriaPersonal() {

		ModelAndView MV = new ModelAndView();

		List lstcategoriapersonal = dao.obtenerCategoriasPersonal();
		MV.addObject("categoriapersonal", new categoriaPersonalModel());
		System.out.println("total:" + lstcategoriapersonal.size());

		MV.setViewName("categoriapersonal");
		MV.addObject("lstcategoriapersonal", lstcategoriapersonal);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "agregarcategoriaspersonal")
	public ModelAndView formAgregaCategoriaPersonal(@ModelAttribute("categoriapersonal") categoriaPersonalModel u,
			BindingResult result, SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertCategoriaPersonal(u);

		if (result2) {
			return new ModelAndView("redirect:/listCategoriapersonal.html");

		} else {
			return new ModelAndView("redirect:/listCategoriapersonal.html");
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "modificarcategoriaspersonal")
	public ResponseEntity formActualizaCategoriaPersonal(@ModelAttribute("categoriapersonal") categoriaPersonalModel u,
			BindingResult result

	) {
		//int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getCategorias_personal_id());
		boolean result2 = dao.modificarCategoriaPersonal(u);
		
		return new ResponseEntity(result2, HttpStatus.OK);
	}
	
	
	
	
	
	

	@RequestMapping("ireliminarcategoriapersonal")
	public ModelAndView eliminarCategoriaPersonal(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		categoriaPersonalModel categoriapersonal = new categoriaPersonalModel();
		if (dao.eliminarCategoriapersonal(id)) {
			System.out.println("finalizo eliminar id:" + id);
			mav.addObject("categoriapersonal", categoriapersonal);
			mav.setViewName("redirect:/listCategoriapersonal.html");
		}

		return mav;
	}

}
