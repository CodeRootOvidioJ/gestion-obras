package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.elementosDAO;
import com.qitcorp.model.elementosModel;

@Controller
public class elementosController {
	@Autowired
	elementosDAO daoelementos = new elementosDAO();

	@RequestMapping("ListElementos")
	public ModelAndView redireccionListaElementos() {

		ModelAndView MV = new ModelAndView();

		List lstsubrubros = daoelementos.obtenerSubRubros();
		List listElementos = daoelementos.obtenerElementos();
		MV.addObject("listElementos", listElementos);
		MV.addObject("elementos", new elementosModel());
		System.out.println("total:" + lstsubrubros.size());

		MV.setViewName("elementos");
		MV.addObject("lstsubrubros", lstsubrubros);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "ListElementos")
	public ModelAndView formularioAgregar(@ModelAttribute("elementos") elementosModel u, BindingResult result, SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = daoelementos.insertElementos(u);

		if (result2) {
			return new ModelAndView("redirect:/ListRubro.html");

		} else {
			return new ModelAndView("redirect:/ListRubro.html");
		}

	}

	@RequestMapping("irEliminarelementos")
	public ModelAndView eliminarElementos(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));

		elementosModel elementos = new elementosModel();
		if (daoelementos.eliminarElemento(id)) {
			System.out.println("elimina:" + id);
			List lstrubros = daoelementos.obtenerElementos();
			mav.addObject("elementos", elementos);
			mav.setViewName("redirect:/ListRubro.html");
		}

		System.out.println("entra form2" + id);
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST, value = "irEditarElementos")
	public ResponseEntity formularioActualiza(@ModelAttribute("elementos") elementosModel u, BindingResult result,
			SessionStatus status

	) {
		//int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getElemento_id());
		boolean result2 = daoelementos.modificaElementos(u);
		return new ResponseEntity(result2, HttpStatus.OK);
	}

}
