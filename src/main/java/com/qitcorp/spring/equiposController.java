package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.categoriaEquipoDAO;
import com.qitcorp.DAO.categoriaPersonalDAO;
import com.qitcorp.DAO.equiposDAO;
import com.qitcorp.DAO.personalDAO;
import com.qitcorp.DAO.siProductosDAO;
import com.qitcorp.DAO.zonasDAO;
import com.qitcorp.model.categoriaPersonalModel;
import com.qitcorp.model.equiposModel;
import com.qitcorp.model.filialesModel;
import com.qitcorp.model.personalModel;
import com.qitcorp.model.siProductosModel;
import com.qitcorp.model.tiposObrasModel;
import com.qitcorp.model.zonasModel;

@Controller
public class equiposController {
	@Autowired
	equiposDAO dao = new equiposDAO();

	@Autowired
	categoriaEquipoDAO daocategoriaequipos = new categoriaEquipoDAO();

	@RequestMapping("ListEquipos")
	public ModelAndView redireccionListaCategoriaPersonal() {

		ModelAndView MV = new ModelAndView();
		List lstcategoriaequipos = daocategoriaequipos.obtenerCategoriasEquipos();
		List lstequipos = dao.obtenerEquipos();
		MV.addObject("equipo", new equiposModel());
		System.out.println("total:" + lstequipos.size());

		MV.setViewName("equipos");
		MV.addObject("lstcategoriaequipos", lstcategoriaequipos);
		MV.addObject("lstequipos", lstequipos);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "agregarequipos")
	public ModelAndView formAgregaEquipos(@ModelAttribute("equipo") equiposModel u, BindingResult result,
			SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertEquipos(u);

		if (result2) {
			return new ModelAndView("redirect:/ListEquipos.html");

		} else {
			return new ModelAndView("redirect:/ListEquipos.html");
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "modificarequipos")
	public ResponseEntity formActualizaEquipos(@ModelAttribute("equipo") equiposModel u, BindingResult result

	) {
		// int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getEquipos_id());
		boolean result2 = dao.modificarEquipos(u);

		return new ResponseEntity(result2, HttpStatus.OK);
	}

	@RequestMapping("ireliminarequipos")
	public ModelAndView eliminarEquipos(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		if (dao.eliminarEquipos(id)) {
			System.out.println("finalizo eliminar id:" + id);
			mav.setViewName("redirect:/ListEquipos.html");
		}

		return mav;
	}

}
