package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.categoriaEquipoDAO;
import com.qitcorp.DAO.categoriaPersonalDAO;
import com.qitcorp.DAO.estadoObraDAO;
import com.qitcorp.DAO.siProductosDAO;
import com.qitcorp.DAO.zonasDAO;
import com.qitcorp.model.categoriaEquipoModel;
import com.qitcorp.model.categoriaPersonalModel;
import com.qitcorp.model.estadoObraModel;
import com.qitcorp.model.siProductosModel;
import com.qitcorp.model.zonasModel;

@Controller
// @SessionAttributes("zonas")
// @RequestMapping("irForm")
public class estadoObraController {
	@Autowired
	estadoObraDAO dao = new estadoObraDAO();

	@RequestMapping("listaestadoobra.html")
	public ModelAndView redireccionListaCompras() {

		ModelAndView MV = new ModelAndView();

		List lstestadoobras = dao.obtenerEstadoObras();
		MV.addObject("estadoobra", new estadoObraModel());
		System.out.println("total:" + lstestadoobras.size());

		MV.setViewName("estadoobra");
		MV.addObject("lstestadoobras", lstestadoobras);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "irAgregaEstadoObra.html")
	public ModelAndView form(@ModelAttribute("estadoobra") estadoObraModel u, BindingResult result, SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertCompras(u);

		if (result2) {
			return new ModelAndView("redirect:/listaestadoobra.html");

		} else {
			return new ModelAndView("redirect:/listaestadoobra.html");
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "modificarcaestadodeobras")
	public ResponseEntity formActualizaEstadoObra(@ModelAttribute("estadoobra") estadoObraModel u, BindingResult result

	) {
		//int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getEstadosdeobra_id());
		boolean result2 = dao.modificarEstadoObra(u);
		return new ResponseEntity(result2, HttpStatus.OK);
	}

	@RequestMapping("ireliminarestadodeobras")
	public ModelAndView eliminarEstadoObra(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		estadoObraModel estadoobra = new estadoObraModel();
		if (dao.eliminarEstadoObra(id)) {
			System.out.println("finalizo eliminar id:" + id);
			mav.addObject("estadoobra", estadoobra);
			mav.setViewName("redirect:/listaestadoobra.html");
		}

		return mav;
	}

}
