package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.filialesDAO;
import com.qitcorp.DAO.zonasDAO;
import com.qitcorp.model.filialesModel;
import com.qitcorp.model.zonasModel;

@Controller
public class filialesController {
	@Autowired
	filialesDAO filialesdao = new filialesDAO();

	@Autowired
	zonasDAO daozonas = new zonasDAO();

	@RequestMapping("irFiliales")
	public ModelAndView redireccionListaCompras() {

		ModelAndView MV = new ModelAndView();
		List lstzonas = daozonas.obtenerZonas();
		
		List lstfiliales = filialesdao.obtenerFiliales();
		
		MV.addObject("filiales", new filialesModel());
		System.out.println("total:" + lstfiliales.size());

		MV.setViewName("filiales");
		MV.addObject("lstfiliales", lstfiliales);
		MV.addObject("lstzonas", lstzonas);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "irFiliales")
	public ModelAndView form(@ModelAttribute("filiales") filialesModel u, BindingResult result, SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = filialesdao.insertfiliales(u);

		if (result2) {
			return new ModelAndView("redirect:/irZonas.html");

		} else {
			return new ModelAndView("redirect:/irZonas.html");
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "irEditarFiliales")
	public ResponseEntity formActualiza(@ModelAttribute("filiales") filialesModel u, BindingResult result

	) {
		//int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("id a modificar: " + u.getFiliales_id());
		boolean result2 ;
		
		result2 = filialesdao.modificarfiliales(u);
		
		
		
		return new ResponseEntity(result2, HttpStatus.OK);
		//return "result2";
	}
	
	
	@RequestMapping("irEliminarFiliales")
	public ModelAndView eliminarFiliales(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		
		//zonasModel zonas = new zonasModel();
		if (filialesdao.eliminarfiliales(id)) {
			System.out.println("elimina filiales"+ id);
			//List lstzonas = dao.obtenerZonas();
			System.out.println("elimina:" + id);
			//mav.addObject("lstzonas", lstzonas);
		//	mav.addObject("zonas", zonas);
			mav.setViewName("redirect:/irZonas.html");
		}

		
		System.out.println("entra form2" + id);
		return mav;
	}
	
	
}
