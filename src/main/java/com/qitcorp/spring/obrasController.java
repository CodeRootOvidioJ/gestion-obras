package com.qitcorp.spring;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.categoriaEquipoDAO;
import com.qitcorp.DAO.categoriaPersonalDAO;
import com.qitcorp.DAO.estadoObraDAO;
import com.qitcorp.DAO.filialesDAO;
import com.qitcorp.DAO.obrasDAO;
import com.qitcorp.DAO.obrasPrevisionDAO;
import com.qitcorp.DAO.periodosDAO;
import com.qitcorp.DAO.rubrosDAO;
import com.qitcorp.DAO.siProductosDAO;
import com.qitcorp.DAO.tipoClienteDAO;
import com.qitcorp.DAO.tipoContratosDAO;
import com.qitcorp.DAO.tipoMercadoDAO;
import com.qitcorp.DAO.tiposFinanciamientoDAO;
import com.qitcorp.DAO.tiposObrasDAO;
import com.qitcorp.DAO.turnosTrabajoDAO;
import com.qitcorp.DAO.zonasDAO;
import com.qitcorp.model.categoriaEquipoModel;
import com.qitcorp.model.categoriaPersonalModel;
import com.qitcorp.model.estadoObraModel;
import com.qitcorp.model.obrasModel;
import com.qitcorp.model.obrasPrevisionModel;
import com.qitcorp.model.siProductosModel;
import com.qitcorp.model.subRubrosModel;
import com.qitcorp.model.zonasModel;

@Controller
public class obrasController {
	@Autowired
	obrasDAO dao = new obrasDAO();
	@Autowired
	periodosDAO daoperiodos = new periodosDAO();
	@Autowired
	rubrosDAO daorubros = new rubrosDAO();
	@Autowired
	obrasPrevisionDAO daoprevision = new obrasPrevisionDAO();

	@RequestMapping("listaobra.html")
	public ModelAndView redireccionListaCompras() {

		ModelAndView MV = new ModelAndView();

		List lstobras = dao.obtenerEstadoObras();
		List lstsobrasprevision = daoprevision.obtenerSemanas();
		MV.addObject("obra", new obrasModel());
		MV.addObject("obrasprevision", new obrasPrevisionModel());
		List lstperiodos = daoperiodos.obtenerPeriodos();
		List lstrubros = daorubros.obtenerRubros();
		System.out.println("total:" + lstobras.size());

		MV.setViewName("obras");
		MV.addObject("lstobras", lstobras);
		MV.addObject("lstsobrasprevision", lstsobrasprevision);
		MV.addObject("lstperiodos", lstperiodos);
		MV.addObject("lstrubros", lstrubros);
		return MV;

	}

	@Autowired
	estadoObraDAO daoestadoobra = new estadoObraDAO();
	@Autowired
	filialesDAO filialesdao = new filialesDAO();
	@Autowired
	tiposObrasDAO daotiposdeobras = new tiposObrasDAO();
	@Autowired
	tipoContratosDAO daotipocontrato = new tipoContratosDAO();
	@Autowired
	tiposFinanciamientoDAO daotipofinanciamiento = new tiposFinanciamientoDAO();
	@Autowired
	tipoMercadoDAO daotipomercado = new tipoMercadoDAO();
	@Autowired
	tipoClienteDAO daotipocliente = new tipoClienteDAO();
	@Autowired
	turnosTrabajoDAO daoturnostrabajo = new turnosTrabajoDAO();

	@RequestMapping("AgregarEstadoObra.html")
	public ModelAndView redireccionagregar() {

		ModelAndView MV = new ModelAndView();
		// lista para llenar los combos
		List lstestadoobras = daoestadoobra.obtenerEstadoObras();
		List lstfiliales = filialesdao.obtenerFiliales();
		List lsttipodeobra = daotiposdeobras.obtenerTipoObra();
		List lsttiposcontratos = daotipocontrato.obtenerTipoContrato();
		List lsttiposfinanciamientos = daotipofinanciamiento.obtenerTipoFinanciamiento();
		List lsttipodemercado = daotipomercado.obtenerTipoMercado();
		List lsttipodecliente = daotipocliente.obtenerTipoCliente();
		List lstturnostrabajo = daoturnostrabajo.obtenerTurnosTrabajo();

		MV.addObject("obra", new obrasModel());
		System.out.println("total:" + lstestadoobras.size());

		MV.setViewName("form");
		MV.addObject("lstestadoobras", lstestadoobras);
		MV.addObject("lstfiliales", lstfiliales);
		MV.addObject("lsttipodeobra", lsttipodeobra);
		MV.addObject("lsttiposcontratos", lsttiposcontratos);
		MV.addObject("lsttiposfinanciamientos", lsttiposfinanciamientos);
		MV.addObject("lsttipodemercado", lsttipodemercado);
		MV.addObject("lsttipodecliente", lsttipodecliente);
		MV.addObject("lstturnostrabajo", lstturnostrabajo);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "agregarformobras.html")
	public ModelAndView form(@ModelAttribute("obra") obrasModel u, BindingResult result, SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertObras(u);

		if (result2) {
			return new ModelAndView("redirect:/listaobra.html");

		} else {
			return new ModelAndView("redirect:/listaobra.html");
		}

	}

	@RequestMapping("irForm2")
	public ModelAndView formModificar(HttpServletRequest request) {
		ModelAndView MV = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		MV.setViewName("formmodificaobra");
		obrasModel obra = new obrasModel();

		obra = dao.obtenerObrasId(id);
		// lista para llenar los combos
		List lstestadoobras = daoestadoobra.obtenerEstadoObras();
		List lstfiliales = filialesdao.obtenerFiliales();
		List lsttipodeobra = daotiposdeobras.obtenerTipoObra();
		List lsttiposcontratos = daotipocontrato.obtenerTipoContrato();
		List lsttiposfinanciamientos = daotipofinanciamiento.obtenerTipoFinanciamiento();
		List lsttipodemercado = daotipomercado.obtenerTipoMercado();
		List lsttipodecliente = daotipocliente.obtenerTipoCliente();
		List lstturnostrabajo = daoturnostrabajo.obtenerTurnosTrabajo();
		// comprasM.setCantidad(5);
		// System.out.println("total:" + lstcompras.size());
		MV.addObject("lstestadoobras", lstestadoobras);
		MV.addObject("lstfiliales", lstfiliales);
		MV.addObject("lsttipodeobra", lsttipodeobra);
		MV.addObject("lsttiposcontratos", lsttiposcontratos);
		MV.addObject("lsttiposfinanciamientos", lsttiposfinanciamientos);
		MV.addObject("lsttipodemercado", lsttipodemercado);
		MV.addObject("lsttipodecliente", lsttipodecliente);
		MV.addObject("lstturnostrabajo", lstturnostrabajo);
		MV.addObject("obra", obra);
		System.out.println("entra form2" + id);
		return MV;
	}

	@RequestMapping(method = RequestMethod.POST, value = "modificarcaobras")
	public ModelAndView formActualizaEstadoObra(@ModelAttribute("obra") obrasModel u, BindingResult result,
			SessionStatus status

	) {
		// int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getObras_id());
		boolean result2 = dao.modificarEstadoObra(u);
		if (result2) {
			return new ModelAndView("redirect:/listaobra.html");

		} else {
			return new ModelAndView("redirect:/listaobra.html");
		}

	}

	@RequestMapping("ireliminarobras")
	public ModelAndView eliminarEstadoObra(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		if (dao.eliminarObra(id)) {
			System.out.println("finalizo eliminar id:" + id);
			mav.setViewName("redirect:/listaobra.html");
		}

		return mav;
	}

	@RequestMapping("irDetalle")
	public ModelAndView detalle(HttpServletRequest request) {
		ModelAndView MV = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		MV.setViewName("obrasdetalle");
		obrasModel obra = new obrasModel();

		obra = dao.obtenerObrasId(id);

		List lstobras = dao.obtenerEstadoObrasId(id);
		MV.addObject("obra", obra);
		System.out.println("entra form2" + id);
		return MV;
	}
	
	
	
	@RequestMapping(method = RequestMethod.GET, value = "listarprevision")
	public ResponseEntity listarSubRubrosId(HttpServletRequest request

	) {
		System.out.println("entra con id" + Integer.parseInt(request.getParameter("id")));
		int id = Integer.parseInt(request.getParameter("id"));

		ArrayList<obrasPrevisionModel> lstsubrubros = dao.obtenerSubRubrosId(id);

		return new ResponseEntity(lstsubrubros, HttpStatus.OK);
	}
	

}
