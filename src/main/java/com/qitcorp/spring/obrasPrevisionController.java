package com.qitcorp.spring;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.obrasPrevisionDAO;
import com.qitcorp.DAO.periodosDAO;
import com.qitcorp.DAO.rubrosDAO;
import com.qitcorp.DAO.semanasDAO;
import com.qitcorp.DAO.subRubrosDAO;
import com.qitcorp.model.obrasModel;
import com.qitcorp.model.obrasPrevisionModel;
import com.qitcorp.model.subRubrosModel;

@Controller
public class obrasPrevisionController {
	
	@Autowired
	obrasPrevisionDAO dao = new obrasPrevisionDAO();

	@Autowired
	periodosDAO daoperiodos = new periodosDAO();
	@Autowired
	rubrosDAO daorubros = new rubrosDAO();
	@Autowired
	subRubrosDAO daosubrubros = new subRubrosDAO();
	
	@RequestMapping("listaobraprevision")
	public ModelAndView redireccionListaSemanas() {

		ModelAndView MV = new ModelAndView();
		List lstperiodos = daoperiodos.obtenerPeriodos();
		List lstsobrasprevision = dao.obtenerSemanas();
		List lstrubros = daorubros.obtenerRubros();
		List lstSubrubros = null;
		
		MV.addObject("obrasprevision", new obrasPrevisionModel());
		System.out.println("total:" + lstsobrasprevision.size());

		MV.setViewName("obrasprevision");
	MV.addObject("lstperiodos", lstperiodos);
		MV.addObject("lstsobrasprevision", lstsobrasprevision);
		MV.addObject("lstrubros", lstrubros);
		MV.addObject("lstSubrubros", lstSubrubros);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "addObrasPrevision")
	public ModelAndView form(@ModelAttribute("obrasprevision") obrasPrevisionModel u, BindingResult result, SessionStatus status

	) {
		System.out.println("subrubro:" + u.getSub_rubro_id());
		System.out.println("elemento:" + u.getElemento_id());
		
		boolean result2 = dao.insertSemanas(u);

		if (result2) {
			return new ModelAndView("redirect:/listaobra.html");

		} else {
			return new ModelAndView("redirect:/listaobra.html");
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "modificarobrasprevision")
	public ResponseEntity formActualizaSemanas(@ModelAttribute("obrasprevision") obrasPrevisionModel u, BindingResult result

	) {
		// int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getObrasprevision_id());
		boolean result2 = dao.modificarSemanas(u);

		return new ResponseEntity(result2, HttpStatus.OK);
	}

	@RequestMapping("ireliminaobrasprevision")
	public ModelAndView eliminarSemanas(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		if (dao.eliminarSemanas(id)) {
			mav.setViewName("redirect:/listaobra.html");
		}

		return mav;
	}
	
	
	@RequestMapping(method = RequestMethod.GET, value = "listarsubrubrosobrasprevision")
	public ResponseEntity listarSubRubrosId(HttpServletRequest request

	) {
		System.out.println("entra con id" + Integer.parseInt(request.getParameter("id")));
		int id = Integer.parseInt(request.getParameter("id"));

		ArrayList<subRubrosModel> lstsubrubros = dao.obtenerSubRubrosId(id);

		return new ResponseEntity(lstsubrubros, HttpStatus.OK);
	}
	
	@RequestMapping("irDetallePrevision")
	public ModelAndView detalle(HttpServletRequest request) {
		ModelAndView MV = new ModelAndView();
		System.out.println("entra abuscar detalle" + request.getParameter("id"));
		int id = Integer.parseInt(request.getParameter("id"));
		MV.setViewName("obrasprevisiondetalle");
		obrasPrevisionModel obrasprevision = new obrasPrevisionModel();

		obrasprevision = dao.obtenerObrasId(id);

		MV.addObject("obrasprevision", obrasprevision);
	
		return MV;
	}

}
