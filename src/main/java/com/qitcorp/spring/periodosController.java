package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.periodosDAO;
import com.qitcorp.model.periodosModel;

@Controller
public class periodosController {
	@Autowired
	periodosDAO dao = new periodosDAO();

	@RequestMapping("ListPeriodos")
	public ModelAndView redireccionListaPeriodos() {

		ModelAndView MV = new ModelAndView();

		List lstperiodos = dao.obtenerPeriodos();
		MV.addObject("periodo", new periodosModel());
		System.out.println("total:" + lstperiodos.size());

		MV.setViewName("periodos");
		MV.addObject("lstperiodos", lstperiodos);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "addPeriodos")
	public ModelAndView form(@ModelAttribute("periodo") periodosModel u, BindingResult result, SessionStatus status

	) {
		boolean result2 = dao.insertPeriodos(u);

		if (result2) {
			return new ModelAndView("redirect:/ListPeriodos.html");

		} else {
			return new ModelAndView("redirect:/ListPeriodos.html");
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "modificarperiodos")
	public ResponseEntity formActualizaPeriodos(@ModelAttribute("periodo") periodosModel u, BindingResult result

	) {
		// int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getPeriodos_id());
		boolean result2 = dao.modificarPeriodos(u);

		return new ResponseEntity(result2, HttpStatus.OK);
	}

	@RequestMapping("ireliminaPeriodos")
	public ModelAndView eliminarTurnosTrabajo(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		if (dao.eliminarPeriodos(id)) {
			mav.setViewName("redirect:/ListPeriodos.html");
		}

		mav.setViewName("redirect:/ListPeriodos.html");
		return mav;
	}

}
