package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.siProductosDAO;
import com.qitcorp.DAO.zonasDAO;
import com.qitcorp.model.siProductosModel;
import com.qitcorp.model.zonasModel;

@Controller
@SessionAttributes("productos")
// @RequestMapping("irForm")
public class productosController {
	@Autowired
	siProductosDAO dao = new siProductosDAO();

	@RequestMapping("irProductos")
	public ModelAndView redireccionListaCompras() {

		ModelAndView MV = new ModelAndView();

		List lstproductos = dao.obtenerproductos();

		System.out.println("total:" + lstproductos.size());
		MV.addObject("lstproductos", lstproductos);
		
		MV.setViewName("productos");
		
		return MV;

	}

	@RequestMapping("irFormProductos")
	public ModelAndView form() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("formproductos");
		mav.addObject("productos", new siProductosModel());
		List lstproductos = dao.obtenerproductos();
		mav.addObject("lstcompras", lstproductos);
		System.out.println("entra1");
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST, value = "irFormProductos")
	public ModelAndView form(@ModelAttribute("productos") siProductosModel u, BindingResult result, SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertProductos(u);

		if (result2) {
			return new ModelAndView("redirect:/irProductos.html");

		} else {
			return new ModelAndView("redirect:/irFormProductos.html");
		}

	}
	
	@RequestMapping("irFormProductos2")
	public ModelAndView formModificar(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		mav.setViewName("formproductos");
		siProductosModel productosM = new siProductosModel();

		productosM = dao.obtenerProductos(id);
		List lstproductos = dao.obtenerproductos();
		// comprasM.setCantidad(5);
		// System.out.println("total:" + lstcompras.size());
		mav.addObject("lstproductos", lstproductos);
		mav.addObject("productos", productosM);
		System.out.println("entra form2" + id);
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST, value = "irFormProductos2")
	public ModelAndView formActualiza(@ModelAttribute("compras") siProductosModel u, BindingResult result,
			SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.modificaTipologiasModal(u);
	
		if (result2) {
			return new ModelAndView("redirect:/irProductos.html");

		} else {
			return new ModelAndView("redirect:/irFormproductos2.html");
		}

	}
	
	
	/*

	@RequestMapping("irEliminarCompras")
	public ModelAndView eliminarCompras(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		List lstcompras = dao.obtenerCompras();
		zonasModel comprasM = new zonasModel();
		if (dao.eliminarCompras(id)) {
			System.out.println("elimina:" + id);
			mav.addObject("lstcompras", lstcompras);
			mav.addObject("compras", comprasM);
			mav.setViewName("redirect:/irCompras.html");
		}
		// comprasM.setCantidad(5);
		
		System.out.println("entra form2" + id);
		return mav;
	}
	*/
}
