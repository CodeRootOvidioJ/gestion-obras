package com.qitcorp.spring;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.qitcorp.DAO.elementosDAO;
import com.qitcorp.DAO.rubrosDAO;
import com.qitcorp.DAO.subRubrosDAO;
import com.qitcorp.model.elementosModel;
import com.qitcorp.model.rubrosModel;
import com.qitcorp.model.subRubrosModel;

@Controller

public class rubrosController {
	@Autowired
	rubrosDAO dao = new rubrosDAO();

	@Autowired
	subRubrosDAO daosubrubros = new subRubrosDAO();

	@Autowired
	elementosDAO daoelementos = new elementosDAO();

	@RequestMapping("ListRubro")
	public ModelAndView redireccionListaCompras() {

		ModelAndView MV = new ModelAndView();

		List lstrubros = dao.obtenerRubros();
		List lstSubrubros = daosubrubros.obtenerSubRubros();

		MV.addObject("rubros", new rubrosModel());
		MV.addObject("subrubros", new subRubrosModel());
		MV.addObject("elementos", new elementosModel());

		System.out.println("total:" + lstrubros.size());

		MV.setViewName("rubros");
		MV.addObject("lstrubros", lstrubros);
		MV.addObject("lstSubrubros", lstSubrubros);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "ListRubro")
	public ModelAndView form(@ModelAttribute("rubros") rubrosModel u, BindingResult result, RedirectAttributes flash,
			SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertRubros(u);

		if (result2) {
			flash.addFlashAttribute("Exito", "El registro se ingreso en la base de datos");
			return new ModelAndView("redirect:/ListRubro.html");

		} else {
			return new ModelAndView("redirect:/ListRubro.html");
		}

	}

	@RequestMapping("irEliminarRubros")
	public ModelAndView eliminarrubros(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("elimina:" + id);
		rubrosModel rubros = new rubrosModel();
		
		dao.eliminarElemento(id);
		dao.eliminarSubRubro(id);
	
		if (dao.eliminarRubro(id)) {
		//	System.out.println("elimina:" + id);
		//	List lstrubros = dao.obtenerRubros();
			//mav.addObject("rubros", rubros);
			mav.setViewName("redirect:/ListRubro.html");
		}

		System.out.println("entra form2" + id);
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST, value = "irEditarRubros")
	public ResponseEntity formActualiza(@ModelAttribute("rubros") rubrosModel u, BindingResult result

	) {

	
	//	int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra con id a modificar rubros:" +u.getRubro_id());
		boolean result2 = dao.modificaRubros(u);
		return new ResponseEntity(result2, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, value = "listarRubros")
	public List<rubrosModel> listarrubrosID(HttpServletRequest request

	) {
		System.out.println("entra");
		int id = Integer.parseInt(request.getParameter("id"));

		List<rubrosModel> lstrubros = dao.obtenerRubrosId(id);

		return lstrubros;
	}

	@RequestMapping(method = RequestMethod.GET, value = "listarsubrubros")
	public ResponseEntity listarSubRubrosId(HttpServletRequest request

	) {
		System.out.println("entra con id" + Integer.parseInt(request.getParameter("id")));
		int id = Integer.parseInt(request.getParameter("id"));

		ArrayList<subRubrosModel> lstsubrubros = daosubrubros.obtenerSubRubrosId(id);

		return new ResponseEntity(lstsubrubros, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, value = "listarelementos")
	public ResponseEntity listarElementosId(HttpServletRequest request

	) {
		System.out.println("entra");
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a buscar elementos con sub rubros id : " + id);
		ArrayList<elementosModel> listElementos = daoelementos.obtenerElementosId(id);

		return new ResponseEntity(listElementos, HttpStatus.OK);
	}

}
