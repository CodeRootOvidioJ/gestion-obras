package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.periodosDAO;
import com.qitcorp.DAO.semanasDAO;
import com.qitcorp.model.semanasModel;

@Controller
public class semanasController {
	@Autowired
	semanasDAO dao = new semanasDAO();

	@Autowired
	periodosDAO daoperiodos = new periodosDAO();

	@RequestMapping("ListSemanas")
	public ModelAndView redireccionListaSemanas() {

		ModelAndView MV = new ModelAndView();
		List lstperiodos = daoperiodos.obtenerPeriodos();
		List lstsemanas = dao.obtenerSemanas();
		MV.addObject("semana", new semanasModel());
		System.out.println("total:" + lstsemanas.size());

		MV.setViewName("semanas");
		MV.addObject("lstperiodos", lstperiodos);
		MV.addObject("lstsemanas", lstsemanas);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "addSemanas")
	public ModelAndView form(@ModelAttribute("semana") semanasModel u, BindingResult result, SessionStatus status

	) {
		boolean result2 = dao.insertSemanas(u);

		if (result2) {
			return new ModelAndView("redirect:/ListSemanas.html");

		} else {
			return new ModelAndView("redirect:/ListSemanas.html");
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "modificarsemanas")
	public ResponseEntity formActualizaSemanas(@ModelAttribute("semana") semanasModel u, BindingResult result

	) {
		// int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getSemanas_id());
		boolean result2 = dao.modificarSemanas(u);

		return new ResponseEntity(result2, HttpStatus.OK);
	}

	@RequestMapping("ireliminaSemanas")
	public ModelAndView eliminarSemanas(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		if (dao.eliminarSemanas(id)) {
			mav.setViewName("redirect:/ListSemanas.html");
		}

		return mav;
	}

}
