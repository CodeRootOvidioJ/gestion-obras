package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.subRubrosDAO;
import com.qitcorp.model.rubrosModel;
import com.qitcorp.model.subRubrosModel;

@Controller
public class subRubrosController {
	@Autowired
	subRubrosDAO daosubrubros = new subRubrosDAO();

	@RequestMapping("ListSubRubro")
	public ModelAndView redireccionListaCompras() {

		ModelAndView MV = new ModelAndView();

		List lstsubrubros = daosubrubros.obtenerSubRubros();
		MV.addObject("subrubros", new subRubrosModel());
		System.out.println("total:" + lstsubrubros.size());

		MV.setViewName("subrubros");
		MV.addObject("lstsubrubros", lstsubrubros);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "ListSubRubro")
	public ModelAndView form(@ModelAttribute("subrubros") subRubrosModel u, BindingResult result, SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = daosubrubros.insertSubRubros(u);

		if (result2) {
			return new ModelAndView("redirect:/ListRubro.html");

		} else {
			return new ModelAndView("redirect:/ListRubro.html");
		}

	}

	@RequestMapping("irEliminarSubRubros")
	public ModelAndView eliminarSubRubros(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));

		rubrosModel rubros = new rubrosModel();
		daosubrubros.eliminarElemento(id);
		if (daosubrubros.eliminarSubRubro(id)) {
			System.out.println("elimina:" + id);
			List lstrubros = daosubrubros.obtenerSubRubros();
			mav.addObject("rubros", rubros);
			mav.setViewName("redirect:/ListRubro.html");
		}

		System.out.println("entra form2" + id);
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST, value = "irEditarSubRubros")
	public ResponseEntity formActualiza(@ModelAttribute("subrubros") subRubrosModel u, BindingResult result

	) {
		//int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getSub_rubro_id());
		boolean result2 = daosubrubros.modificaSubRubros(u);
		return new ResponseEntity(result2, HttpStatus.OK);
	}

}
