package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.siProductosDAO;
import com.qitcorp.DAO.tipoClienteDAO;
import com.qitcorp.DAO.tiposObrasDAO;
import com.qitcorp.DAO.zonasDAO;
import com.qitcorp.model.estadoObraModel;
import com.qitcorp.model.siProductosModel;
import com.qitcorp.model.tipoClienteModel;
import com.qitcorp.model.tipoGarantiaModel;
import com.qitcorp.model.tiposObrasModel;
import com.qitcorp.model.zonasModel;

@Controller
// @SessionAttributes("zonas")
// @RequestMapping("irForm")
public class tipoClienteController {
	@Autowired
	tipoClienteDAO dao = new tipoClienteDAO();


	@RequestMapping("ListTipoClientes")
	public ModelAndView redireccionListaCompras() {

		ModelAndView MV = new ModelAndView();

		List lsttipodecliente = dao.obtenerTipoCliente();
		MV.addObject("tipodecliente", new tipoClienteModel());
		System.out.println("total:" + lsttipodecliente.size());

		MV.setViewName("tipocliente");
		MV.addObject("lsttipodecliente", lsttipodecliente);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "irAgregaTipoCliente")
	public ModelAndView form(@ModelAttribute("tipodecliente") tipoClienteModel u, BindingResult result, SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertTipoCliente(u);

		if (result2) {
			return new ModelAndView("redirect:/ListTipoClientes.html");

		} else {
			return new ModelAndView("redirect:/ListTipoClientes.html");
		}

	}
	

	@RequestMapping(method = RequestMethod.POST, value = "modificarcatipocliente")
	public ResponseEntity formActualizaEstadoObra(@ModelAttribute("tipodecliente") tipoClienteModel u, BindingResult result

	) {
		//int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getTiposdecliente_id());
		boolean result2 = dao.modificaTipoCliente(u);
		return new ResponseEntity(result2, HttpStatus.OK);
	}

	@RequestMapping("ireliminartipocliente")
	public ModelAndView eliminarEstadoObra(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		estadoObraModel estadoobra = new estadoObraModel();
		if (dao.eliminarEstadoObra(id)) {
			System.out.println("finalizo eliminar id:" + id);
			mav.addObject("estadoobra", estadoobra);
			mav.setViewName("redirect:/ListTipoClientes.html");
		}

		return mav;
	}


}
