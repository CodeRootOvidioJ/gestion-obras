package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.tipoContratosDAO;
import com.qitcorp.model.tipoContratoModel;

@Controller
public class tipoContratosController {
	@Autowired
	tipoContratosDAO dao = new tipoContratosDAO();

	@RequestMapping("listatipocontrato")
	public ModelAndView redireccionListaTipoContrato() {

		ModelAndView MV = new ModelAndView();

		List lsttiposcontratos = dao.obtenerTipoContrato();
		MV.addObject("tipocontratos", new tipoContratoModel());
		System.out.println("total:" + lsttiposcontratos.size());

		MV.setViewName("tipocontratos");
		MV.addObject("lsttiposcontratos", lsttiposcontratos);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "irAgregaTipoContrato")
	public ModelAndView form(@ModelAttribute("tipocontratos") tipoContratoModel u, BindingResult result,
			SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertTipoContrato(u);

		if (result2) {
			return new ModelAndView("redirect:/listatipocontrato.html");

		} else {
			return new ModelAndView("redirect:/listatipocontrato.html");
		}

	}

	@RequestMapping("irEliminarTipoContratos")
	public ModelAndView eliminarTiposContratos(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		// List lsttiposcontratos = dao.obtenerCompras();
		tipoContratoModel tipocontratoM = new tipoContratoModel();
		if (dao.eliminarTipoContrato(id)) {
			System.out.println("elimina:" + id);
			// mav.addObject("lsttiposcontratos", lsttiposcontratos);
			mav.addObject("tipocontratos", tipocontratoM);
			mav.setViewName("redirect:/listatipocontrato.html");
		}
		// comprasM.setCantidad(5);

		System.out.println("entra form2" + id);
		return mav;
	}

	@RequestMapping(method = RequestMethod.POST, value = "modificarcatipocontrato")
	public ResponseEntity formActualizaTipoContrato(@ModelAttribute("tipocontratos") tipoContratoModel u,
			BindingResult result

	) {
		//int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getTipocontrato_id());
		boolean result2 = dao.modificarTipoContrato(u);
		return new ResponseEntity(result2, HttpStatus.OK);
	}

}
