package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.tipoGarantiaDAO;
import com.qitcorp.model.tipoGarantiaModel;

@Controller
public class tipoGarantiaController {
	@Autowired
	tipoGarantiaDAO dao = new tipoGarantiaDAO();


	@RequestMapping("ListGarantia")
	public ModelAndView redireccionListaCompras() {

		ModelAndView MV = new ModelAndView();

		List lsttipodegarantia = dao.obtenerGarantia();
		MV.addObject("tipodegarantia", new tipoGarantiaModel());
		System.out.println("total:" + lsttipodegarantia.size());

		MV.setViewName("tipogarantia");
		MV.addObject("lsttipodegarantia", lsttipodegarantia);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "agregartiposgarantias")
	public ModelAndView form(@ModelAttribute("tipodegarantia") tipoGarantiaModel u, BindingResult result,
			SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertGarantia(u);

		if (result2) {
			return new ModelAndView("redirect:/ListGarantia.html");

		} else {
			return new ModelAndView("redirect:/ListGarantia.html");
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "modificartiposgarantias")
	public ResponseEntity formActualiza(@ModelAttribute("tipodegarantia") tipoGarantiaModel u, BindingResult result

	) {
		// int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getTiposdegarantia_id());
		boolean result2 = dao.modificarTiposGarantia(u);

		return new ResponseEntity(result2, HttpStatus.OK);
	}

	@RequestMapping("ireliminartipogarantias")
	public ModelAndView eliminarTiposGarantia(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		tipoGarantiaModel tipodegarantia = new tipoGarantiaModel();
		if (dao.eliminarTiposGarantia(id)) {
			mav.addObject("tipodegarantia", tipodegarantia);
			mav.setViewName("redirect:/ListGarantia.html");
		}

		return mav;
	}

}
