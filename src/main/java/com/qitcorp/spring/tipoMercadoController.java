package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.siProductosDAO;
import com.qitcorp.DAO.tipoMercadoDAO;
import com.qitcorp.DAO.tiposFinanciamientoDAO;
import com.qitcorp.DAO.tiposObrasDAO;
import com.qitcorp.DAO.zonasDAO;
import com.qitcorp.model.estadoObraModel;
import com.qitcorp.model.siProductosModel;
import com.qitcorp.model.tipoClienteModel;
import com.qitcorp.model.tipoGarantiaModel;
import com.qitcorp.model.tipoMercadoModel;
import com.qitcorp.model.tiposObrasModel;
import com.qitcorp.model.zonasModel;

@Controller
// @SessionAttributes("zonas")
// @RequestMapping("irForm")
public class tipoMercadoController {
	@Autowired
	tipoMercadoDAO dao = new tipoMercadoDAO();

	@Autowired
	tiposFinanciamientoDAO daofinanciamiento = new tiposFinanciamientoDAO();

	@RequestMapping("ListTipoMercado.html")
	public ModelAndView redireccionListaCompras() {

		ModelAndView MV = new ModelAndView();
		List lsttiposfinanciamientos = daofinanciamiento.obtenerTipoFinanciamiento();
		
		List lsttipodemercado = dao.obtenerTipoMercado();
		MV.addObject("tipodemercado", new tipoMercadoModel());
		System.out.println("total:" + lsttipodemercado.size());
		MV.addObject("lsttiposfinanciamientos", lsttiposfinanciamientos);

		MV.setViewName("tipomercado");
		MV.addObject("lsttipodemercado", lsttipodemercado);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "irAgregaTipoMercado")
	public ModelAndView form(@ModelAttribute("tipodemercado") tipoMercadoModel u, BindingResult result, SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertTipoMercado(u);

		if (result2) {
			return new ModelAndView("redirect:/ListTipoMercado.html");

		} else {
			return new ModelAndView("redirect:/ListTipoMercado.html");
		}

	}
	

	 @RequestMapping(method = RequestMethod.POST, value = "modificarcatipomercado")
	public ResponseEntity formActualizaEstadoObra(@ModelAttribute("tipodemercado") tipoMercadoModel u, BindingResult result

	) {
		//int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getTiposdemercado_id());
		boolean result2 = dao.modificaTipomercado(u);
		return new ResponseEntity(result2, HttpStatus.OK);
	}

	@RequestMapping("ireliminartipomercado")
	public ModelAndView eliminarTipoMercado(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		if (dao.eliminarTipoMercado(id)) {
			System.out.println("finalizo eliminar id:" + id);
			mav.setViewName("redirect:/ListTipoMercado.html");
		}

		return mav;
	}



}
