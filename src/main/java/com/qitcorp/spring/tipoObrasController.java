package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import com.qitcorp.DAO.tiposObrasDAO;
import com.qitcorp.model.filialesModel;
import com.qitcorp.model.tiposObrasModel;

@Controller
public class tipoObrasController {
	@Autowired
	tiposObrasDAO dao = new tiposObrasDAO();
	
	@RequestMapping("irTiposObras")
	public ModelAndView redireccionListaCompras() {

		ModelAndView MV = new ModelAndView();

		List lsttipodeobra = dao.obtenerTipoObra();
		MV.addObject("tipodeobra", new tiposObrasModel());
		System.out.println("total:" + lsttipodeobra.size());

		MV.setViewName("tipodeobra");
		MV.addObject("lsttipodeobra", lsttipodeobra);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "agregartiposobras")
	public ModelAndView form(@ModelAttribute("tipodeobra") tiposObrasModel u, BindingResult result, SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertCompras(u);

		if (result2) {
			return new ModelAndView("redirect:/irTiposObras.html");

		} else {
			return new ModelAndView("redirect:/irTiposObras.html");
		}

	}
	
	
	
	@RequestMapping(method = RequestMethod.POST, value = "modificartiposobras")
	public ResponseEntity formActualiza(@ModelAttribute("tipodeobra") tiposObrasModel u, BindingResult result

	) {
	//	int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:"+ u.getTiposdeobra_id());
		boolean result2 = dao.modificarTiposObras(u);
		
		return new ResponseEntity(result2, HttpStatus.OK);
	}
	
	

	
	
	@RequestMapping("ireliminartipoobra")
	public ModelAndView eliminarTipoObras(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		tiposObrasModel tipodeobra = new tiposObrasModel();
		if (dao.eliminarTipoObra(id)) {
			mav.addObject("tipodeobra", tipodeobra);
			mav.setViewName("redirect:/irTiposObras.html");
		}
		
		return mav;
	}
	
}
