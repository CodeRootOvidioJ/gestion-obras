package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.tiposFinanciamientoDAO;
import com.qitcorp.model.tiposFinanciamientoModel;

@Controller
public class tiposFinanciamientoController {
	@Autowired
	tiposFinanciamientoDAO dao = new tiposFinanciamientoDAO();

	@RequestMapping("listafinanciamiento")
	public ModelAndView redireccionListTipoFinanciamiento() {

		ModelAndView MV = new ModelAndView();

		List lsttiposfinanciamientos = dao.obtenerTipoFinanciamiento();
		MV.addObject("tiposfinanciamiento", new tiposFinanciamientoModel());
		System.out.println("total:" + lsttiposfinanciamientos.size());

		MV.setViewName("tiposfinanciamiento");
		MV.addObject("lsttiposfinanciamientos", lsttiposfinanciamientos);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "irAgregaTtipofinanciamiento")
	public ModelAndView form(@ModelAttribute("tiposfinanciamiento") tiposFinanciamientoModel u, BindingResult result,
			SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertTipoFinanciamiento(u);

		if (result2) {
			return new ModelAndView("redirect:/listafinanciamiento.html");

		} else {
			return new ModelAndView("redirect:/listafinanciamiento.html");
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "modificarcatipofinanciamiento")
	public ResponseEntity formActualizaFinanciamiento(@ModelAttribute("tiposfinanciamiento") tiposFinanciamientoModel u,
			BindingResult result

	) {
		//int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getTiposdefinanciamiento_id());
		boolean result2 = dao.modificarFinanciamiento(u);
		return new ResponseEntity(result2, HttpStatus.OK);
	}

	@RequestMapping("ireliminartipofinanciamiento")
	public ModelAndView eliminarFinanciamiento(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		tiposFinanciamientoModel tiposfinanciamiento = new tiposFinanciamientoModel();
		if (dao.eliminarFinanciamiento(id)) {
			System.out.println("finalizo eliminar id:" + id);
			mav.addObject("tiposfinanciamiento", tiposfinanciamiento);
			mav.setViewName("redirect:/listafinanciamiento.html");
		}

		return mav;
	}

}
