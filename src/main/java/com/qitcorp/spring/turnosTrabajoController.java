package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.categoriaEquipoDAO;
import com.qitcorp.DAO.categoriaPersonalDAO;
import com.qitcorp.DAO.estadoObraDAO;
import com.qitcorp.DAO.siProductosDAO;
import com.qitcorp.DAO.tipoContratosDAO;
import com.qitcorp.DAO.tiposFinanciamientoDAO;
import com.qitcorp.DAO.turnosTrabajoDAO;
import com.qitcorp.DAO.zonasDAO;
import com.qitcorp.model.categoriaEquipoModel;
import com.qitcorp.model.categoriaPersonalModel;
import com.qitcorp.model.estadoObraModel;
import com.qitcorp.model.siProductosModel;
import com.qitcorp.model.tipoContratoModel;
import com.qitcorp.model.tipoGarantiaModel;
import com.qitcorp.model.tiposFinanciamientoModel;
import com.qitcorp.model.turnosTrabajoModel;
import com.qitcorp.model.zonasModel;

@Controller
// @SessionAttributes("zonas")
// @RequestMapping("irForm")
public class turnosTrabajoController {
	@Autowired
	turnosTrabajoDAO dao = new turnosTrabajoDAO();

	@RequestMapping("ListTurnosTrabajo")
	public ModelAndView redireccionListaTurnosTrabajo() {

		ModelAndView MV = new ModelAndView();

		List lstturnostrabajo = dao.obtenerTurnosTrabajo();
		MV.addObject("turnostrabajo", new turnosTrabajoModel());
		System.out.println("total:" + lstturnostrabajo.size());

		MV.setViewName("turnostrabajo");
		MV.addObject("lstturnostrabajo", lstturnostrabajo);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "addTurnosTrabajo")
	public ModelAndView form(@ModelAttribute("turnostrabajo") turnosTrabajoModel u, BindingResult result,
			SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertTurnosTrabajo(u);

		if (result2) {
			return new ModelAndView("redirect:/ListTurnosTrabajo.html");

		} else {
			return new ModelAndView("redirect:/ListTurnosTrabajo.html");
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "modificarturnostrabajo")
	public ResponseEntity formActualiza(@ModelAttribute("turnostrabajo") turnosTrabajoModel u, BindingResult result

	) {
		// int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getTurnosdetrabajo_id());
		boolean result2 = dao.modificarTurnosTrabajo(u);

		return new ResponseEntity(result2, HttpStatus.OK);
	}

	@RequestMapping("ireliminarturnostrabajo")
	public ModelAndView eliminarTurnosTrabajo(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		turnosTrabajoModel turnostrabajo = new turnosTrabajoModel();
		if (dao.eliminarTurnosTrabajo(id)) {
			mav.addObject("turnostrabajo", turnostrabajo);
			mav.setViewName("redirect:/ListTurnosTrabajo.html");
		}

		return mav;
	}

}
