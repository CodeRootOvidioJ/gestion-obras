package com.qitcorp.spring;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.categoriaPersonalDAO;
import com.qitcorp.DAO.personalDAO;
import com.qitcorp.model.personalModel;

@Controller
public class unidadesEjecutarController {
	/*
	@Autowired
	personalDAO dao = new personalDAO();

	@Autowired
	categoriaPersonalDAO daocategoriapersonal = new categoriaPersonalDAO();

	@RequestMapping("ListPersonal")
	public ModelAndView redireccionListaCategoriaPersonal() {

		ModelAndView MV = new ModelAndView();
		List lstcategoriapersonal = daocategoriapersonal.obtenerCategoriasPersonal();
		List lstpersonal = dao.obtenerPersonal();
		MV.addObject("personal", new personalModel());
		System.out.println("total:" + lstpersonal.size());

		MV.setViewName("personal");
		MV.addObject("lstcategoriapersonal", lstcategoriapersonal);
		MV.addObject("lstpersonal", lstpersonal);
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "agregarpersonal")
	public ModelAndView formAgregaPersonal(@ModelAttribute("personal") personalModel u, BindingResult result,
			SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertPersonal(u);

		if (result2) {
			return new ModelAndView("redirect:/ListPersonal.html");

		} else {
			return new ModelAndView("redirect:/ListPersonal.html");
		}

	}

	@RequestMapping(method = RequestMethod.POST, value = "modificarpersonal")
	public ResponseEntity formActualizaPersonal(@ModelAttribute("personal") personalModel u, BindingResult result

	) {
		// int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra a modificar con id:" + u.getCategorias_personal_id());
		boolean result2 = dao.modificarPersonal(u);

		return new ResponseEntity(result2, HttpStatus.OK);
	}

	@RequestMapping("ireliminarpersonal")
	public ModelAndView eliminarCategoriaPersonal(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("entra eliminar id:" + id);
		if (dao.eliminarpersonal(id)) {
			System.out.println("finalizo eliminar id:" + id);
			mav.setViewName("redirect:/ListPersonal.html");
		}

		return mav;
	}
*/
}
