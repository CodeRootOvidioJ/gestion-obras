package com.qitcorp.spring;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.qitcorp.DAO.filialesDAO;
import com.qitcorp.DAO.siProductosDAO;
import com.qitcorp.DAO.zonasDAO;
import com.qitcorp.model.filialesModel;
import com.qitcorp.model.zonasModel;

@Controller
public class zonasController {
	@Autowired
	filialesDAO filialesdao = new filialesDAO();
	
	@Autowired
	zonasDAO dao = new zonasDAO();


	@RequestMapping("irZonas")
	public ModelAndView redireccionListaCompras() {

		ModelAndView MV = new ModelAndView();

		List lstzonas = dao.obtenerZonas();
		MV.addObject("zonas", new zonasModel());
		System.out.println("total:" + lstzonas.size());

		MV.setViewName("zonas");
		MV.addObject("lstzonas", lstzonas);
		
		
		//para mantenimiento de filiales
		//List lstfiliales = filialesdao.obtenerFiliales();
		MV.addObject("filiales", new filialesModel());
	//	System.out.println("total filiales:" + lstfiliales.size());

	//	MV.addObject("lstfiliales", lstfiliales);
		
		//fin
		
		return MV;

	}

	@RequestMapping(method = RequestMethod.POST, value = "irZonas")
	public ModelAndView form(@ModelAttribute("zonas") zonasModel u, BindingResult result, SessionStatus status

	) {
		System.out.println("entra");
		boolean result2 = dao.insertZonas(u);

		if (result2) {
			return new ModelAndView("redirect:/irZonas.html");

		} else {
			return new ModelAndView("redirect:/irZonas.html");
		}

	}
	
	@RequestMapping("irEliminarZonas")
	public ModelAndView eliminarZonas(HttpServletRequest request) {
		ModelAndView mav = new ModelAndView();
		int id = Integer.parseInt(request.getParameter("id"));
		
		zonasModel zonas = new zonasModel();
		dao.eliminarfiliales(id);
		if (dao.eliminarZonas(id)) {
			System.out.println("elimina filiales"+ id);
			
			System.out.println("elimina zonas" + id);
			List lstzonas = dao.obtenerZonas();
			System.out.println("elimina:" + id);
			mav.addObject("lstzonas", lstzonas);
			mav.addObject("zonas", zonas);
			mav.setViewName("redirect:/irZonas.html");
		}

		
		System.out.println("entra form2" + id);
		return mav;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "irEditarzonas")
	public ResponseEntity formActualiza(@ModelAttribute("zonas") zonasModel u, BindingResult result
			

	) {
		System.out.println("zona id:" + u.getZonas_id());
		boolean result2 = dao.modificaZonas(u);
		
		
		return new ResponseEntity(result2, HttpStatus.OK);
	}
	

	

	
	
	@RequestMapping(method = RequestMethod.GET, value = "listarfiliales")
	public ResponseEntity  listarfilialesId(HttpServletRequest request

	) {
	
		int id = Integer.parseInt(request.getParameter("id"));
		System.out.println("id a buscar: "+ id);
		ArrayList<filialesModel> lstfiliales = dao.obtenerfilialesId(id);
		
		return new ResponseEntity(lstfiliales, HttpStatus.OK);
	}
	
}
