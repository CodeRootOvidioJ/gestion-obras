<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"  %>


<%@page contentType="text/html"%> 
<%@page pageEncoding="UTF-8"%> 
 
<html>
    <head>
   <meta  charset=UTF-8"> 
      <title>Gestion de obras</title>
 <!--Import Google Icon Font-->
      <link type="text/css" href="public/css/icon.css" rel="stylesheet">
     
      <!--Import materialize.css-->
      <link rel="stylesheet" href="public/css/materialize.min.css"  media="screen,projection"/>
        
        <link type="text/css" href="public/css/main.css" rel="stylesheet">

  <link type="text/css" href="public/css/jquery.dataTables.min.css" rel="stylesheet">
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      
</head>
<body>
 <div class="navbar-fixed hide-on-med-and-down">
            <nav class="indigo" role="navigation">
                <div class="nav-wrapper container"><a id="logo-container" href="irIndex.html" class="brand-logo">GESTION DE OBRAS</a>
                  <ul class="right hide-on-med-and-down">
                    <li><a href="" class="white-text bold"></a></li>
                    <li><a href="" class="white-text bold"></a></li>
                  </ul>
                </div>
            </nav>
        </div>
        
        <div class="row">
            <div class="col s1">
                <ul id="slide-out" class="side-nav fixed grey darken-4">
                    <li><div class="user-view">
                      <div class="background">
                        <img src="http://materializecss.com/images/office.jpg">
                      </div>
                      <a href="#!user"><img class="circle" src="public/images/userempresa.jpg"></a>
                      <a href="#!name"><span class="white-text name">Administrador</span></a>
                      <a href="#!email"><span class="white-text email">admin@gmail.com</span></a>
                    </div></li>
                    <li><a href="irIndex.html" class="white-text"><i class="material-icons white-text">assignment</i>Inicio</a></li>
                    <li><div class="divider"></div></li>

                   <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a class="collapsible-header white-text grey darken-4" href="irZonas.html"><i class="material-icons white-text">local_play</i>Zonas</a>
                         
                         </li>
                       </ul>
                   </li>

              



                   <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="irTiposObras.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">perm_media</i>Tipos de obras</a>
                         
                         </li>
                       </ul>
                   </li>
                   
                    <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="listCategoriapersonal.html"  class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">group</i>Categoria Personal</a>
                         
                         </li>
                       </ul>
                   </li>
                   
                   
                    <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a   href="listcategoriaequipo.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">assignment_ind</i>Categoria equipos</a>
                         
                         </li>
                       </ul>
                   </li>


   <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="listaestadoobra.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">poll</i>Estado de Obra</a>
                          
                         </li>
                       </ul>
                   </li>
                   
                   
                      <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="listafinanciamiento.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">monetization_on</i>Tipos de financiamiento</a>
                         
                         </li>
                       </ul>
                   </li>
                   
                   
                   
                   <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="listatipocontrato.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">receipt</i>Tipos de contrato</a>
                          
                         </li>
                       </ul>
                   </li>
                   
                     <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="ListTurnosTrabajo.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">chrome_reader_mode</i>Turnos de trabajo</a>
                          
                         </li>
                       </ul>
                   </li>
                   
                     <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="ListTipoClientes.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">person_pin</i>Tipos de clientes</a>
                          
                         </li>
                       </ul>
                   </li>
                   
                    <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="ListGarantia.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">assignment_late</i>Tipos de garantia</a>
                        
                         </li>
                       </ul>
                   </li>
                   
                     <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="ListTipoMercado.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">business</i>Tipos de mercado</a>
                          
                         </li>
                       </ul>
                   </li>
                   
                   
                    <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="ListRubro.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">view_list</i>Rubros</a>
                          
                         </li>
                       </ul>
                   </li>
                   
                   
                     <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="ListPeriodos.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">call_to_action</i>Periodos</a>
                         
                         </li>
                       </ul>
                   </li>
                   <!--  
                    <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="ListSemanas.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">event_note</i>Semanas</a>
                         
                         </li>
                       </ul>
                   </li>
             
                     <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="ListPersonal.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">perm_contact_calendar</i>Personal</a>
                         
                         </li>
                       </ul>
                   </li>
                 
                   
                         <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="ListEquipos.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">supervisor_account</i>Equipos</a>
                         
                         </li>
                       </ul>
                   </li>
                           -->
                      <li class="no-padding">
                       <ul class="collapsible collapsible-accordion">
                         <li>
                           <a href="listaobra.html" class="collapsible-header white-text grey darken-4"><i class="material-icons white-text">view_compact</i>Obras</a>
                         
                         </li>
                       </ul>
                   </li>
               
                   
                   
                   <li><a href="#!" class="white-text"></a></li>


                   <li><a class="subheader white-text"></a></li>
                   <li><a class="waves-effect white-text" href="#!"></a></li>
               </ul>

                  <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">Inicio</i></a>
            </div>
          
       
       
       
       
       
       
       
	<div id="modal-error" class="modal small center-align  clasemodal_confirm_css ">
		<div class="modal-content">
			<h5 class="center-align">Error al realizar la accion</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_error_modificar" href="#!"
				class="btn red btn-custom center-align" align="center">OK</a>
		</div>
	</div>
       
       
       
        