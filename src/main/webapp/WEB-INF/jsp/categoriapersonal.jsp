<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="col s9  offset-s3 center-align" align="center">

	<h5 class="clase1" align="center">Categorias de personal</h5>

	
	<div class="fixed-action-btn">
		<a class="btn-floating btn-large blue modal-trigger"
			href="#modalAgregar"> <i class="large material-icons">add</i>
		</a>

	</div>


	<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tableCategoriapersonal">
			<thead class="color-thead">
				<tr>
					<th>Codigo</th>
					<th>Descripcion</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lstcategoriapersonal}"
					var="lstcategoriapersonal">
					<tr>
						<td><c:out value="${lstcategoriapersonal.codigo}" /></td>
						<td><c:out value="${lstcategoriapersonal.descripcion}" /></td>




						<td><a class="categoriapersonal_edit" href="#"
							data-id="<c:out value="${lstcategoriapersonal.categorias_personal_id}"/>"
							data-codigo="<c:out value="${lstcategoriapersonal.codigo}"/>"
							data-name="<c:out value="${lstcategoriapersonal.descripcion}"/>" >
								<i class="material-icons small icon-custom">edit</i>
						</a> <a class="categoriapersonal_del" href="#"
							data-id="<c:out value="${lstcategoriapersonal.categorias_personal_id}"/>"
							data-name="<c:out value="${lstcategoriapersonal.descripcion}"/>" >
								<i class="material-icons small icon-custom red-text">delete</i>
						</a></td>


					</tr>

				</c:forEach>



			</tbody>
		</table>
	</div>

	<div id="modal_actions_categoria_personal" class="modal small">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del_categoria_personal" href=""
				class="btn blue btn-custom">Aceptar</a> <a href="#!"
				class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>


	<div id="modal_confirm" class="modal small center-align">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>



	<!-- Modal Structure agregar -->
	<div id="modalAgregar" class="modal modal-fixed-footer clasemodal_add_mod">
	<form:form method="post" commandName="categoriapersonal"
					action="agregarcategoriaspersonal.html">
		<div class="modal-content">

		

				<h5 class="lighten-5"
					align="center">Categorias Personal</h5>
		
			
				


					<form:errors path="*" element="div" cssClass="alert alert-danger" />
					<div class="input-field col s12" >
						<i class="material-icons prefix black-text">dialpad</i>
						<form:input path="codigo"  cssClass="validate black-text"
							required="true" data-length="10" maxlength="9" />
						<form:label path="codigo">codigo:</form:label>
					</div>


					<div class="input-field col s12">
						<i class="material-icons prefix black-text">turned_in</i>
						<form:label path="descripcion">Descripcion: </form:label>
						<form:input path="descripcion" cssClass="validate black-text"
							required="true" data-length="50" maxlength="50" />
					</div>

					
				

			</div>

	<div class="modal-footer col s12" >
						<input type="submit" value="Guardar" class="btn blue btn-custom" />
						<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
					</div>
</form:form>
	</div>


	<!-- Modal Structure para editar -->
	<div id="modalmodificar" class="modal modal-fixed-footer clasemodal_add_mod">
	<form:form commandName="categoriapersonal" id="formedit"
					action="modificarcategoriaspersonal.html">
		<div class="modal-content">

			

				<h5 class="lighten-5"
					align="center">Categorias Personal</h5>

		
	
				


					<form:errors path="*" element="div" cssClass="alert alert-danger" />

					<div class="input-field col s12" >
						<p>Codigo:</p>
						<form:label path="codigo">
						</form:label>
						<i class="material-icons prefix black-text">perm_identity</i>
						<form:input path="codigo" id="txtcodigo"
							cssClass="validate black-text" required="true" data-length="10" maxlength="9" />


					</div>

					<div class="input-field col s12">
						<p>Descripcion:</p>
						<form:label path="descripcion"></form:label>
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:input path="descripcion" id="txtdescripcion"
							cssClass="validate black-text" required="true" data-length="50" maxlength="48"  />
					</div>

				
				
		

		</div>

	<div class="modal-footer col s12" >
						<input type="hidden" name="categorias_personal_id" id="txtIdCategoriaPersonal">
							
							<a id="btn_editar" href=""
							class="modal-close waves-effect waves-green btn blue btn-custom">Guardar</a>
						<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
					</div>
	
</form:form>


</div>

//foter
</div>
<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>
<script src="public/js/categoriapersonal.js"></script>


<%@include file="/WEB-INF/include/footer.jsp"%>
