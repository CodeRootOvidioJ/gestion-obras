<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="col s9  offset-s3 center-align" align="center">

	<h1 class="clase1" align="center">Elementos</h1>

	<div class="col s12 ">
		<div class="row">
			<div class="input-field col s12 offset-s4">
				<a class="waves-effect waves-light btn modal-trigger" href="#modal2">Agregar</a>

			</div>
		</div>
	</div>

	<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tableElementos">
			<thead class="color-thead">
				<tr>
					<th>Codigo</th>
					<th>Descripcion</th>
					<th>Sub Rubro</th>
					<th>Unidad</th>
					<th>Costo</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${listElementos}" var="listElementos">
					<tr>
						<td><c:out value="${listElementos.codigo}" /></td>
						<td><c:out value="${listElementos.descripcion}" /></td>
						<td><c:out value="${listElementos.subrubro}" /></td>
						<td><c:out value="${listElementos.unidad}" /></td>
						<td><c:out value="${listElementos.costo}" /></td>
						<td><a class="zona_edit" href="#"
							data-id=<c:out value="${listElementos.codigo}" />
							data-name=<c:out value="${listElementos.descripcion}" />
							data-desplegable=<c:out value="${listElementos.subrubro}" />
							data-rubro=<c:out value="${listElementos.unidad}" />
							data-rubro=<c:out value="${listElementos.costo}" />>
								<i class="material-icons small icon-custom">edit</i>
						</a> <a class="zona_del" href="#"
							data-id=<c:out value="${listElementos.codigo}" />
							data-name=<c:out value="${listElementos.descripcion}" />> <i
								class="material-icons small icon-custom red-text">delete</i>
						</a></td>
					</tr>

				</c:forEach>

			</tbody>
		</table>
	</div>

	<div id="modal_actions" class="modal small">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del" href="" class="btn blue btn-custom">Aceptar</a> <a
				href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>

	<div id="modal_confirm" class="modal small center-align">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>

	<!-- Modal Structure agregar -->
	<div id="modal2" class="modal modal-fixed-footer">
		<div class="modal-content">

			<div class="row">

				<h5 class="color #f1f8e9 light-green lighten-5" th:text="${titulo}"
					align="center">Elementos</h5>
			</div>
			<div class="row">
				<form:form method="post" commandName="elementos">


					<form:errors path="*" element="div" cssClass="alert alert-danger" />
					<div class="input-field col s6">
						<i class="material-icons prefix black-text">perm_identity</i>
						<form:input path="codigo" data-length="2" maxlength="2"
							cssClass="validate black-text" required="true" />
						<form:label path="codigo">codigo:</form:label>
					</div>

					<div class="input-field col s6">
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="descripcion">Descripcion: </form:label>
						<form:input path="descripcion" cssClass="validate black-text"
							required="true" />
					</div>

					<div class="input-field col s6">
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="unidad">Unidad: </form:label>
						<form:input path="unidad" cssClass="validate black-text"
							required="true" />
					</div>

					<div class="input-field col s6">
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="costo">Costo: </form:label>
						<form:input path="costo" cssClass="validate black-text"
							required="true" />
					</div>

					<div class="col s6">
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="subrubro">Seleccione Rubro</form:label>
						<form:select path="subrubro" class="browser-default">
							<c:forEach items="${lstsubrubros}" var="lstsubrubros">

								<form:option path="subrubro" value="${lstsubrubros.codigo}">${lstsubrubros.descripcion}</form:option>

							</c:forEach>
						</form:select>
					</div>

					<div class="modal-footer" style="width: 670px;">
						<input type="submit" value="Guardar" class="btn btn-danger green" />
						<a href="#!"
							class="modal-close waves-effect waves-green btn-flat red">Cancelar</a>
					</div>
				</form:form>
			</div>

		</div>

	</div>

	<!-- Modal Structure para editar -->
	<div id="modal1" class="modal modal-fixed-footer">
		<div class="modal-content">

			<div class="row">

				<h5 class="color #f1f8e9 light-green lighten-5" th:text="${titulo}"
					align="center">Elementos</h5>

			</div>
			<div class="row">
				<form:form commandName="elementos" id="formedit">


					<form:errors path="*" element="div" cssClass="alert alert-danger" />

					<div class="input-field col s6">
						<p>Codigo:</p>
						<form:label path="codigo">
						</form:label>
						<i class="material-icons prefix black-text">perm_identity</i>
						<form:input path="codigo" id="txtcodigo"
							cssClass="validate black-text" required="true" />
					</div>

					<div class="input-field col s6">
						<p>Descripcion:</p>
						<form:label path="descripcion"></form:label>
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:input path="descripcion" id="txtdescripcion"
							cssClass="validate black-text" required="true" />
					</div>
					<div class="input-field col s6">
						<p>Unidad:</p>
						<form:label path="unidad"></form:label>
						<i class="material-icons prefix black-text">lock_outline</i>

						<form:input path="unidad" id="txtdesplegable"
							cssClass="validate black-text" required="true" />
					</div>


					<div class="input-field col s6">
						<p>Costo:</p>
						<form:label path="costo"></form:label>
						<i class="material-icons prefix black-text">lock_outline</i>

						<form:input path="costo" id="txtdesplegable"
							cssClass="validate black-text" required="true" />
					</div>

					<div class="col s10 m6">
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="subrubro" id="txtrubro">Seleccione Rubro</form:label>
						<form:select path="subrubro" class="browser-default">
							<c:forEach items="${lstsubrubros}" var="lstsubrubros">

								<form:option path="subrubro" value="${lstsubrubros.codigo}">${lstsubrubros.descripcion}</form:option>

							</c:forEach>
						</form:select>
					</div>

					<div class="modal-footer" style="width: 670px;">
						<a id="btn_editar" href=""
							class="modal-close waves-effect waves-green btn-flat green">Guardar</a>
						<a href="#!"
							class="modal-close waves-effect waves-green btn-flat red">Cancelar</a>
					</div>
				</form:form>
			</div>

		</div>

	</div>

</div>




//foter
</div>
<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>
<script src="public/js/elementos.js"></script>

<%@include file="/WEB-INF/include/footer.jsp"%>
