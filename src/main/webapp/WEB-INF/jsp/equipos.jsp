<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="col s9  offset-s3 center-align" align="center">

	<h5 class="clase1" align="center">Equipos</h5>


	<div class="fixed-action-btn">
		<a class="btn-floating btn-large blue modal-trigger"
			href="#modalAgregarEquipos"> <i class="large material-icons">add</i>
		</a>

	</div>


	<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tableEquipos">
			<thead class="color-thead">
				<tr>
					<th>Codigo</th>
					<th>Nombre</th>
					<th>Unidad</th>
					<th>T1</th>
					<th>PGR1</th>
					<th>T2</th>
					<th>PGR2</th>
					<th>T3</th>
					<th>PGR3</th>
					<th>Categoria</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lstequipos}" var="lstequipos">
					<tr>
						<td><c:out value="${lstequipos.codigo}" /></td>
						<td><c:out value="${lstequipos.nombre}" /></td>
						<td><c:out value="${lstequipos.unidad}" /></td>
						<td><c:out value="${lstequipos.t1}" /></td>
						<td><c:out value="${lstequipos.pgr1}" /></td>
						<td><c:out value="${lstequipos.t2}" /></td>
						<td><c:out value="${lstequipos.pgr2}" /></td>
						<td><c:out value="${lstequipos.t3}" /></td>
						<td><c:out value="${lstequipos.pgr3}" /></td>
						<td><c:out value="${lstequipos.nombrecategoria}" /></td>

						<td><a class="equipos_edit" href="#"
							data-id="<c:out value="${lstequipos.equipos_id}"/>"
							data-codigo="<c:out value="${lstequipos.codigo}"/>"
							data-unidad="<c:out value="${lstequipos.unidad}"/>"
							data-name="<c:out value="${lstequipos.nombre}"/>"
							data-categorias_equipos="<c:out value="${lstequipos.categorias_equipos_id}"/>"
							data-t1="<c:out value="${lstequipos.t1}"/>"
							data-pgr1="<c:out value="${lstequipos.pgr1}"/>"
							data-t2="<c:out value="${lstequipos.t2}"/>"
							data-pgr2="<c:out value="${lstequipos.pgr2}"/>"
							data-t3="<c:out value="${lstequipos.t3}"/>"
							data-pgr3="<c:out value="${lstequipos.pgr3}"/>"> <i
								class="material-icons small icon-custom">edit</i>
						</a> <a class="equipos_del" href="#"
							data-id="<c:out value="${lstequipos.equipos_id}"/>"
							data-name="<c:out value="${lstequipos.nombre}"/>"> <i
								class="material-icons small icon-custom red-text">delete</i>
						</a></td>


					</tr>

				</c:forEach>



			</tbody>
		</table>
	</div>

	<div id="modal_actions_categoria_personal"
		class="modal small clasemodal_actionscss">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del_categoria_personal" href=""
				class="btn blue btn-custom">Aceptar</a> <a href="#!"
				class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>


	<div id="modal_confirm"
		class="modal small center-align   clasemodal_confirm_css">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>



	<!-- Modal Structure agregar -->
	<div id="modalAgregarEquipos" class="modal modal-fixed-footer ">
		<form:form method="post" commandName="equipo"
			action="agregarequipos.html">
			<div class="modal-content">



				<h5 class="lighten-5" align="center">Equipos</h5>

				<form:errors path="*" element="div" cssClass="alert alert-danger" />
				<div class="input-field col s3">
					<i class="material-icons prefix black-text">dialpad</i>
					<form:input path="codigo" cssClass="validate black-text"
						required="true" data-length="7" maxlength="7" />
					<form:label path="codigo">Codigo:</form:label>
				</div>





				<div class="input-field col s3">
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="t1" cssClass="validate black-text decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
					<form:label path="t1">T1:</form:label>
				</div>

				<div class="input-field col s3">
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="pgr1" cssClass="validate black-text decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
					<form:label path="pgr1">PGR1:</form:label>
				</div>
				<div class="input-field col s3">
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="t2" cssClass="validate black-text decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
					<form:label path="t2">T2:</form:label>
				</div>

				<div class="input-field col s3">
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="pgr2" cssClass="validate black-text decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
					<form:label path="pgr2">PGR2:</form:label>
				</div>
				<div class="input-field col s3">
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="t3" cssClass="validate black-text decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
					<form:label path="t3">T3:</form:label>
				</div>

				<div class="input-field col s3">
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="pgr3" cssClass="validate black-text decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
					<form:label path="pgr3">PGR3:</form:label>
				</div>

				<div class="input-field col s3">
					<i class="material-icons prefix black-text">dialpad</i>
					<form:input path="unidad" cssClass="validate black-text"
						required="true" data-length="3" maxlength="3" />
					<form:label path="unidad">Unidad:</form:label>
				</div>


				<div class="input-field col s6">
					<i class="material-icons prefix black-text">perm_identity</i>
					<form:label path="nombre">Nombre: </form:label>
					<form:input path="nombre" cssClass="validate black-text"
						required="true" data-length="30" maxlength="30" />
				</div>
				<div class="col s6">
					<i class="material-icons prefix black-text">lock_outline</i>
					<form:label path="categorias_equipos_id">Seleccione equipo</form:label>
					<form:select path="categorias_equipos_id" class="browser-default">
						<c:forEach items="${lstcategoriaequipos}"
							var="lstcategoriaequipos">

							<form:option path="categorias_equipos_id"
								value="${lstcategoriaequipos.categorias_equipos_id}">${lstcategoriaequipos.descripcion}</form:option>

						</c:forEach>
					</form:select>
				</div>

			</div>

			<div class="modal-footer col s12">
				<input type="submit" value="Guardar" class="btn blue btn-custom" />
				<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
			</div>
		</form:form>
	</div>


	<!-- Modal Structure para editar -->
	<div id="modalmodificar" class="modal modal-fixed-footer ">
		<form:form commandName="equipo" id="formedit"
			action="modificarequipos.html">
			<div class="modal-content">



				<h5 class="lighten-5" align="center">Equipos</h5>






				<form:errors path="*" element="div" cssClass="alert alert-danger" />

				<div class="input-field col s3">
					<p>Codigo:</p>
					<form:label path="codigo">
					</form:label>
					<i class="material-icons prefix black-text">dialpad</i>
					<form:input path="codigo" id="txtcodigo"
						cssClass="validate black-text" required="true" data-length="7"
						maxlength="7" />


				</div>



				<div class="input-field col s3">
					<p>T1:</p>
					<form:label path="t1">
					</form:label>
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="t1" id="txtt1" cssClass="validate black-text decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />


				</div>

				<div class="input-field col s3">
					<p>PGR1:</p>
					<form:label path="pgr1"></form:label>
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="pgr1" id="txtpgr1" cssClass="validate black-text decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
				</div>

				<div class="input-field col s3">
					<p>T2:</p>
					<form:label path="t2">
					</form:label>
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="t2" id="txtt2" cssClass="validate black-text decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />


				</div>

				<div class="input-field col s3">
					<p>PGR2:</p>
					<form:label path="pgr2"></form:label>
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="pgr2" id="txtpgr2" cssClass="validate black-text decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
				</div>

				<div class="input-field col s3">
					<p>T3:</p>
					<form:label path="t3">
					</form:label>
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="t3" id="txtt3" cssClass="validate black-text decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />


				</div>

				<div class="input-field col s3">
					<p>PGR3:</p>
					<form:label path="pgr3"></form:label>
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="pgr3" id="txtpgr3" cssClass="validate black-text decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
				</div>

				<div class="input-field col s3">
					<p>Unidad:</p>
					<form:label path="unidad"></form:label>
					<i class="material-icons prefix black-text">dialpad</i>
					<form:input path="unidad" id="txtunidad"
						cssClass="validate black-text" required="true" data-length="3"
						maxlength="3" />
				</div>

				<div class="input-field col s6">
					<p>Nombre:</p>
					<form:label path="nombre"></form:label>
					<i class="material-icons prefix black-text">perm_identity</i>
					<form:input path="nombre" id="txtnombre"
						cssClass="validate black-text" required="true" data-length="30"
						maxlength="30" />
				</div>

				<div class="col s6">
					<i class="material-icons prefix black-text">dialpad</i>
					<form:label path="categorias_equipos_id">Seleccione equipo</form:label>
					<form:select path="categorias_equipos_id" id="cmbcategoriaequipos"
						class="browser-default">
						<c:forEach items="${lstcategoriaequipos}"
							var="lstcategoriaequipos">

							<form:option path="categorias_equipos_id"
								value="${lstcategoriaequipos.categorias_equipos_id}">${lstcategoriaequipos.descripcion}</form:option>

						</c:forEach>
					</form:select>
				</div>


			</div>

			<div class="modal-footer col s12">
				<input type="hidden" name="equipos_id" id="txtIdEquipos"> <a
					id="btn_editar" href=""
					class="modal-close waves-effect waves-green btn blue btn-custom">Guardar</a>
				<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
			</div>

		</form:form>


	</div>


</div>
<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>
<script src="public/js/equipos.js"></script>
<script src="public/js/main.js"></script>

<%@include file="/WEB-INF/include/footer.jsp"%>
