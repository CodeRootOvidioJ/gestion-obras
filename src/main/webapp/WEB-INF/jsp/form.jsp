<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="col s10  offset-s2 center-align" align="center">

		<div class="modal-content col s10 offset-s1">
			<div class="row">
				<form:form method="post" commandName="obra"
			action="agregarformobras.html">
			<div class="modal-content">



				<h5 class="lighten-5" align="center">Obras</h5>

				<form:errors path="*" element="div" cssClass="alert alert-danger" />
				<div class="col s12  ">
				<div class="input-field col s4">
					<i class="material-icons prefix black-text">dialpad</i>
					<form:input path="codigo" cssClass="validate black-text"
						required="true" data-length="10" maxlength="10" />
					<form:label path="codigo">Codigo:</form:label>
				</div>


				<div class="input-field col s4">
					<i class="material-icons prefix black-text">assignment</i>
					<form:input path="denominacion" cssClass="validate black-text"
						required="true" data-length="50" maxlength="50" />
					<form:label path="denominacion">Denominacion:</form:label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix black-text">assignment_ind</i>
					<form:input path="nombre_corto" cssClass="validate black-text"
						required="true" data-length="15" maxlength="15" />
					<form:label path="nombre_corto">Nombre corto:</form:label>
				</div>
				<div class="input-field col s4">
					<i class="material-icons prefix black-text">burst_mode</i>
					<form:input path="direccion" cssClass="validate black-text"
						required="true" data-length="50" maxlength="50" />
					<form:label path="direccion">Direccion:</form:label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix black-text">center_focus_strong</i>
					<form:input path="coordenadas" cssClass="validate black-text"
						required="true" data-length="50" maxlength="50" />
					<form:label path="coordenadas">Coordenadas:</form:label>
				</div>
				<div class="input-field col s4">
					<i class="material-icons prefix black-text">person</i>
					<form:input path="ingeniero" cssClass="validate black-text"
						required="true" data-length="4" maxlength="4" />
					<form:label path="ingeniero">Ingeniero:</form:label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix black-text">person_outline</i>
					<form:input path="director" cssClass="validate black-text"
						required="true" data-length="4" maxlength="4" />
					<form:label path="director">Director:</form:label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix black-text">poll</i>
					<form:input path="finalidad" cssClass="validate black-text"
						required="true" data-length="50" maxlength="50" />
					<form:label path="finalidad">Finalidad:</form:label>
				</div>
				
				<div class="input-field col s4">
					<i class="material-icons prefix black-text">settings_overscan</i>
					<form:input path="tipo_terreno" cssClass="validate black-text"
						required="true" data-length="50" maxlength="50" />
					<form:label path="tipo_terreno">Tipo terreno:</form:label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix black-text">sync_problem</i>
					<form:input path="dificultades_tecnicas" cssClass="validate black-text"
						required="true" data-length="50" maxlength="50" />
					<form:label path="dificultades_tecnicas">Dificultades tecnicas:</form:label>
				</div>
				
				<div class="input-field col s4">
					<i class="material-icons prefix black-text">person_pin</i>
					<form:input path="nombre_cliente" cssClass="validate black-text"
						required="true" data-length="50" maxlength="50" />
					<form:label path="nombre_cliente">Nombre cliente:</form:label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix black-text">dialpad</i>
					<form:input path="nit" cssClass="validate black-text"
						required="true" data-length="20" maxlength="20" />
					<form:label path="nit">NIT:</form:label>
				</div>
				
				<div class="input-field col s4">
					<i class="material-icons prefix black-text">pin_drop</i>
					<form:input path="direcion_cliente" cssClass="validate black-text"
						required="true" data-length="50" maxlength="50" />
					<form:label path="direcion_cliente">Direcion cliente:</form:label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix black-text">phone</i>
					<form:input path="telefonos" cssClass="validate black-text"
						required="true" data-length="30" maxlength="30" />
					<form:label path="telefonos">Telefonos:</form:label>
				</div>
				
				<div class="input-field col s4">
					<i class="material-icons prefix black-text">phone</i>
					<form:input path="contacto_tecnico" cssClass="validate black-text"
						required="true" data-length="30" maxlength="30" />
					<form:label path="contacto_tecnico">Contacto tecnico:</form:label>
				</div>

				<div class="input-field col s4">
					<i class="material-icons prefix black-text">phone</i>
					<form:input path="contacto_cobro" cssClass="validate black-text"
						required="true" data-length="30" maxlength="30" />
					<form:label path="contacto_cobro">Contacto cobro:</form:label>
				</div>
				
				
				<div class=" col s4">
				<i class="material-icons prefix black-text">chrome_reader_mode</i>
					<form:label path="inicio">Fecha inicia: </form:label>
					<form:input path="inicio" type="text" class="datepicker"
						required="true" />

				</div>
			
	<div class=" col s4">
				<i class="material-icons prefix black-text">chrome_reader_mode</i>
					<form:label path="fin">Fecha fin: </form:label>
					<form:input path="fin" type="text" class="datepicker"
						required="true" />

				</div>
				
				
				
				
				
				
				
				
				<div class="input-field col s3">
					<i class="material-icons prefix black-text">arrow_upward</i>
					<form:input path="duracion" cssClass="validate black-text numeric"
						required="true" data-length="4" maxlength="4" />
					<form:label path="duracion">Duracion:</form:label>
				</div>

				<div class="input-field col s3">
					<i class="material-icons prefix black-text">blur_off</i>
					<form:input path="anticipo" cssClass="validate black-text numeric"
						required="true" data-length="2" maxlength="2" />
					<form:label path="anticipo">Anticipo:</form:label>
				</div>
				
				<div class="input-field col s3">
					<i class="material-icons prefix black-text">arrow_upward</i>
					<form:input path="dia_corte" cssClass="validate black-text numeric"
						required="true" data-length="3" maxlength="3" />
					<form:label path="dia_corte">Dia corte:</form:label>
				</div>

				<div class="input-field col s3">
					<i class="material-icons prefix black-text">arrow_upward</i>
					<form:input path="dias_credito" cssClass="validate black-text numeric"
						required="true" data-length="3" maxlength="3" />
					<form:label path="dias_credito">Dias credito:</form:label>
				</div>
				
				
				<div class="col s4">
					<form:label path="estadosdeobras_id">Seleccione estado de obras</form:label>
					<form:select path="estadosdeobras_id" class="browser-default">
						<c:forEach items="${lstestadoobras}"
							var="lstestadoobras">

							<form:option path="estadosdeobras_id"
								value="${lstestadoobras.estadosdeobra_id}">${lstestadoobras.descripcion}</form:option>

						</c:forEach>
					</form:select>
				</div>

	<div class="col s4">
					<form:label path="filiales_id">Seleccione Filial</form:label>
					<form:select path="filiales_id" class="browser-default">
						<c:forEach items="${lstfiliales}"
							var="lstfiliales">

							<form:option path="filiales_id"
								value="${lstfiliales.filiales_id}">${lstfiliales.descripcion}</form:option>

						</c:forEach>
					</form:select>
				</div>
				
					<div class="col s4">
					<form:label path="tiposdeobra_id">Seleccione tipo obra</form:label>
					<form:select path="tiposdeobra_id" class="browser-default">
						<c:forEach items="${lsttipodeobra}"
							var="lsttipodeobra">

							<form:option path="tiposdeobra_id"
								value="${lsttipodeobra.tiposdeobra_id}">${lsttipodeobra.descripcion}</form:option>

						</c:forEach>
					</form:select>
				</div>
				
					<div class="col s4">
					<form:label path="tiposdecontrato_id">Seleccione tipo contrato</form:label>
					<form:select path="tiposdecontrato_id" class="browser-default">
						<c:forEach items="${lsttiposcontratos}"
							var="lsttiposcontratos">

							<form:option path="tiposdecontrato_id"
								value="${lsttiposcontratos.tipocontrato_id}">${lsttiposcontratos.descripcion}</form:option>

						</c:forEach>
					</form:select>
				</div>
				
					<div class="col s4">
					<form:label path="tiposdefinanciamiento_id">Seleccione tipo financiamiento</form:label>
					<form:select path="tiposdefinanciamiento_id" class="browser-default">
						<c:forEach items="${lsttiposfinanciamientos}"
							var="lsttiposfinanciamientos">

							<form:option path="tiposdefinanciamiento_id"
								value="${lsttiposfinanciamientos.tiposdefinanciamiento_id}">${lsttiposfinanciamientos.descripcion}</form:option>

						</c:forEach>
					</form:select>
				</div>
				
					<div class="col s4">
					<form:label path="tiposdemercado_id">Seleccione tipo mercado</form:label>
					<form:select path="tiposdemercado_id" class="browser-default">
						<c:forEach items="${lsttipodemercado}"
							var="lsttipodemercado">

							<form:option path="tiposdemercado_id"
								value="${lsttipodemercado.tiposdemercado_id}">${lsttipodemercado.descripcion}</form:option>

						</c:forEach>
					</form:select>
				</div>
				
					<div class="col s4">
					<form:label path="tiposdecliente_id">Seleccione tipo cliente</form:label>
					<form:select path="tiposdecliente_id" class="browser-default">
						<c:forEach items="${lsttipodecliente}"
							var="lsttipodecliente">

							<form:option path="tiposdecliente_id"
								value="${lsttipodecliente.tiposdecliente_id}">${lsttipodecliente.descripcion}</form:option>

						</c:forEach>
					</form:select>
				</div>
				
					<div class="col s4">
					<form:label path="turnosdetrabajo_id">Seleccione turno trabajo</form:label>
					<form:select path="turnosdetrabajo_id" class="browser-default">
						<c:forEach items="${lstturnostrabajo}"
							var="lstturnostrabajo">

							<form:option path="turnosdetrabajo_id"
								value="${lstturnostrabajo.turnosdetrabajo_id}">${lstturnostrabajo.descripcion}</form:option>

						</c:forEach>
					</form:select>
				</div>
				
					
				
				
				
			</div>

			<div class="modal-footer col s12">
				<input type="submit" value="Guardar" class="btn blue btn-custom" />
				<a href="listaobra.html" class="modal-action modal-close btn-flat ">Cancelar</a>
			</div>
			</div>
		</form:form>
			</div>



		</div>



</div>
<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>
<script src="public/js/obras.js"></script>
<script src="public/js/main.js"></script>
<%@include file="/WEB-INF/include/footer.jsp"%>
