<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


	
<div class="col s9  offset-s3 "  commandName="obra" >
<h3 class="clase1" align="center">Detalle de Obra</h3>
<ul id="tabs-swipe-demo" class="tabs">
    <li class="tab col s2 "><a href="#test-swipe-1">datos de obra</a></li>
    <li class="tab col s2"><a class="active" href="#test-swipe-2">Datos Cliente</a></li>
    <li class="tab col s2"><a href="#test-swipe-3">Contacto</a></li>
     <li class="tab col s2"><a class="active" href="#test-swipe-4">tiempo</a></li>
    <li class="tab col s2"><a href="#test-swipe-5">General</a></li>
  </ul>
  <div id="test-swipe-1" class="col s12 ">

     <div class="col s6 m6  ">
   <ul class="collection">
    <li class="collection-item avatar">
      
      <span class="title">Nombre</span>
    <i class="material-icons circle green">assignment_ind</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.nombre_corto}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
  
      <li class="collection-item avatar">
      
      <span class="title">Direccion</span>
    <i class="material-icons circle green">burst_mode</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.direccion}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
    <li class="collection-item avatar">
      
      <span class="title">Coordenadas</span>
    <i class="material-icons circle green">center_focus_strong</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.coordenadas}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
  </ul>
   </div>
   
    <div class="col s6 m6  offset-s6 ">
   <ul class="collection">
   
     <li class="collection-item avatar">
      
      <span class="title">Codigo</span>
    <i class="material-icons circle green">dialpad</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.codigo}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
   
    <li class="collection-item avatar">
      
      <span class="title">Ingeniero</span>
    <i class="material-icons circle green">person</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.ingeniero}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
   
      <li class="collection-item avatar">
      
      <span class="title">Director</span>
    <i class="material-icons circle green">person_outline</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.director}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
     
   
    
  </ul>
   </div>


			
   
  
      
  </div>
  <div id="test-swipe-2" class="col s12 #e3f2fd  lighten-5">
  
     <div class="col s6 m6  ">
   <ul class="collection">
    <li class="collection-item avatar">
      
      <span class="title">Nombre del cliente</span>
    <i class="material-icons circle green">person_pin</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.nombre_cliente}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
  
      <li class="collection-item avatar">
      
      <span class="title">Direccion del cliente</span>
    <i class="material-icons circle green">pin_drop</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.direcion_cliente}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
   
    
  </ul>
   </div>
   
    <div class="col s6 m6  offset-s6 ">
   <ul class="collection">
    <li class="collection-item avatar">
      
      <span class="title">NIT</span>
    <i class="material-icons circle green">dialpad</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.nit}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
   
      <li class="collection-item avatar">
      
      <span class="title">Telefonos</span>
    <i class="material-icons circle green">phone</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.telefonos}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
     
   
    
  </ul>
   </div>
  
  
  
     
       
  </div>
  <div id="test-swipe-3" class="col s12 #bbdefb  lighten-4">
  
   <div class="col s6 m6  ">
   <ul class="collection">
    <li class="collection-item avatar">
      
      <span class="title">Contacto tecnico</span>
    <i class="material-icons circle green">phone</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.contacto_tecnico}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
  
    
   
    
  </ul>
   </div>
   
    <div class="col s6 m6  offset-s6 ">
   <ul class="collection">
    <li class="collection-item avatar">
      
      <span class="title">Contacto cobro</span>
    <i class="material-icons circle green">phone</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.contacto_cobro}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
   
    
    
     
   
    
  </ul>
   </div>
   
  
  
  
     
    
   
  </div>
   <div id="test-swipe-4" class="col s12 #90caf9  lighten-3">
  
   <div class="col s6 m6  ">
   <ul class="collection">
    <li class="collection-item avatar">
      
      <span class="title">Fecha de inicio</span>
    <i class="material-icons circle green">chrome_reader_mode</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.inicio}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
     <li class="collection-item avatar">
      
      <span class="title">Fecha de finalizacion</span>
    <i class="material-icons circle green">chrome_reader_mode</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.fin}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
   
    
  </ul>
   </div>
   
    <div class="col s6 m6  offset-s6 ">
   <ul class="collection">
    <li class="collection-item avatar">
      
      <span class="title">Duracion</span>
    <i class="material-icons circle green">arrow_upward</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.duracion}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
   
    
    
     
   
    
  </ul>
   </div>
   
  
  
  
    
    
   
   </div>
  <div id="test-swipe-5" class="col s12 #64b5f6  lighten-2">
 <div class="col s6 m6  ">
   <ul class="collection">
    <li class="collection-item avatar">
      
      <span class="title">Denominacion</span>
    <i class="material-icons circle green">assignment</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.denominacion}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
      <li class="collection-item avatar">
      
      <span class="title">Anticipo</span>
    <i class="material-icons circle green">blur_off</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.anticipo}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
     <li class="collection-item avatar">
      
      <span class="title">dia corte</span>
    <i class="material-icons circle green">arrow_upward</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.dia_corte}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
     <li class="collection-item avatar">
      
      <span class="title">dia credito</span>
    <i class="material-icons circle green">arrow_upward</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.dias_credito}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
     <li class="collection-item avatar">
      
      <span class="title">estados de obras</span>
    <i class="material-icons circle green">assignment</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.estadosdeobras_id}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
     <li class="collection-item avatar">
      
      <span class="title">Filiales</span>
    <i class="material-icons circle green">assignment</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.filiales_id}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
     <li class="collection-item avatar">
      
      <span class="title">tipos de obra</span>
    <i class="material-icons circle green">assignment</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.tiposdeobra_id}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
     <li class="collection-item avatar">
      
      <span class="title">tipos de contrato</span>
    <i class="material-icons circle green">assignment</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.tiposdecontrato_id}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
   
    
    
  </ul>
   </div>
   
   
   <div class="col s6 m6  offset-s6">
     <ul class="collection">
   
   
     <li class="collection-item avatar">
      
      <span class="title">tipos de financiamiento</span>
    <i class="material-icons circle green">assignment</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.denominacion}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
   
    
     <li class="collection-item avatar">
      
      <span class="title">tipos de financiamiento</span>
    <i class="material-icons circle green">assignment</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.tiposdefinanciamiento_id}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
     <li class="collection-item avatar">
      
      <span class="title">tipos de mercado</span>
    <i class="material-icons circle green">assignment</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.tiposdemercado_id}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
     <li class="collection-item avatar">
      
      <span class="title">tipos de cliente</span>
    <i class="material-icons circle green">assignment</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.tiposdecliente_id}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
     <li class="collection-item avatar">
      
      <span class="title">turnos de trabajo</span>
    <i class="material-icons circle green">assignment</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.turnosdetrabajo_id}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
      <li class="collection-item avatar">
      
      <span class="title">Finalidad</span>
    <i class="material-icons circle green">poll</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.finalidad}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
      <li class="collection-item avatar">
      
      <span class="title">Tipo terreno</span>
    <i class="material-icons circle green">settings_overscan</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.tipo_terreno}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
      <li class="collection-item avatar">
      
      <span class="title">Dificultades tecnicas</span>
    <i class="material-icons circle green">sync_problem</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obra.dificultades_tecnicas}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
  </ul>
   </div>
   
   
   
   
    
  
  
  </div>
  
  
  <div class="modal-footer col s12 " >
							
							
						<a href="listaobra.html" class="modal-action modal-close btn-flat ">Volver</a>
					</div>
    	</div>    








</div>
<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>
<script src="public/js/obras.js"></script>

<%@include file="/WEB-INF/include/footer.jsp"%>
