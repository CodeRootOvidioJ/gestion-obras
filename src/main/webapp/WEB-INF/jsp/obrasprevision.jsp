<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="col s9  offset-s3 center-align" align="center">

	<h5 class="clase1" align="center">Obras Prevision 0</h5>

	<div class="fixed-action-btn">
		<a class="btn-floating btn-large blue modal-trigger"
			href="#modalAgregarObrasPrevision"> <i class="large material-icons">add</i>
		</a>

	</div>
	<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tableObrasPrevision">
			<thead class="color-thead">
				<tr>
					<th>Codigo</th>
					<th>Periodo</th>
					<th>Rubro</th>
					<th>SubRubro</th>
					<th>Elemento</th>
					<th>Descripcion</th>
					<th>Medida</th>
					<th>Costo sugerido</th>
					<th>Unidades</th>
					<th>Costo</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lstsobrasprevision}" var="lstsobrasprevision">
					<tr>
						<td><c:out value="${lstsobrasprevision.codigo}" /></td>
						<td><c:out value="${lstsobrasprevision.nombreperiodo}" /></td>
						<td><c:out value="${lstsobrasprevision.nombrerubro}" /></td>
							<td><c:out value="${lstsobrasprevision.sub_rubro_id}" /></td>
								<td><c:out value="${lstsobrasprevision.elemento_id}" /></td>
						<td><c:out value="${lstsobrasprevision.descripcion}" /></td>
						<td><c:out value="${lstsobrasprevision.medida}" /></td>
						<td><c:out value="${lstsobrasprevision.costosugerido}" /></td>
									<td><c:out value="${lstsobrasprevision.unidades}" /></td>
												<td><c:out value="${lstsobrasprevision.costo}" /></td>
		<td><a class="obras_prevision_edit" href="#"
							data-id="<c:out value="${lstsobrasprevision.obrasprevision_id}"/>"
							data-codigo="<c:out value="${lstsobrasprevision.codigo}"/>"
							data-periodos="<c:out value="${lstsobrasprevision.periodos_id}"/>"
							data-rubro-id="<c:out value="${lstsobrasprevision.rubro_id}"/>"
								data-sub-rubro-id="<c:out value="${lstsobrasprevision.sub_rubro_id}"/>"
									data-elemento-id="<c:out value="${lstsobrasprevision.elemento_id}"/>"
							
							data-medida="<c:out value="${lstsobrasprevision.medida}"/>"
							data-costosugerido="<c:out value="${lstsobrasprevision.costosugerido}"/>"
							data-unidades="<c:out value="${lstsobrasprevision.unidades}"/>"
							data-costo="<c:out value="${lstsobrasprevision.costo}"/>"
							
							data-descripcion="<c:out value="${lstsobrasprevision.descripcion}"/>"> <i
								class="material-icons small icon-custom">edit</i>
						</a> <a class="obrasprevision_del" href="#"
							data-id="<c:out value="${lstsobrasprevision.obrasprevision_id}"/>"
							data-name="<c:out value="${lstsobrasprevision.descripcion}"/>"> <i
								class="material-icons small icon-custom red-text">delete</i>
						</a></td>
					</tr>

				</c:forEach>

			</tbody>
		</table>


	</div>


	<div id="modal_actions" class="modal small clasemodal_actionscss ">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del_semanas" href="" class="btn blue btn-custom">Aceptar</a>
			<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>


	<div id="modal_confirm"
		class="modal small center-align clasemodal_confirm_css">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>


	<!-- Modal Structure agregar  semanas -->
	<div id="modalAgregarObrasPrevision" class="modal modal-fixed-footer">
		<form:form method="post" commandName="obrasprevision" action="addObrasPrevision.html">
			<div class="modal-content">



				<h5 class="lighten-5" align="center">Obras Prevision 0</h5>





				<form:errors path="*" element="div" cssClass="alert alert-danger" />




			

				
<div class="input-field col s6">
<i class="material-icons prefix black-text">dialpad</i>
					<form:label path="codigo">Codigo:</form:label>
					
					<form:input path="codigo" cssClass="validate black-text"
						required="true" data-length="7" maxlength="7" />
				</div>
	<div class="input-field col s6">
					<i class="material-icons prefix black-text">turned_in</i>
					<form:input path="descripcion" cssClass="validate black-text"
						required="true" data-length="30" maxlength="50" />
					<form:label path="descripcion">Descripcion:</form:label>
				</div>
				
				
					<div class="input-field col s3">
					<i class="material-icons prefix black-text">blur_off</i>
					<form:input path="medida" cssClass="validate black-text "
						required="true" data-length="3" maxlength="3" />
					<form:label path="medida">Medida:</form:label>
				</div>
				<div class="input-field col s3">
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="costosugerido" cssClass="decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
					<form:label path="costosugerido">Costo sugerido:</form:label>
				</div>
				<div class="input-field col s3">
					<i class="material-icons prefix black-text">blur_off</i>
					<form:input path="unidades" cssClass="numeric"
						required="true" data-length="4" maxlength="4" />
					<form:label path="unidades">Unidades:</form:label>
				</div>
				<div class="input-field col s3">
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="costo" cssClass="decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
					<form:label path="costo">Costo:</form:label>
				</div>
				

				<div class="col s6">
					<form:label path="periodos_id">Seleccione Periodo</form:label>
					<form:select path="periodos_id" class="browser-default">
						<c:forEach items="${lstperiodos}" var="lstperiodos">

							<form:option path="periodos_id"
								value="${lstperiodos.periodos_id}">${lstperiodos.fecha_inicio}</form:option>
						</c:forEach>
					</form:select>
				</div>
		<div class="col s6">
					<form:label path="rubro_id">Seleccione Rubro</form:label>
					<form:select path="rubro_id"  id="cmbrubro" class="browser-default">
						<c:forEach items="${lstrubros}" var="lstrubros">

							<form:option path="rubro_id"
								value="${lstrubros.rubro_id}">${lstrubros.codigo}</form:option>
						</c:forEach>
					</form:select>
				</div>
				
						<div class="col s6">
					<form:label path="sub_rubro_id">Seleccione SubRubro</form:label>
					<form:select path="sub_rubro_id"  id="cmbsubrubro" class="browser-default">
					
					</form:select>
				</div>
				
					<div class="col s6">
					<form:label path="elemento_id">Seleccione Elemento</form:label>
					<form:select path="elemento_id"  id="cmbelemento" class="browser-default">
						
					</form:select>
				</div>
			

			</div>
			<div class="modal-footer col s12">
				<input type="submit" value="Guardar" class="btn blue btn-custom" />
				<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
			</div>
		</form:form>
	</div>


	
	<!-- Modal Structure para editar -->
	<div id="modalmodificar" class="modal modal-fixed-footer ">
		<form:form commandName="obrasprevision" id="formedit"
			action="modificarobrasprevision.html">
			<div class="modal-content">



				<h5 class="lighten-5" align="center">Obras Prevision 0</h5>





				<form:errors path="*" element="div" cssClass="alert alert-danger" />


				
	<div class="input-field col s6">
					<p>Codigo:</p>
					<form:label path="codigo">
					</form:label>
					<i class="material-icons prefix black-text">dialpad</i>
					<form:input path="codigo" id="txtcodigo"
						cssClass="validate black-text" required="true" data-length="7"
						maxlength="7" />
				</div>

<div class="input-field col s6">
					<p>Descripcion:</p>
					<form:label path="descripcion">
					</form:label>
					<i class="material-icons prefix black-text">turned_in</i>
					<form:input path="descripcion" id="txtdescripcion"
						cssClass="validate black-text" required="true" data-length="30"
						maxlength="30" />
				</div>
				
				
					<div class="input-field col s3">
					<p>Medida:</p>
					<i class="material-icons prefix black-text">blur_off</i>
					<form:input path="medida" id="txtmedida" cssClass="validate black-text "
						required="true" data-length="3" maxlength="3" />
					<form:label path="medida"></form:label>
				</div>
				<div class="input-field col s3">
				<p>Costo Sugerido:</p>
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="costosugerido" id="txtcostosugerido" cssClass="decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
					<form:label path="costosugerido"></form:label>
				</div>
				<div class="input-field col s3">
					<p>Unidades:</p>
					<i class="material-icons prefix black-text">blur_off</i>
					<form:input path="unidades" id="txtunidades" cssClass="numeric"
						required="true" data-length="4" maxlength="4" />
					<form:label path="unidades"></form:label>
				</div>
				<div class="input-field col s3">
					<p>Costo:</p>
					<i class="material-icons prefix black-text">attach_money</i>
					<form:input path="costo" id="txtcosto" cssClass="decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
					<form:label path="costo"></form:label>
				</div>
				

				<div class="col s6">
					<form:label path="periodos_id">Seleccione Periodo</form:label>
					<form:select path="periodos_id" id="cmbperiodos"  class="browser-default">
						<c:forEach items="${lstperiodos}" var="lstperiodos">

							<form:option path="periodos_id"
								value="${lstperiodos.periodos_id}">${lstperiodos.fecha_inicio}</form:option>
						</c:forEach>
					</form:select>
				</div>
		<div class="col s6">
					<form:label path="rubro_id">Seleccione Rubro</form:label>
					<form:select path="rubro_id"  id="cmbrubros" class="browser-default">
						<c:forEach items="${lstrubros}" var="lstrubros">

							<form:option path="rubro_id"
								value="${lstrubros.rubro_id}">${lstrubros.codigo}</form:option>
						</c:forEach>
					</form:select>
				</div>
				
						<div class="col s6">
					<form:label path="sub_rubro_id">Seleccione SubRubro</form:label>
					<form:select path="sub_rubro_id"  id="cmbsubrubros" class="browser-default">
				
					</form:select>
				</div>
				
						<div class="col s6">
					<form:label path="elemento_id">Seleccione Elemento</form:label>
					<form:select path="elemento_id"  id="cmbelementos" class="browser-default">
					
					</form:select>
				</div>
				

			</div>
			
				<div class="modal-footer col s12">
				<input type="hidden" name="obrasprevision_id" id="txtIdobrasprevision"> <a
					id="btn_editar" href=""
					class="modal-close waves-effect waves-green btn blue btn-custom">Guardar</a>
				<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
			</div>
			
	
			
			
		</form:form>
	</div>






		


</div>



</div>

<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>

<script src="public/js/obrasprevision.js"></script>
<script src="public/js/main.js"></script>
<%@include file="/WEB-INF/include/footer.jsp"%>
