<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


	
<div class="col s9  offset-s3 "  commandName="obrasprevision" >
<h5 class="clase1" align="center">Detalle de Obra Prevision 0</h5>
<ul id="tabs-swipe-demo" class="tabs">
    <li class="tab col s2 "><a href="#test-swipe-1">Datos de obra prevision 0</a></li>
  </ul>
  <div id="test-swipe-1" class="col s12 ">

     <div class="col s6 m6  ">
   <ul class="collection">
    <li class="collection-item avatar">
      
      <span class="title">Codigo</span>
    <i class="material-icons circle green">assignment_ind</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obrasprevision.codigo}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
  
      <li class="collection-item avatar">
      
      <span class="title">Periodo</span>
    <i class="material-icons circle green">burst_mode</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obrasprevision.nombreperiodo}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
    <li class="collection-item avatar">
      
      <span class="title">Rubro</span>
    <i class="material-icons circle green">center_focus_strong</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obrasprevision.nombrerubro}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
        <li class="collection-item avatar">
      
      <span class="title">Sub Rubro</span>
    <i class="material-icons circle green">center_focus_strong</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obrasprevision.sub_rubro_id}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
        <li class="collection-item avatar">
      
      <span class="title">Elemento</span>
    <i class="material-icons circle green">center_focus_strong</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obrasprevision.elemento_id}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
  </ul>
   </div>
   
  

   <div class="col s6 m6  ">
   <ul class="collection">

  
      <li class="collection-item avatar">
      
      <span class="title">Descripcion</span>
    <i class="material-icons circle green">burst_mode</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obrasprevision.descripcion}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
    <li class="collection-item avatar">
      
      <span class="title">Medida</span>
    <i class="material-icons circle green">center_focus_strong</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obrasprevision.medida}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
    
        <li class="collection-item avatar">
      
      <span class="title">Costo sugerido</span>
    <i class="material-icons circle green">center_focus_strong</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obrasprevision.costosugerido}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
            <li class="collection-item avatar">
      
      <span class="title">Unidades</span>
    <i class="material-icons circle green">center_focus_strong</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obrasprevision.unidades}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
            <li class="collection-item avatar">
      
      <span class="title">Costo</span>
    <i class="material-icons circle green">center_focus_strong</i>
       <br>
        <br>
         <br>
    
        
      <c:out value="${obrasprevision.costo}"/>
      <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
    </li>
    
    
  </ul>
   </div>
			
   
  
  
  </div>
  
  
  <div class="modal-footer col s12 " >
							
							
						<a href="listaobra.html" class="modal-action modal-close btn-flat ">Volver</a>
					</div>
    	</div>    








</div>
<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>
<script src="public/js/obras.js"></script>

<%@include file="/WEB-INF/include/footer.jsp"%>
