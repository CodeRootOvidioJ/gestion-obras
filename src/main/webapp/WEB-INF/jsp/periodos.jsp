<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>



<div class="col s9  offset-s3 center-align" align="center">


	<h5 class="clase1" align="center">Periodos</h5>

	
	
	<div class="fixed-action-btn">
		<a class="btn-floating btn-large blue modal-trigger"
			href="#modalAgregarPeriodos"> <i class="large material-icons">add</i>
		</a>

	</div>


		<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tablePeriodos">
			<thead class="color-thead">
			<tr>
				<th>Codigo</th>
				<th>Descripcion</th>
				<th>Nombre</th>
				<th>General</th>
				<th>Fecha Inicio</th>
				<th>Fecha fin</th>
				<th>Estado</th>
				<th>Accion</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${lstperiodos}" var="lstperiodos">
				<tr>
					<td><c:out value="${lstperiodos.codigo}" /></td>
					<td><c:out value="${lstperiodos.descripcion}" /></td>
					<td><c:out value="${lstperiodos.nombre}" /></td>
					<td><c:out value="${lstperiodos.generales}" /></td>
					<td><c:out value="${lstperiodos.fecha_inicio}" /></td>
<td><c:out value="${lstperiodos.fecha_fin}" /></td>
<td><c:out value="${lstperiodos.nombreestado}" /></td>



					<td><a class="periodos_edit" href="#"
							data-id="<c:out value="${lstperiodos.periodos_id}"/>"
							data-codigo="<c:out value="${lstperiodos.codigo}"/>"
							data-name="<c:out value="${lstperiodos.nombre}"/>" 
							data-descripcion="<c:out value="${lstperiodos.descripcion}"/>" 
							data-hora-inicio="<c:out value="${lstperiodos.fecha_inicio}"/>"
							data-hora-fin="<c:out value="${lstperiodos.fecha_fin}"/>"
							data-estado="<c:out value="${lstperiodos.estado}"/>"
							data-generales="<c:out value="${lstperiodos.generales}"/>"
							>
								<i class="material-icons small icon-custom">edit</i>
						</a> <a class="periodos_del" href="#"
							data-id="<c:out value="${lstperiodos.periodos_id}"/>"
							data-name="<c:out value="${lstperiodos.descripcion}"/>" >
								<i class="material-icons small icon-custom red-text">delete</i>
						</a></td>


				</tr>

			</c:forEach>



		</tbody>
	</table>
	
	
	</div>
	
	
	<div id="modal_actions" class="modal small clasemodal_actionscss ">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del_periodos" href=""
				class="btn blue btn-custom">Aceptar</a> <a href="#!"
				class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>


	<div id="modal_confirm" class="modal small center-align clasemodal_confirm_css">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>
	
	
	<!-- Modal Structure agregar -->
	<div id="modalAgregarPeriodos" class="modal modal-fixed-footer">
	<form:form method="post" commandName="periodo" 	action="addPeriodos.html">
		<div class="modal-content">

			

				<h5 class="lighten-5" 
					align="center">Periodos</h5>
			
	
				


					<form:errors path="*" element="div" cssClass="alert alert-danger" />
					<div class="col s12">
					<div class="input-field col s6">
						<form:label path="codigo">Codigo:</form:label>
						<form:input path="codigo" cssClass="validate black-text"
							required="true" data-length="7" maxlength="7" />
					</div>


					<div class="input-field col s6">
						<form:label path="descripcion">Descripcion: </form:label>
						<form:input path="descripcion" cssClass="validate black-text"
							required="true"  data-length="30" maxlength="30" />
					</div>
						</div>
						<div class="col s12">
					<div class="input-field col s6">
						<form:label path="nombre">Nombre:</form:label>
						<form:input path="nombre" cssClass="validate black-text"
							required="true" data-length="5" maxlength="5" />
					</div>
	<div class="input-field col s6">
						<form:label path="generales">Generales:</form:label>
						<form:input path="generales" class="decimal-2-texbox"
							required="true" data-length="5" maxlength="5" />
					</div>

					
						</div>
						
						
							<div class="col s12" >
						
						<div class=" col s6">
						<form:label path="fecha_inicio">Fecha inicia: </form:label>
						<form:input path="fecha_inicio" type="text" class="datepicker" required="true" />
				
					</div>
				
					<div class=" col s6" >
						<form:label path="fecha_fin">Fecha fin: </form:label>
						<form:input path="fecha_fin" type="text" class="datepicker"   required="true" />
					</div>
					</div>
					
					
<div class="col s6">
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="estado">Seleccione estado</form:label>
						<form:select path="estado" class="browser-default">
						

								<form:option path="estado" value="1">Abierto</form:option>
								<form:option path="estado" value="0">Cerrado</form:option>

						</form:select>
					</div>


				
			</div>
			<div class="modal-footer col s12" >
						<input type="submit" value="Guardar" class="btn blue btn-custom" />
						<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
					</div>
</form:form>
	</div>
	

	

<!-- Modal Structure editar  Periodos-->
	<div id="modalmodificar" class="modal modal-fixed-footer">
		
<form:form commandName="periodo" id="formedit"
			action="modificarperiodos.html">

			<div class="modal-content">
<h5 class="clase1" align="center">Periodos</h5>
<form:errors path="*" element="div" cssClass="alert alert-danger" />

					<div class="input-field col s6">
						<p>Codigo:</p>
						<form:label path="codigo">
						</form:label>
						<form:input path="codigo" id="txtcodigo"
							cssClass="validate black-text" required="true"  data-length="7" maxlength="7"/>
					</div>

					<div class="input-field col s6">
						<p>Descripcion:</p>
						<form:label path="descripcion"></form:label>
						<form:input path="descripcion" id="txtdescripcion"
							cssClass="validate black-text" required="true" data-length="30" maxlength="30" />
					</div>
					
					<div class="input-field col s6">
					<p>Nombre:</p>
						<form:label path="nombre"></form:label>
						<form:input path="nombre"  id="txtnombre" cssClass="validate black-text"
							required="true" data-length="5" maxlength="5" />
					</div>
					
						<div class="input-field col s6">
						<p>Generales:</p>
						<form:label path="generales"></form:label>
						<form:input path="generales" id="txtgenerales" cssClass="validate black-text decimal-2-texbox"
							required="true" data-length="5" maxlength="5" />
					</div>
					
				
<div class="col s12" >
						
							<div class=" col s6">
						<form:label path="fecha_inicio">Fecha inicia: </form:label>
						<form:input path="fecha_inicio" type="text" id="txtfechainicia" class="datepicker" required="true" />
				
					</div>
				
					<div class=" col s6" >
						<form:label path="fecha_fin">Fecha fin: </form:label>
						<form:input path="fecha_fin" type="text" id="txtfechafin" class="datepicker"   required="true" />
					</div>
					</div>
					
					
					<div class="col s6">
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="estado">Seleccione estado</form:label>
						<form:select path="estado" id="cmbestado" class="browser-default">
						

								<form:option path="estado" value="1">Abierto</form:option>
								<form:option path="estado" value="0">Cerrado</form:option>

						</form:select>
					</div>
					

			</div>
			<div class="modal-footer">
			<input type="hidden" name="periodos_id"
					id="txtIdPeriodos">
				<a id="btn_editar" href="" class="modal-close btn btn-danger blue">Guardar</a>
				<a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
			</div>
		</form:form>
	</div>


</div>








</div>

<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>




<script src="public/js/periodos.js"></script>
<script src="public/js/main.js"></script>
<%@include file="/WEB-INF/include/footer.jsp"%>
