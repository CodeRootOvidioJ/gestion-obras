<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="col s9  offset-s3 center-align" align="center">

	<h5 class="clase1" align="center">Personal</h5>


	<div class="fixed-action-btn">
		<a class="btn-floating btn-large blue modal-trigger"
			href="#modalAgregarPersonal"> <i class="large material-icons">add</i>
		</a>

	</div>


	<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tablePersonal">
			<thead class="color-thead">
				<tr>
					<th>Codigo</th>
					<th>Nombre</th>
					<th>Unidad</th>
					<th>Costo</th>
					<th>Categoria</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lstpersonal}" var="lstpersonal">
					<tr>
						<td><c:out value="${lstpersonal.codigo}" /></td>
						<td><c:out value="${lstpersonal.nombre}" /></td>
						<td><c:out value="${lstpersonal.unidad}" /></td>
						<td><c:out value="${lstpersonal.costo}" /></td>
						<td><c:out value="${lstpersonal.nombrecategoriapersonal}" /></td>




						<td><a class="personal_edit" href="#"
							data-id="<c:out value="${lstpersonal.personal_id}"/>"
							data-codigo="<c:out value="${lstpersonal.codigo}"/>"
							data-unidad="<c:out value="${lstpersonal.unidad}"/>"
							data-costo="<c:out value="${lstpersonal.costo}"/>"
							data-IdCategoriaPersonal="<c:out value="${lstpersonal.categorias_personal_id}"/>"
							data-name="<c:out value="${lstpersonal.nombre}"/>"> <i
								class="material-icons small icon-custom">edit</i>
						</a> <a class="personal_del" href="#"
							data-id="<c:out value="${lstpersonal.personal_id}"/>"
							data-name="<c:out value="${lstpersonal.nombre}"/>"> <i
								class="material-icons small icon-custom red-text">delete</i>
						</a></td>


					</tr>

				</c:forEach>



			</tbody>
		</table>
	</div>

	<div id="modal_actions_personal"
		class="modal small clasemodal_actionscss">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del_personal" href="" class="btn blue btn-custom">Aceptar</a>
			<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>


	<div id="modal_confirm"
		class="modal small center-align clasemodal_confirm_css">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>



	<!-- Modal Structure agregar -->
	<div id="modalAgregarPersonal" class="modal modal-fixed-footer ">
		<form:form method="post" commandName="personal"
			action="agregarpersonal.html">
			<div class="modal-content">



				<h5 class="lighten-5" align="center">Personal</h5>





				<form:errors path="*" element="div" cssClass="alert alert-danger" />
				<div class="input-field col s6">
					<i class="material-icons prefix black-text">dialpad</i>
					<form:input path="codigo" cssClass="validate black-text"
						required="true" data-length="10" maxlength="10" />
					<form:label path="codigo">Codigo:</form:label>
				</div>


				<div class="input-field col s6">
					<i class="material-icons prefix black-text">perm_identity</i>
					<form:label path="nombre">Nombre: </form:label>
					<form:input path="nombre" cssClass="validate black-text"
						required="true" data-length="30" maxlength="30" />
				</div>
				<div class="input-field col s6">
					<i class="material-icons prefix black-text">dialpad</i>
					<form:input path="unidad" cssClass="validate black-text"
						required="true" data-length="3" maxlength="3" />
					<form:label path="unidad">Unidad:</form:label>
				</div>


				<div class="input-field col s6">
					<i class="material-icons prefix black-text">attach_money</i>
					<form:label path="costo">Costo: </form:label>
					<form:input path="costo" cssClass="validate black-text decimal-2-texbox"
						required="true" data-length="5" maxlength="5" />
				</div>

				<div class="col s6">
					<i class="material-icons prefix black-text">turned_in</i>
					<form:label path="categorias_personal_id">Seleccione categoria</form:label>
					<form:select path="categorias_personal_id" class="browser-default">
						<c:forEach items="${lstcategoriapersonal}"
							var="lstcategoriapersonal">

							<form:option path="categorias_personal_id"
								value="${lstcategoriapersonal.categorias_personal_id}">${lstcategoriapersonal.descripcion}</form:option>

						</c:forEach>
					</form:select>
				</div>


			</div>

			<div class="modal-footer col s12">
				<input type="submit" value="Guardar" class="btn blue btn-custom" />
				<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
			</div>
		</form:form>
	</div>


	<!-- Modal Structure para editar -->
	<div id="modalmodificar" class="modal modal-fixed-footer ">
		<form:form commandName="personal" id="formedit"
			action="modificarpersonal.html">
			<div class="modal-content">



				<h5 class="lighten-5" align="center">Personal</h5>






				<form:errors path="*" element="div" cssClass="alert alert-danger" />

				<div class="input-field col s6">
					<p>Codigo:</p>
					<form:label path="codigo">
					</form:label>
					<i class="material-icons prefix black-text">dialpad</i>
					<form:input path="codigo" id="txtcodigo"
						cssClass="validate black-text" required="true" data-length="10"
						maxlength="10" />


				</div>

				<div class="input-field col s6">
					<p>Nombre:</p>
					<form:label path="nombre"></form:label>
					<i class="material-icons prefix black-text">perm_identity</i>
					<form:input path="nombre" id="txtnombre"
						cssClass="validate black-text" required="true" data-length="30"
						maxlength="30" />
				</div>
				<div class="input-field col s6">
					<p>Unidad:</p>
					<form:label path="unidad">
					</form:label>
					<i class="material-icons prefix black-text">dialpad</i>
					<form:input path="unidad" id="txtunidad"
						cssClass="validate black-text" required="true" data-length="3"
						maxlength="3" />


				</div>

				<div class="input-field col s6">
					<p>Costo:</p>
					<form:label path="costo"></form:label>
					<i class="material-icons prefix black-text decimal-2-texbox">attach_money</i>
					<form:input path="costo" id="txtcosto"
						cssClass="validate black-text" required="true" data-length="5"
						maxlength="5" />
				</div>

				<div class="col s6">
					<p>Seleccione Personal</p>
					<i class="material-icons prefix black-text">lock_outline</i>
					<form:label path="categorias_personal_id"></form:label>
					<form:select path="categorias_personal_id"
						id="cmbIdCategoriaPersonal" class="browser-default">
						<c:forEach items="${lstcategoriapersonal}"
							var="lstcategoriapersonal">

							<form:option path="categorias_personal_id"
								value="${lstcategoriapersonal.categorias_personal_id}">${lstcategoriapersonal.descripcion}</form:option>

						</c:forEach>
					</form:select>

				</div>


			</div>

			<div class="modal-footer col s12">
				<input type="hidden" name="personal_id" id="txtIdPersonal">

				<a id="btn_editar" href=""
					class="modal-close waves-effect waves-green btn blue btn-custom">Guardar</a>
				<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
			</div>

		</form:form>


	</div>





</div>
<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>
<script src="public/js/personal.js"></script>
<script src="public/js/main.js"></script>

<%@include file="/WEB-INF/include/footer.jsp"%>
