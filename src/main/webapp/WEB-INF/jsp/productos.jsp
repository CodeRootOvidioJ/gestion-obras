<%@include file="/WEB-INF/include/header.jsp"%>




<a href="irFormProductos.html"
	class="btn-floating btn-large waves-effect waves-light red"><i
	class="material-icons">add</i></a>


<div class="container">
	<div class="row">

		<h1 class="clase1" align="center">Control de Productos</h1>


		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>productoid</th>
					<th>categoraid</th>
					<th>nombre</th>
					<th>marca</th>
					<th>modelo</th>
					<th>precio</th>
					<th>cantidad</th>
					<th>Modificar</th>
					<th>Eliminar</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lstproductos}" var="lstproductos">
					<tr>
						<td><c:out value="${lstproductos.productoid}" /></td>
						<td><c:out value="${lstproductos.categoraid}" /></td>
						<td><c:out value="${lstproductos.nombre}" /></td>
						<td><c:out value="${lstproductos.marca}" /></td>
						<td><c:out value="${lstproductos.modelo}" /></td>
						<td><c:out value="${lstproductos.precio}" /></td>
						<td><c:out value="${lstproductos.cantidad}" /></td>
						



						<td><a
							href="<c:out value="irFormProductos2.html?id=${lstproductos.productoid}"></c:out>"><span
								class="card-title black"><c:out value="Editar"></c:out></span></a></td>

						<td><a
							href="<c:out value="irEliminarCompras.html?id=${lstcompras.productoid}" />"><span
								class="glyphicon glyphicon-trash" aria-hidden="true"> <c:out
										value="Eliminar"></c:out></span></a></td>










					</tr>

				</c:forEach>
				<label>Browser Select</label>
				<select class="browser-default">
					<option value="lstcompras" disabled selected>Seleccione</option>
					<c:forEach items="${lstproductos}" var="lstproductos">
						<option value="precio"><c:out
								value="${lstproductos.precio}" /></option>
					</c:forEach>
				</select>


			</tbody>
		</table>
	</div>

</div>
<%@include file="/WEB-INF/include/footer.jsp"%>
