<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="col s9  offset-s3 center-align" align="center">


	<h5 class="clase1" align="center">Rubros</h5>

	


	<div class="fixed-action-btn">
		<a class="btn-floating btn-large blue"> <i
			class="large material-icons">add</i>
		</a>
		<ul>
<li><a class="btn-floating btn tooltipped  blue   modal-trigger" href="#modal_agregar_rubro" data-position="LEFT" data-tooltip="Rubros" ><i class="material-icons">toc</i></a></li>
<li><a class="btn-floating btn tooltipped  blue   modal-trigger" href="#modal2" data-position="RIGHT" data-tooltip="Sub Rubros" ><i class="material-icons">insert_chart</i></a></li>
<li><a class="btn-floating btn tooltipped  blue   modal-trigger" href="#modal_agregar_elemento" data-position="TOP" data-tooltip="Elementos" ><i class="material-icons">view_comfy</i></a></li>
	
		</ul>
	</div>




	<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tableRubros">
			<thead class="color-thead">
				<tr>
					<th>Codigo</th>
					<th>Descripcion</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lstrubros}" var="lstrubros">
					<tr>
						<td class="item" data-id="<c:out value="${lstrubros.rubro_id}"/>" ><c:out value="${lstrubros.codigo}"/></td>
						<td><c:out value="${lstrubros.descripcion}"/></td>
						<td><a class="rubro_edit" href="#"
							data-id="<c:out value="${lstrubros.rubro_id}"/>"
							data-codigo="<c:out value="${lstrubros.codigo}"/>"
							data-name="<c:out value="${lstrubros.descripcion}"/>"  > <i
								class="material-icons small icon-custom">edit</i>
						</a> <a class="rubro_del" href="#"
							data-id="<c:out value="${lstrubros.rubro_id}"/>"
							data-name="<c:out value="${lstrubros.descripcion}"/>"  > <i
								class="material-icons small icon-custom red-text">delete</i>
						</a></td>
					</tr>

				</c:forEach>



			</tbody>
		</table>
	</div>



	<div id="modal_actions" class="modal small">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del_rubro" href="" class="btn blue btn-custom">Aceptar</a> <a
				href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>


	<div id="modal_confirm_rubro" class="modal small center-align">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK_rubro" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>


	<!-- Modal Structure agregar -->
	<div id="modal_agregar_rubro" class="modal modal-fixed-footer clasemodal_add_mod ">
		<form:form method="post" commandName="rubros">
		<div class="modal-content">

			

				<h5 class="lighten-5" align="center">Rubros</h5>
			
			
			


					<form:errors path="*" element="div" cssClass="alert alert-danger" />
					<div class="input-field col s12"  >
						<i class="material-icons prefix black-text">dialpad</i>
						<form:input  path="codigo" cssClass="validate black-text"
							required="true" data-length="10" maxlength="10" />
						<form:label path="codigo">codigo:</form:label>
					</div>

					<div class="input-field col s12" >
						<i class="material-icons prefix black-text">turned_in</i>
						<form:label path="descripcion">Descripcion: </form:label>
						<form:input path="descripcion" cssClass="validate black-text"
							required="true" data-length="30" maxlength="30" />
					</div>

					

		

		</div>
<div class="modal-footer col s12"  >
						<input type="submit" value="Guardar" class="btn blue btn-custom" />
						<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
					</div>

				</form:form>
	</div>


<!-- Modal Structure editar -->
	<div id="rubro_edit" class="modal modal-fixed-footer clasemodal_add_mod ">
	<form:form commandName="rubros" id="formedit_rubros">
		<div class="modal-content">

			

				<h5 class="lighten-5" align="center">Rubros</h5>
			
			
				


					<form:errors path="*" element="div" cssClass="alert alert-danger" />

					<div class="input-field col s12" >
						<p>Codigo:</p>
						<form:label path="codigo">
						</form:label>
						<i class="material-icons prefix black-text">dialpad</i>
						<form:input path="codigo" id="txtcodigo"
							cssClass="validate black-text" required="true" data-length="6" maxlength="10" />

					</div>

					<div class="input-field col s12" >
						<p>Descripcion:</p>
						<form:label path="descripcion"></form:label>
						<i class="material-icons prefix black-text">turned_in</i>
						<form:input path="descripcion" id="txtdescripcion"
							cssClass="validate black-text" required="true" data-length="30" maxlength="30" />
					</div>

				

		

		</div>
	<div class="modal-footer col s12"  >
						<input type="hidden" name="rubro_id" id="txtidrubro">
							
							<a id="btn_editar_rubro" href=""
							class="modal-close waves-effect waves-green btn blue btn-custom">Guardar</a>
						<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
							
					</div>
				</form:form>
	</div>

	</div>







<!-- Subrubros  -->
<div class="col s9  offset-s3 center-align" align="center" id="divSubRubros">


	<h5 class="clase1" align="center">Sub Rubros</h5>


	<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tableSubRubros">
			<thead class="color-thead">
				<tr>
					<th>Codigo</th>
					<th>Descripcion</th>
					<th>Rubro</th>
					<th>Desplegable</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lstsubrubros}" var="lstsubrubros">
					<tr>
						<td class="item" data-id="<c:out value="${lstsubrubros.sub_rubro_id}"/>" ><c:out value="${lstsubrubros.codigo}" /></td>
						<td><c:out value="${lstsubrubros.descripcion}" /></td>
						<td><c:out value="${lstsubrubros.rubro_id}"/></td>
						<td><c:out value="${lstsubrubros.desplegable}" /></td>
						<td><a class="zona_edit_subrubro" href="#"
						data-id-sub="<c:out value="${lstsubrubros.sub_rubro_id}"/>"
							data-codigo-sub="<c:out value="${lstsubrubros.codigo}"/>"
							data-name-sub="<c:out value="${lstsubrubros.descripcion}"/>"
							data-desplegable-sub="<c:out value="${lstsubrubros.desplegable}"/>"
							data-rubro-sub="<c:out value="${lstsubrubros.rubro_id}"/>"  >
								<i class="material-icons small icon-custom">edit</i>
						</a> <a class="zona_del_subrubro" href="#"
							data-id="<c:out value="${lstsubrubros.sub_rubro_id}"/>"
							data-name="<c:out value="${lstsubrubros.descripcion}"/>"   > <i
								class="material-icons small icon-custom red-text">delete</i>
						</a></td>
					</tr>

				</c:forEach>

			</tbody>
		</table>
	</div>

	<div id="modal_actions_subrubro" class="modal small">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del_subrubro" href="" class="btn blue btn-custom">Aceptar</a> <a
				href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>


	<div id="modal_confirm_subrubro" class="modal small center-align">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK_subrubro" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>

	


	

</div>


<!-- Modal Structure agregar sub rubro-->
	<div id="modal2" class="modal modal-fixed-footer">
		
		<form:form method="post" commandName="subrubros" action="ListSubRubro.html" >

			<div class="modal-content">

<h5 class="clase1" align="center">Sub Rubros</h5>
<form:errors path="*" element="div" cssClass="alert alert-danger" />
					<div class="input-field col s6">
						<i class="material-icons prefix black-text">dialpad</i>
						<form:input path="codigo" data-length="6" maxlength="10"
							cssClass="validate black-text" required="true" />
						<form:label path="codigo">codigo:</form:label>
					</div>

					<div class="input-field col s6">
						<i class="material-icons prefix black-text">turned_in</i>
						<form:label path="descripcion">Descripcion: </form:label>
						<form:input path="descripcion" cssClass="validate black-text"
							required="true" data-length="30" maxlength="30" />
					</div>

	<div class="col s6 ">
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="rubro_id"> Rubros</form:label>
						<form:select path="rubro_id" class="browser-default">
							<c:forEach items="${lstrubros}" var="lstrubros">

								<form:option path="rubro_id"
									value="${lstrubros.rubro_id}">${lstrubros.descripcion}</form:option>

							</c:forEach>
						</form:select>
					</div>

					<div class="input-field col s6">
						<i class="material-icons prefix black-text">check_box</i>
						<form:label path="desplegable">Desplegable: </form:label>
						<form:input path="desplegable" cssClass="validate black-text"
							required="true" data-length="1" maxlength="1" />
					</div>


			</div>

			<div class="modal-footer">
				<input type="submit" value="Guardar" class="btn btn-danger blue" />
				<a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
			</div>
		</form:form>
	</div>


	<!-- Modal Structure editar  sub rubro-->
	<div id="modal_edit_subrubro" class="modal modal-fixed-footer">
		
	<form:form commandName="subrubros" id="formedit_subrubro">
			<div class="modal-content">
<h5 class="clase1" align="center">Sub Rubros</h5>

				<form:errors path="*" element="div" cssClass="alert alert-danger" />

					<div class="input-field col s6">
						<p>Codigo:</p>
						<i class="material-icons prefix black-text">perm_identity</i>
						<form:label path="codigo">
						</form:label>
						
						<form:input path="codigo" id="txtcodigo_sub"
							cssClass="validate black-text" required="true" data-length="10" maxlength="10"/>
					</div>
					<div class="input-field col s6">
						<p>Descripcion:</p>
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="descripcion"></form:label>
						
						<form:input path="descripcion" id="txtdescripcion_sub"
							cssClass="validate black-text" required="true" data-length="30" maxlength="30" />
					</div>

				


					<div class="col s6">
					<p>Seleccione Rubro:</p>
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="rubro_id" id="txtrubro_sub"></form:label>
						<form:select path="rubro_id" id="cmbrubro" class="browser-default">
							<c:forEach items="${lstrubros}" var="lstrubros">

								<form:option path="rubro_id" value="${lstrubros.rubro_id}">${lstrubros.descripcion}</form:option>

							</c:forEach>
						</form:select>
					</div>


	<div class="input-field col s6">
						<p>Desplegable:</p>
							<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="desplegable"></form:label>
					

						<form:input path="desplegable" id="txtdesplegable_sub"
							cssClass="validate black-text" required="true" data-length="1" maxlength="1" />
					</div>
					
				
			</div>
			<div class="modal-footer">
			<input type="hidden" name="sub_rubro_id" id="txtidsubrubro">
				<a id="btn_editar_subrubro" href="" class="modal-close btn btn-danger blue">Guardar</a>
				<a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
			</div>
		</form:form>
	</div>





<!--  Elementos -->

<div class="col s9  offset-s3 center-align" align="center" id="divElementos">

	<h5 class="clase1" align="center">Elementos</h5>


	<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tableElementos">
			<thead class="color-thead">
				<tr>
					<th>Codigo</th>
					<th>Descripcion</th>
					<th>Sub Rubro</th>
					<th>Unidad</th>
					<th>Costo</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${listElementos}" var="listElementos">
					<tr>
						<td class="item" data-id="<c:out value="${listElementos.elemento_id}"/>" ><c:out value="${listElementos.codigo}" /> </td>
						<td><c:out value="${listElementos.descripcion}" /></td>
						<td><c:out value="${listElementos.subrubro}" /></td>
						<td><c:out value="${listElementos.unidad}" /></td>
						<td><c:out value="${listElementos.costo}" /></td>
						<td><a class="elemento_edit" href="#"
						data-id-ele="<c:out value="${listElementos.elemento_id}" />"
							data-codigo-ele="<c:out value="${listElementos.codigo}"/>"
							data-name-ele="<c:out value="${listElementos.descripcion}"/>"
							data-subrubro-ele="<c:out value="${listElementos.subrubro}"/>"
							data-unidad-ele="<c:out value="${listElementos.unidad}"/>"
							data-costo-ele="<c:out value="${listElementos.costo}"/>"    >
								<i class="material-icons small icon-customE">edit</i>
						</a> <a class="elemento_del" href="#"
							data-id-ele="<c:out value="${listElementos.codigo}"/>"
							data-name-ele="<c:out value="${listElementos.descripcion}"/>"  > <i
								class="material-icons small icon-customE red-text">delete</i>
						</a></td>
					</tr>

				</c:forEach>

			</tbody>
		</table>
	</div>

	<div id="modal_actions_elemento" class="modal small">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del_elemento" href="" class="btn blue btn-custom">Aceptar</a> <a
				href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>

	<div id="modal_confirm_elemento" class="modal small center-align">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK_elemento" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>
	


</div>






<!-- Modal Structure agregar  elementos-->
	<div id="modal_agregar_elemento" class="modal modal-fixed-footer">
	
	<form:form method="post" commandName="elementos"  action="ListElementos.html"  >
	
			<div class="modal-content">
<h5 class="clase1" align="center">Elementos</h5>
<form:errors path="*" element="div" cssClass="alert alert-danger" />
					<div class="input-field col s6">
						<i class="material-icons prefix black-text">perm_identity</i>
						<form:input path="codigo" data-length="4" maxlength="4"
							cssClass="validate black-text" required="true" />
						<form:label path="codigo">codigo:</form:label>
					</div>

					<div class="input-field col s6">
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="descripcion">Descripcion: </form:label>
						<form:input path="descripcion" cssClass="validate black-text"
							required="true" data-length="30" maxlength="30" />
					</div>

					<div class="input-field col s6">
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="unidad">Unidad: </form:label>
						<form:input path="unidad" cssClass="validate black-text"
							required="true" data-length="10" maxlength="10" />
					</div>

				

					<div class="col s6">
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="subrubro">Seleccione Sub-Rubro</form:label>
						<form:select path="subrubro" class="browser-default">
							<c:forEach items="${lstSubrubros}" var="lstSubrubros">

								<form:option path="subrubro" value="${lstSubrubros.sub_rubro_id}">${lstSubrubros.descripcion}</form:option>

							</c:forEach>
						</form:select>
					</div>

					


			</div>

			<div class="modal-footer">
				<input type="submit" value="Guardar" class="btn btn-danger blue" />
				<a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
			</div>
		</form:form>
	</div>
	
	<!-- Modal Structure editar  elemento-->
	<div id="modal_editar_elemento" class="modal modal-fixed-footer">
		
<form:form commandName="elementos" id="formeditelemento">

			<div class="modal-content">
<h5 class="clase1" align="center">Elementos</h5>
<form:errors path="*" element="div" cssClass="alert alert-danger" />

					<div class="input-field col s6">
						<p>Codigo:</p>
						<form:label path="codigo">
						</form:label>
						<i class="material-icons prefix black-text">perm_identity</i>
						<form:input path="codigo" id="txtcodigo-ele"
							cssClass="validate black-text" required="true"  data-length="6" maxlength="10"/>
					</div>

					<div class="input-field col s6">
						<p>Descripcion:</p>
						<form:label path="descripcion"></form:label>
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:input path="descripcion" id="txtdescripcion-ele"
							cssClass="validate black-text" required="true" data-length="30" maxlength="30" />
					</div>
					<div class="input-field col s6">
						<p>Unidad:</p>
						<form:label path="unidad"></form:label>
						<i class="material-icons prefix black-text">lock_outline</i>

						<form:input path="unidad" id="txtunidad-ele"
							cssClass="validate black-text" required="true"  data-length="10" maxlength="10" />
					</div>


					

					<div class="col s10 m6">
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="subrubro" >Seleccione Sub-Rubro</form:label>
						<form:select path="subrubro" id="cmbelemento"  class="browser-default">
							<c:forEach items="${lstSubrubros}" var="lstSubrubros">

								<form:option path="subrubro" value="${lstSubrubros.sub_rubro_id}">${lstSubrubros.descripcion}</form:option>

							</c:forEach>
						</form:select>
					</div>
			</div>
			<div class="modal-footer">
			<input type="hidden" name="elemento_id" id="txtidelemento">
				<a id="btn_editar_elemento" href="" class="modal-close btn btn-danger blue">Guardar</a>
				<a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
			</div>
		</form:form>
	</div>
	







<!-- Footer -->
</div>
<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>
<script src="public/js/rubros.js"></script>
<script src="public/js/subrubros.js"></script>
<script src="public/js/elementos.js"></script>
<%@include file="/WEB-INF/include/footer.jsp"%>
