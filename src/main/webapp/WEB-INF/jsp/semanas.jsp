<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<div class="col s9  offset-s3 center-align" align="center">

	<h5 class="clase1" align="center">Semanas</h5>

	<div class="fixed-action-btn">
		<a class="btn-floating btn-large blue modal-trigger"
			href="#modalAgregarSemanas"> <i class="large material-icons">add</i>
		</a>

	</div>
	<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tableSemanas">
			<thead class="color-thead">
				<tr>
					<th>Codigo</th>
					<th>Periodo</th>
					<th>Fecha Inicio</th>
					<th>Fecha fin</th>
					<th>Estado</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lstsemanas}" var="lstsemanas">
					<tr>
						<td><c:out value="${lstsemanas.codigo}" /></td>
						<td><c:out value="${lstsemanas.periodos_id}" /></td>
						<td><c:out value="${lstsemanas.fecha_inicio}" /></td>
						<td><c:out value="${lstsemanas.fecha_fin}" /></td>
						<td><c:out value="${lstsemanas.nombreestado}" /></td>
						<td><a class="semanas_edit" href="#"
							data-id="<c:out value="${lstsemanas.semanas_id}"/>"
							data-codigo="<c:out value="${lstsemanas.codigo}"/>"
							data-hora-inicio="<c:out value="${lstsemanas.fecha_inicio}"/>"
							data-hora-fin="<c:out value="${lstsemanas.fecha_fin}"/>"
							data-periodos="<c:out value="${lstsemanas.periodos_id}"/>"
							data-estado="<c:out value="${lstsemanas.estado}"/>"> <i
								class="material-icons small icon-custom">edit</i>
						</a> <a class="semanas_del" href="#"
							data-id="<c:out value="${lstsemanas.semanas_id}"/>"
							data-name="<c:out value="${lstsemanas.codigo}"/>"> <i
								class="material-icons small icon-custom red-text">delete</i>
						</a></td>
					</tr>

				</c:forEach>

			</tbody>
		</table>


	</div>


	<div id="modal_actions" class="modal small clasemodal_actionscss ">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del_semanas" href="" class="btn blue btn-custom">Aceptar</a>
			<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>


	<div id="modal_confirm"
		class="modal small center-align clasemodal_confirm_css">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>


	<!-- Modal Structure agregar  semanas -->
	<div id="modalAgregarSemanas" class="modal modal-fixed-footer">
		<form:form method="post" commandName="semana" action="addSemanas.html">
			<div class="modal-content">



				<h5 class="lighten-5" align="center">Periodos</h5>





				<form:errors path="*" element="div" cssClass="alert alert-danger" />




				<div class=" col s6">
					<form:label path="fecha_inicio">Fecha inicia: </form:label>
					<form:input path="fecha_inicio" type="text" class="datepicker"
						required="true" />

				</div>

				<div class=" col s6">
					<form:label path="fecha_fin">Fecha fin: </form:label>
					<form:input path="fecha_fin" type="text" class="datepicker"
						required="true" />
				</div>


				<div class="col s6">
					<form:label path="estado">Seleccione estado</form:label>
					<form:select path="estado" class="browser-default">
						<form:option path="estado" value="1">Abierto</form:option>
						<form:option path="estado" value="0">Cerrado</form:option>
					</form:select>
				</div>

				<div class="col s6">
					<form:label path="periodos_id">Seleccione Periodo</form:label>
					<form:select path="periodos_id" class="browser-default">
						<c:forEach items="${lstperiodos}" var="lstperiodos">

							<form:option path="periodos_id"
								value="${lstperiodos.periodos_id}">${lstperiodos.descripcion}</form:option>
						</c:forEach>
					</form:select>
				</div>

				<div class="input-field col s6">
					<form:label path="codigo">Codigo:</form:label>
					<form:input path="codigo" cssClass="validate black-text"
						required="true" data-length="2" maxlength="2" />
				</div>

			</div>
			<div class="modal-footer col s12">
				<input type="submit" value="Guardar" class="btn blue btn-custom" />
				<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
			</div>
		</form:form>
	</div>




	<!-- Modal Structure editar  semanas-->
	<div id="modalmodificar" class="modal modal-fixed-footer">

		<form:form commandName="semana" id="formedit"
			action="modificarsemanas.html">

			<div class="modal-content">
				<h5 class="clase1" align="center">Semanas</h5>
				<form:errors path="*" element="div" cssClass="alert alert-danger" />

				<div class=" col s6">
					<form:label path="fecha_inicio">Fecha inicia: </form:label>
					<form:input path="fecha_inicio" type="text" id="txtfechainicia"
						class="datepicker" required="true" />
				</div>
				<div class=" col s6">
					<form:label path="fecha_fin">Fecha fin: </form:label>
					<form:input path="fecha_fin" type="text" id="txtfechafin"
						class="datepicker" required="true" />
				</div>
				<div class="col s6">
					<form:label path="estado">Seleccione estado</form:label>
					<form:select path="estado" id="cmbestado" class="browser-default">
						<form:option path="estado" value="1">Abierto</form:option>
						<form:option path="estado" value="0">Cerrado</form:option>
					</form:select>
				</div>

				<div class="col s6">
					<form:label path="periodos_id">Seleccione Periodo</form:label>
					<form:select path="periodos_id" id="cmbperiodo"
						class="browser-default">
						<c:forEach items="${lstperiodos}" var="lstperiodos">
							<form:option path="periodos_id"
								value="${lstperiodos.periodos_id}">${lstperiodos.descripcion}</form:option>
						</c:forEach>
					</form:select>
				</div>

				<div class="col s12" align="center">
					<div class="input-field col s6" align="center">
						<p>Codigo:</p>
						<form:label path="codigo">
						</form:label>
						<form:input path="codigo" id="txtcodigo"
							cssClass="validate black-text" required="true" data-length="2"
							maxlength="2" />
					</div>
				</div>

			</div>

			<div class="modal-footer">
				<input type="hidden" name="semanas_id" id="txtIdSemanas"> <a
					id="btn_editar" href="" class="modal-close btn btn-danger blue">Guardar</a>
				<a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
			</div>
		</form:form>
	</div>


</div>



</div>

<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>
<script src="public/js/semanas.js"></script>
<%@include file="/WEB-INF/include/footer.jsp"%>
