<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<link type="text/css" href="public/css/tipoobras.css" rel="stylesheet">

<div class="col s9  offset-s3 center-align" align="center">

	<h5 class="clase1" align="center">Tipos de obras</h5>




	<div class="fixed-action-btn">
		<a class="btn-floating btn-large blue modal-trigger"
			href="#modalAgregar"> <i class="large material-icons">add</i>
		</a>

	</div>


	<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tableTipoObras">
			<thead class="color-thead">
				<tr>
					<th>Codigo</th>
					<th>Descripcion</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lsttipodeobra}" var="lsttipodeobra">
					<tr>
						<td><c:out value="${lsttipodeobra.codigo}" /></td>
						<td><c:out value="${lsttipodeobra.descripcion}" /></td>

						<td><a class="zona_edit" href="#"
							data-id="<c:out value="${lsttipodeobra.tiposdeobra_id}"/>"
							data-codigo="<c:out value="${lsttipodeobra.codigo}"/>"
							data-name="<c:out value="${lsttipodeobra.descripcion}"/>"  > <i
								class="material-icons small icon-custom">edit</i>
						</a> <a class="zona_del" href="#"
							data-id="<c:out value="${lsttipodeobra.tiposdeobra_id}"/>"
							data-name="<c:out value="${lsttipodeobra.descripcion}"/>" > <i
								class="material-icons small icon-custom red-text">delete</i>
						</a></td>

					</tr>

				</c:forEach>

			</tbody>
		</table>
	</div>


	<div id="modal_actions_tipo_obre" class="modal small clasemodal_actionscss ">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del_tipo_obra" href="" class="btn blue btn-custom">Aceptar</a>
			<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>


	<div id="modal_confirm" class="modal small center-align clasemodal_confirm_css ">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>

	<!-- Modal Structure agregar -->
	<div id="modalAgregar" class="modal modal-fixed-footer clasemodal_add_mod ">
	
		<form:form method="post" commandName="tipodeobra"
					action="agregartiposobras.html">
						<div class="modal-content">
	<div class="col s12">
		

				<h5 class="lighten-5" 
					align="center">Tipos de obras</h5>
		
		
				


					<form:errors path="*" element="div" cssClass="alert alert-danger" />
					<div class="input-field col s12">
						<i class="material-icons prefix black-text">dialpad</i>
						<form:input path="codigo" cssClass="validate black-text"
							required="true" data-length="10" maxlength="10" />
						<form:label path="codigo">codigo:</form:label>
					</div>


					<div class="input-field col s12"  >
						<i class="material-icons prefix black-text">turned_in</i>
						<form:label path="descripcion">Descripcion: </form:label>
						<form:input path="descripcion" cssClass="validate black-text"
							required="true" data-length="30" maxlength="30" />
					</div>

					
				

			</div>
			
					</div>
					<div class="modal-footer col s12" >
						<input type="submit" value="Guardar" class="btn blue btn-custom" />
						<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
					</div>
</form:form>
		

	</div>


	<!-- Modal Structure para editar -->
	<div id="modalmodificar" class="modal modal-fixed-footer clasemodal_add_mod">
	<form:form commandName="tipodeobra" id="formedit"
					action="modificartiposobras.html">
		<div class="modal-content">

			

				<h5 class="lighten-5" 
					align="center">Tipo de obras</h5>

		
		
				


					<form:errors path="*" element="div" cssClass="alert alert-danger" />

					<div class="input-field col s12"  >
						<p>Codigo:</p>
						<form:label path="codigo">
						</form:label>
						<i class="material-icons prefix black-text">dialpad</i>
						<form:input path="codigo" id="txtcodigo"
							cssClass="validate black-text" required="true" data-length="10" maxlength="10" />


					</div>

					<div class="input-field col s12" >
						<p>Descripcion:</p>
						<form:label path="descripcion"></form:label>
						<i class="material-icons prefix black-text">turned_in</i>
						<form:input path="descripcion" id="txtdescripcion"
							cssClass="validate black-text" required="true" data-length="30" maxlength="30"/>
					</div>

					
				
			

		</div>
		<div class="modal-footer col s12" >
						<input type="hidden" name="tiposdeobra_id" id="txtidTipoObra">
							
							<a id="btn_editar" href=""
							class="modal-close waves-effect waves-green btn blue btn-custom">Guardar</a>
						<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
					</div>


</form:form>
</div>
</div>




</div>
<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>
<script src="public/js/tipoobras.js"></script>

<%@include file="/WEB-INF/include/footer.jsp"%>
