<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>



<div class="col s9  offset-s3 center-align" align="center">


	<h5 class="clase1" align="center">Turnos de trabajo</h5>

	
	
	<div class="fixed-action-btn">
		<a class="btn-floating btn-large blue modal-trigger"
			href="#modalAgregarTurnostrabajo"> <i class="large material-icons">add</i>
		</a>

	</div>


		<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tableTurnosTrabajo">
			<thead class="color-thead">
			<tr>
				<th>Codigo</th>
				<th>Descripcion</th>
				<th>Hora Inicio</th>
				<th>Hora fin</th>
				<th>Horas trabajadas</th>
				<th>Accion</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${lstturnostrabajo}" var="lstturnostrabajo">
				<tr>
					<td><c:out value="${lstturnostrabajo.codigo}" /></td>
					<td><c:out value="${lstturnostrabajo.descripcion}" /></td>
					<td><c:out value="${lstturnostrabajo.horainicio}" /></td>
					<td><c:out value="${lstturnostrabajo.horafin}" /></td>
					<td><c:out value="${lstturnostrabajo.horastrabajadas}" /></td>



					<td><a class="turnos_trabajo_edit" href="#"
							data-id="<c:out value="${lstturnostrabajo.turnosdetrabajo_id}"/>"
							data-codigo="<c:out value="${lstturnostrabajo.codigo}"/>"
							data-name="<c:out value="${lstturnostrabajo.descripcion}"/>" 
							data-hora-inicio="<c:out value="${lstturnostrabajo.horainicio}"/>"
							data-hora-fin="<c:out value="${lstturnostrabajo.horafin}"/>"
							data-hora-trabajadas="<c:out value="${lstturnostrabajo.horastrabajadas}"/>"
							>
								<i class="material-icons small icon-custom">edit</i>
						</a> <a class="turnos_trabajo_del" href="#"
							data-id="<c:out value="${lstturnostrabajo.turnosdetrabajo_id}"/>"
							data-name="<c:out value="${lstturnostrabajo.descripcion}"/>" >
								<i class="material-icons small icon-custom red-text">delete</i>
						</a></td>


				</tr>

			</c:forEach>



		</tbody>
	</table>
	
	
	</div>
	
	
	<div id="modal_actions" class="modal small clasemodal_actionscss ">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del_turnos_trabajo" href=""
				class="btn blue btn-custom">Aceptar</a> <a href="#!"
				class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>


	<div id="modal_confirm" class="modal small center-align clasemodal_confirm_css">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>
	
	
	<!-- Modal Structure agregar -->
	<div id="modalAgregarTurnostrabajo" class="modal modal-fixed-footer">
	<form:form method="post" commandName="turnostrabajo" 	action="addTurnosTrabajo.html">
		<div class="modal-content">

			

				<h5 class="lighten-5" 
					align="center">Turnos de trabajo</h5>
			
	
				


					<form:errors path="*" element="div" cssClass="alert alert-danger" />
					<div class="col s12">
					<div class="input-field col s6">
						<form:label path="codigo">Codigo:</form:label>
						<form:input path="codigo" cssClass="validate black-text"
							required="true" data-length="10" maxlength="10" />
					</div>


					<div class="input-field col s6">
						<form:label path="descripcion">Descripcion: </form:label>
						<form:input path="descripcion" cssClass="validate black-text"
							required="true" data-length="30" maxlength="30" />
					</div>
						</div>
							<div class="col s12" >
						
						<div class=" col s6">
						<form:label path="horainicio">Hora inicia: </form:label>
						<form:input path="horainicio" type="text" class="validate black-text" id="timepickerr" required="true" />
				
					</div>
				
					<div class=" col s6" >
						<form:label path="horafin">Hora fin: </form:label>
						<form:input path="horafin" type="text" class="validate black-text" id="timepicker"  required="true" />
					</div>
					</div>
					

				
			</div>
			<div class="modal-footer col s12" >
						<input type="submit" value="Guardar" class="btn blue btn-custom" />
						<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
					</div>
</form:form>
	</div>
	

	

<!-- Modal Structure editar  Turnos de trabajo-->
	<div id="modalmodificar" class="modal modal-fixed-footer">
		
<form:form commandName="turnostrabajo" id="formedit"
			action="modificarturnostrabajo.html">

			<div class="modal-content">
<h5 class="clase1" align="center">Turnos de trabajo</h5>
<form:errors path="*" element="div" cssClass="alert alert-danger" />

					<div class="input-field col s6">
						<p>Codigo:</p>
						<form:label path="codigo">
						</form:label>
						<form:input path="codigo" id="txtcodigo"
							cssClass="validate black-text" required="true"  data-length="10" maxlength="10"/>
					</div>

					<div class="input-field col s6">
						<p>Descripcion:</p>
						<form:label path="descripcion"></form:label>
						<form:input path="descripcion" id="txtdescripcion"
							cssClass="validate black-text" required="true" data-length="30" maxlength="30" />
					</div>
				
<div class="col s12" >
						
						<div class=" col s6">
						<form:label path="horainicio">Hora inicia: </form:label>
						<form:input path="horainicio" type="text"  id="txthorainicia" class="timepickerrr" required="true"  />
				
					</div>
				
					<div class=" col s6" >
						<form:label path="horafin">Hora fin: </form:label>
						<form:input path="horafin" type="text" id="txthorafin" class="timepickerrr" required="true" />
					</div>
					</div>

			</div>
			<div class="modal-footer">
			<input type="hidden" name="turnosdetrabajo_id"
					id="txtIdTurnosTrabajo">
				<a id="btn_editar" href="" class="modal-close btn btn-danger blue">Guardar</a>
				<a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
			</div>
		</form:form>
	</div>


</div>








</div>

<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>




<script src="public/js/turnostrabajo.js"></script>
<%@include file="/WEB-INF/include/footer.jsp"%>
