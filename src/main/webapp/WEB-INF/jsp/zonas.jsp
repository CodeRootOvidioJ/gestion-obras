<%@include file="/WEB-INF/include/header.jsp"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>



<div class="col s9  offset-s3 center-align" align="center">


	<h5 class="clase1" align="center">Zonas</h5>
	
	
	<div class="fixed-action-btn">
		<a class="btn-floating btn-large blue"> <i
			class="large material-icons">add</i>
		</a>
		<ul>
		
				
<li><a class="btn-floating btn tooltipped  blue   modal-trigger" href="#modal2" data-position="RIGHT" data-tooltip="Zonas" ><i class="material-icons">line_weight</i></a></li>
<li><a class="btn-floating btn tooltipped  blue   modal-trigger" href="#modal_agregar_filiales" data-position="TOP" data-tooltip="Filiales" ><i class="material-icons">insert_chart</i></a></li>
	
		</ul>
	</div>
	<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tableZonas">
			<thead class="color-thead">
				<tr>
					<th>Codigo</th>
					<th>Descripcion</th>
					<th>Moneda</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lstzonas}" var="lstzonas">
					<tr>
						<td class="item" data-id=<c:out value="${lstzonas.zonas_id}" />><c:out
								value="${lstzonas.codigo}" /></td>
						<td><c:out value="${lstzonas.descripcion}" /></td>
						<td><c:out value="${lstzonas.moneda}" /></td>
						<td><a class="zona_edit" href="#"
							data-id="<c:out value="${lstzonas.zonas_id}" />"
							data-codigo="<c:out value="${lstzonas.codigo}" />"
							data-name="<c:out value="${lstzonas.descripcion}" />"
							data-moneda="<c:out value="${lstzonas.moneda}" />" > <i
								class="material-icons small icon-custom">edit</i>
						</a> <a class="zona_del" href="#"
							data-id="<c:out value="${lstzonas.zonas_id}" />"
							data-codigo="<c:out value="${lstzonas.codigo}" />"
							data-name="<c:out value="${lstzonas.descripcion}" /> "  > <i
								class="material-icons small icon-custom red-text">delete</i>
						</a></td>
					</tr>

				</c:forEach>



			</tbody>
		</table>
	</div>

	<div id="modal_actions" class="modal small clasemodal_actionscss">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del" href="" class="btn blue btn-custom">Aceptar</a> <a
				href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>


	<div id="modal_confirm" class="modal small center-align clasemodal_confirm_css ">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>


	<!-- Modal Structure -->
	<div id="modal2" class="modal modal-fixed-footer modal_zonas">
	
		<form:form method="post" commandName="zonas">

			<div class="modal-content">



				<form:errors path="*" element="div" cssClass="alert alert-danger" />
					<h6 class="clase1" align="center">Zonas</h6>
				<div class="input-field col s12" >
					<i class="material-icons prefix black-text">dialpad</i>
					<form:input path="codigo" cssClass="validate black-text"
						required="true" data-length="3" maxlength="3" />
					<form:label path="codigo">codigo:</form:label>
				</div>


				<div class="input-field col s12"   >
					<i class="material-icons prefix black-text">turned_in</i>
					<form:label path="descripcion">Descripcion: </form:label>
					<form:input path="descripcion" cssClass="validate black-text"
						required="true" data-length="30" maxlength="30" />
				</div>



				<div class="input-field col s12"  >
					<i class="material-icons prefix black-text">attach_money</i>
					<form:label path="moneda">Moneda: </form:label>
					<form:input path="moneda" cssClass="validate black-text"
						required="true" data-length="3" maxlength="3" />
				</div>


			</div>

			<div class="modal-footer"  style="width: 380px;" >
				<input type="submit" value="Guardar" class="btn btn-danger blue" />
				<a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
			</div>
		</form:form>
	</div>


	<!-- Modal Structure editar -->
	<div id="modal1" class="modal modal-fixed-footer modal_zonas "  align="center"  >
		
		<form:form commandName="zonas" id="formedit"  >
			

				<form:errors path="*" element="div" cssClass="alert alert-danger" />
<h5 class="clase1" align="center">Zonas</h5>
<div class="col s12 " align="center">
				<div class="input-field col s12" >
					<p>Codigo:</p>
					<i class="material-icons prefix black-text">dialpad</i>
					<form:label path="codigo" />
					
					<form:input path="codigo" id="txtcodigo"
						cssClass="validate black-text" required="true" data-length="3"
						maxlength="3" />

				</div>


				<div class="input-field col s12"  >
					<p>Descripcion:</p>
					<i class="material-icons prefix black-text">turned_in</i>
					<form:label path="descripcion"></form:label>
					
					<form:input path="descripcion" id="txtdescripcion"
						cssClass="validate black-text" required="true" data-length="30"
						maxlength="30" />
				</div>



				<div class="input-field col s12" >
					<p>Monedas:</p>
					<i class="material-icons prefix black-text">attach_money</i>
					<form:label path="moneda"></form:label>
					
					
					<form:input path="moneda" id="txtmoneda"
						cssClass="validate black-text" required="true" data-length="3"
						maxlength="3" />
				</div>

		
			<div class="input-field col s12"   >
			<input type="hidden" name="zonas_id" id="txtid">
				<a id="btn_editar" href="" class="modal-close btn btn-danger blue">Guardar</a>
				<a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
			</div>
			
			</div>
		</form:form>
	</div>

</div>


<!-- Filiales -->


<div class="col s9  offset-s3 center-align" align="center"
	id="divFiliales">


	<h5 class="clase1" align="center">Filiales</h5>

	<div class="col s12" style="padding: 10px;">
		<table class="bordered responsive-table z-depth-4 highlight centered"
			id="tableFiliales">
			<thead class="color-thead">
				<tr>
					<th>Codigo</th>
					<th>Descripcion</th>
					<th>Moneda</th>
					<th>Zona</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${lstfiliales}" var="lstfiliales">
					<tr>
						<td><c:out value="${lstfiliales.codigo}" /></td>
						<td><c:out value="${lstfiliales.descripcion}" /></td>
						<td><c:out value="${lstfiliales.moneda}" /></td>
						<td><c:out value="${lstfiliales.descripcion_zonas}" /></td>
						<td><a class="filiales_editt" href="#"
							data-filiales_idt="<c:out value="${lstfiliales.filiales_id}" />"
							data-codigot="<c:out value="${lstfiliales.codigo}" />"
							data-namet="<c:out value="${lstfiliales.descripcion}" />"
							data-zonas_idt="<c:out value="${lstfiliales.zonas_id}" />"
							data-monedat="<c:out value="${lstfiliales.moneda}" /> " > <i
								class="material-icons small icon-custom">edit</i>
						</a> <a class="filiales_del" href="#"
							data-filiales_id="<c:out value="${lstfiliales.filiales_id}" />"
							data-name="<c:out value="${lstfiliales.descripcion}" />" > <i
								class="material-icons small icon-custom red-text">delete</i>
						</a></td>
					</tr>

				</c:forEach>

			</tbody>
		</table>
	</div>

	<div id="modal_actions_filiales" class="modal small clasemodal_actionscss ">
		<div class="modal-content">
			<h5 class="center-align">�Seguro que desea eliminar este
				registro?</h5>
			<p id="text_body"></p>
		</div>

		<div class="modal-footer">
			<a id="btn_del_filiales" href="" class="btn blue btn-custom">Aceptar</a>
			<a href="#!" class="modal-action modal-close btn-flat ">Cancelar</a>
		</div>
	</div>


	<div id="modal_confirm_filiales" class="modal small center-align  clasemodal_confirm_css ">
		<div class="modal-content">
			<h5 class="center-align">Registro modificado con exito</h5>

		</div>

		<div class="modal-footer" style="width: 430px;">
			<a id="btn_confirOK_filiales" href="#!"
				class="btn blue btn-custom center-align" align="center">OK</a>
		</div>
	</div>



</div>


<!-- Modal Structure -->
<div id="modal_agregar_filiales" class="modal modal-fixed-footer "  >
	
	<form:form method="post" commandName="filiales"
		action="irFiliales.html">
		<div class="modal-content">



			<form:errors path="*" element="div" cssClass="alert alert-danger" />
			<h5 class="clase1"  align="center">Filiales</h5>

			<div class="input-field col s6">
				<i class="material-icons prefix black-text">dialpad</i>
				<form:input path="codigo" cssClass="validate black-text"
					required="true" data-length="3" maxlength="3" />
				<form:label path="codigo">codigo:</form:label>
			</div>

			<div class="input-field col s6">
				<i class="material-icons prefix black-text">turned_in</i>
				<form:label path="descripcion">Descripcion: </form:label>
				<form:input path="descripcion" cssClass="validate black-text"
					required="true" data-length="30" maxlength="30" />
			</div>

			<div class="input-field col s6">
				<i class="material-icons prefix black-text">attach_money</i>
				<form:label path="moneda">Moneda: </form:label>
				<form:input path="moneda" cssClass="validate black-text"
					required="true" data-length="3" maxlength="3" />
			</div>

			<div class="col s6">
				<i class="material-icons prefix black-text">lock_outline</i>
				<form:label path="zonas_id">Seleccione zona</form:label>
				<form:select path="zonas_id" class="browser-default">
					<c:forEach items="${lstzonas}" var="lstzonas">

						<form:option path="zonas_id" value="${lstzonas.zonas_id}">${lstzonas.descripcion}</form:option>

					</c:forEach>
				</form:select>
			</div>
</div>
		
		<div class="modal-footer">
			<input type="submit" value="Guardar" class="btn btn-danger blue" />
			<a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
		</div>
	</form:form>
</div>




<!-- Modal Structure editar -->
<div id="modal_editar_filiales" class="modal modal-fixed-footer" >
	
	<form:form commandName="filiales" id="formedit_filiales"  >
	
		<div class="modal-content">



	<form:errors path="*" element="div" cssClass="alert alert-danger" />
<h5 class="clase1"  align="center">Filiales</h5>

					<div class="input-field col s6">
						<p>Codigo:</p>
						<form:label path="codigo">
						</form:label>
						<i class="material-icons prefix black-text">dialpad</i>
						<form:input path="codigo" id="txtcodigofiliales"
							cssClass="validate black-text" required="true" data-length="3"
							maxlength="3" />


					</div>


					<div class="input-field col s6">
						<p>Descripcion:</p>
						<form:label path="descripcion"></form:label>
						<i class="material-icons prefix black-text">turned_in</i>
						<form:input path="descripcion" id="txtdescripcionfiliales"
							cssClass="validate black-text" required="true" data-length="30"
							maxlength="30" />
					</div>



					<div class="input-field col s6">
						<p>Monedas:</p>
						<i class="material-icons prefix black-text">attach_money</i>
						<form:label path="moneda">
						</form:label>
					
						<form:input path="moneda" id="txtmonedafiliales"
							cssClass="validate black-text" required="true" data-length="3"
							maxlength="3" />
					</div>

					<div class="col s6">
					<p>Seleccione zona</p>
						<i class="material-icons prefix black-text">lock_outline</i>
						<form:label path="zonas_id" ></form:label>
						<form:select path="zonas_id"  id="cmbzona"  class="browser-default">
							<c:forEach items="${lstzonas}" var="lstzonas">

								<form:option path="zonas_id" value="${lstzonas.zonas_id}">${lstzonas.descripcion}</form:option>

							</c:forEach>
						</form:select>
		
</div>
		</div>
		<div class="modal-footer">
		<input type="hidden" name="filiales_id" id="txtidfilial">
			<a id="btn_editar_filiales" href=""
							class="modal-close btn btn-danger blue">Guardar</a>
						<a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
		</div>
	</form:form>
</div>





</div>
<script type="text/javascript" src="public/js/jquery-3.3.1.min.js"></script>
<script src="public/js/main.js"></script>
<script src="public/js/zonas.js"></script>
<script src="public/js/filiales.js"></script>
<%@include file="/WEB-INF/include/footer.jsp"%>
