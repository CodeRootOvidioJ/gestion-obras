$(document).ready(function(){
      $('.modal').modal();
      //$('select').material_select();
      
      $('#tablapersona').DataTable();
      
      
      
      $('select').attr("style", "display:initial !important;");
      
      
      $('.datepicker').pickadate({
     	 format: 'dd/mm/yyyy',
     	monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 		monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
 		weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 weekdaysShort: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
 
     	
     	selectMonths: true, // Creates a dropdown to control month
     	selectYears: 15, // Creates a dropdown of 15 years to control year,
     	
     	
     	today: 'Hoy',
 		clear: 'Limpiar',
 close: 'Ok',
     	closeOnSelect: false // Close upon selecting a date,
     	});
     
      
  });

$(document).on('click', '.elemento_del', function(){
    var id = $(this).attr('data-id-ele');
    var name = $(this).attr('data-name');
    console.log("Nombre: " + name);
    console.log("id: " + id);
  //$('#btn_del_elemento').attr('href', "ListRubro.html");
    $('#btn_del_elemento').attr('href', "irEliminarelementos.html?id="+id);
    $("#text_body").text(name);
    $('#modal_actions_elemento').modal('open');
});


$(document).on('click', '.elemento_edit', function(){
    var id = $(this).attr('data-id-ele');
    var codigo = $(this).attr('data-codigo-ele');
    var name = $(this).attr('data-name-ele');
    var subrubro = $(this).attr('data-subrubro-ele');
    var unidad = $(this).attr('data-unidad-ele');
    var costo = $(this).attr('data-costo-ele');
    console.log("Nombre: " + name);
    console.log("id: " + id);
    console.log("subrubro: " + subrubro);
    

    $('#txtcodigo-ele').val(codigo);
    $('#txtdescripcion-ele').val(name);
    $('#txtunidad-ele').val(unidad);
    $('#txtcosto-ele').val(costo);
   // $('#txtsubrubro-ele').val(subrubro);
    $('#txtidelemento').val(id);
    $('#cmbelemento').val(subrubro);
    
    
    
    localStorage.setItem("idSubRubro", subrubro);
    
    
    
    $('#modal_editar_elemento').modal('open');
    
    //editar();
    
});


$('#btn_editar_elemento').click(function(e){
	 e.preventDefault();
	 
	 $.ajax({
			type:"POST",
			url:"irEditarElementos.html",
			data:$("#formeditelemento").serialize(),
			success:function(data){
				console.log(data);
			
				
				if(data==true){
					$('#modal_confirm_elemento').modal('open');
					
				}else{
					$('#modal-error').modal('open');
				}
			},
			error:function(jqxhr,estado,error){
				console.log(estado);
				console.log(error);
			}
		});
	 
});


$('#btn_confirOK_elemento').click(function(){
	
	var idSubRubro = localStorage.getItem("idSubRubro");
	 //location.href="ListRubro.html";
	
	console.log("id sub rubro: " + idSubRubro);
	 getElementos(idSubRubro);
	 $('#modal_confirm_elemento').modal('close');
	 localStorage.removeItem("idSubRubro");
	 
});

$('#btn_error_modificar').click(function() {
	var idSubRubro = localStorage.getItem("idSubRubro");
	 //location.href="ListRubro.html";
	
	console.log("id sub rubro: " + idSubRubro);
	 getElementos(idSubRubro);
	 $('#modal-error').modal('close');
	 localStorage.removeItem("idSubRubro");
	 
});


//function editar(){
	
//}
