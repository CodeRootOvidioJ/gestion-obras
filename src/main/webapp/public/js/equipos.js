$(document).ready(function(){
      $('.modal').modal();
      //$('select').material_select();
      
     
      $('#tableEquipos').DataTable(     {
          "language": {
                  "sProcessing":     "Procesando...",
                  "sLengthMenu":     "Mostrar _MENU_ registros",
                  "sZeroRecords":    "No se encontraron resultados",
                  "sEmptyTable":     "Ningun dato disponible en esta tabla",
                  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                  "sInfoPostFix":    "",
                  "sSearch":         "Buscar:",
                  "sUrl":            "",
                  "sInfoThousands":  ",",
                  "sLoadingRecords": "Cargando...",
                  "oPaginate": {
                      "sFirst":    "Primero",
                      "sLast":     "�ltimo",
                      "sNext":     "Siguiente",
                      "sPrevious": "Anterior"
                  },
                  "oAria": {
                      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                  }
              },
          responsive: true
      });
      
      $('select').attr("style", "display:initial !important;");

					      
      
      $('.datepicker').pickadate({
     	 format: 'dd/mm/yyyy',
     	monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 		monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
 		weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 weekdaysShort: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
 
     	
     	selectMonths: true, // Creates a dropdown to control month
     	selectYears: 15, // Creates a dropdown of 15 years to control year,
     	
     	
     	today: 'Hoy',
 		clear: 'Limpiar',
 close: 'Ok',
     	closeOnSelect: false // Close upon selecting a date,
     	});
     
      
  });



$(document).on('click', '.equipos_del', function(){
    var id = $(this).attr('data-id');
    var name = $(this).attr('data-name');
    console.log("Nombre: " + name);
    console.log("id: " + id);
 // $('#btn_del').attr('href', "irTiposObras.html");
    $('#btn_del_categoria_personal').attr('href', "ireliminarequipos.html?id="+id);
    $("#text_body").text(name);
    $('#modal_actions_categoria_personal').modal('open');
});


$(document).on('click', '.equipos_edit', function(){
    var id = $(this).attr('data-id');
    var codigo = $(this).attr('data-codigo');
    var name = $(this).attr('data-name');
    var unidad = $(this).attr('data-unidad');
    
    var categoriasequipos = $(this).attr('data-categorias_equipos');
    var t1 = $(this).attr('data-t1');
    var pgr1 = $(this).attr('data-pgr1');
    var t2 = $(this).attr('data-t2');
    var pgr2 = $(this).attr('data-pgr2');
    var t3 = $(this).attr('data-t3');
    var pgr3 = $(this).attr('data-pgr3');
    
    
    console.log("Nombre: " + name);
    console.log("id: " + id);
    

    $('#txtcodigo').val(codigo);
    $('#txtnombre').val(name);
    $('#txtt1').val(t1);
    $('#txtpgr1').val(pgr1);
    $('#txtt2').val(t2);
    $('#txtpgr2').val(pgr2);
    $('#txtt3').val(t3);
    $('#txtpgr3').val(pgr3);
    $('#txtunidad').val(unidad);
    $('#cmbcategoriaequipos').val(categoriasequipos);
    $('#txtIdEquipos').val(id);
    
    
    
    
    $('#modalmodificar').modal('open');
    
    //editar();
    
});

$('#btn_editar').click(function(e){
	 e.preventDefault();
	 
	 $.ajax({
			type:"POST",
			url:"modificarequipos.html",
			data:$("#formedit").serialize(),
			success:function(data){
				console.log(data);
				
				
				if(data==true){
					$('#modal_confirm').modal('open');
					
				}else{
					$('#modal-error').modal('open');
				}
				
			},
			error:function(jqxhr,estado,error){
				console.log(estado);
				console.log(error);
			}
		});
	 
});


$('#btn_confirOK').click(function(){
	 location.href="ListEquipos.html";
});

$('#btn_error_modificar').click(function() {
	//location.href = "irZonas.html";
	
	location.href="ListEquipos.html";
	 $('#modal-error').modal('close');
	 
	 
});



//function editar(){
	
//}
