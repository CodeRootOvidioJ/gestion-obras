var tableprevision = null;
$(document)
		.ready(
				function() {
					$('.modal').modal();
					// $('select').material_select();

					$('#tablapersona').DataTable();

					$('#tableObras')
							.DataTable(
									{
										"language" : {
											"sProcessing" : "Procesando...",
											"sLengthMenu" : "Mostrar _MENU_ registros",
											"sZeroRecords" : "No se encontraron resultados",
											"sEmptyTable" : "Ningun dato disponible en esta tabla",
											"sInfo" : "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
											"sInfoEmpty" : "Mostrando registros del 0 al 0 de un total de 0 registros",
											"sInfoFiltered" : "(filtrado de un total de _MAX_ registros)",
											"sInfoPostFix" : "",
											"sSearch" : "Buscar:",
											"sUrl" : "",
											"sInfoThousands" : ",",
											"sLoadingRecords" : "Cargando...",
											"oPaginate" : {
												"sFirst" : "Primero",
												"sLast" : "�ltimo",
												"sNext" : "Siguiente",
												"sPrevious" : "Anterior"
											},
											"oAria" : {
												"sSortAscending" : ": Activar para ordenar la columna de manera ascendente",
												"sSortDescending" : ": Activar para ordenar la columna de manera descendente"
											}
										},
										responsive : true
									});
					
					  tableprevision = $('#tablePrevision').DataTable(     {
				          "language": {
				                  "sProcessing":     "Procesando...",
				                  "sLengthMenu":     "Mostrar _MENU_ registros",
				                  "sZeroRecords":    "No se encontraron resultados",
				                  "sEmptyTable":     "Ningun dato disponible en esta tabla",
				                  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
				                  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
				                  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
				                  "sInfoPostFix":    "",
				                  "sSearch":         "Buscar:",
				                  "sUrl":            "",
				                  "sInfoThousands":  ",",
				                  "sLoadingRecords": "Cargando...",
				                  "oPaginate": {
				                      "sFirst":    "Primero",
				                      "sLast":     "�ltimo",
				                      "sNext":     "Siguiente",
				                      "sPrevious": "Anterior"
				                  },
				                  "oAria": {
				                      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
				                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
				                  }
				              },
				          responsive: true
				      });
				      
					
				      

					$('select').attr("style", "display:initial !important;");

					$('.datepicker').pickadate(
							{
								format : 'dd/mm/yyyy',
								monthsFull : [ 'Enero', 'Febrero', 'Marzo',
										'Abril', 'Mayo', 'Junio', 'Julio',
										'Agosto', 'Septiembre', 'Octubre',
										'Noviembre', 'Diciembre' ],
								monthsShort : [ 'Ene', 'Feb', 'Mar', 'Abr',
										'May', 'Jun', 'Jul', 'Ago', 'Sep',
										'Oct', 'Nov', 'Dic' ],
								weekdaysFull : [ 'Domingo', 'Lunes', 'Martes',
										'Miércoles', 'Jueves', 'Viernes',
										'Sábado' ],
								weekdaysShort : [ 'Domingo', 'Lunes', 'Martes',
										'Miercoles', 'Jueves', 'Viernes',
										'Sabado' ],

								selectMonths : true, // Creates a dropdown to
														// control month
								selectYears : 15, // Creates a dropdown of 15
													// years to control year,

								today : 'Hoy',
								clear : 'Limpiar',
								close : 'Ok',
								closeOnSelect : false
							// Close upon selecting a date,
							});
					
					   $("#divprevision").hide();

				});

$(document).on(
		'click',
		'.obras_del',
		function() {
			var id = $(this).attr('data-id');
			var name = $(this).attr('data-name');
			console.log("Nombre: " + name);
			console.log("id: " + id);
			// $('#btn_del').attr('href', "irTiposObras.html");
			$('#btn_del_obras').attr('href',
					"ireliminarobras.html?id=" + id);
			$("#text_body").text(name);
			$('#modal_actions_obras').modal('open');
		});

$(document).on('click', '.obras_det', function() {
	var id = $(this).attr('data-id');
	var codigo = $(this).attr('data-codigo');
	var name = $(this).attr('data-name');
	var moneda = $(this).attr('data-moneda');
	console.log("Nombre: " + name);
	console.log("id: " + id);

	$('#txtcodigo').val(codigo);
	$('#txtdescripcion').val(name);
	$('#txtIdEstadoObra').val(id);

	$('#modaldetalle').modal('open');

	// editar();

});

$('#btn_editar_estadoobras').click(function(e) {
	e.preventDefault();

	$.ajax({
		type : "POST",
		url : "modificarcaestadodeobras.html",
		data : $("#formedit").serialize(),
		success : function(data) {
			console.log(data);

			if (data == true) {
				$('#modal_confirm').modal('open');

			} else {
				$('#modal-error').modal('open');
			}

		},
		error : function(jqxhr, estado, error) {
			console.log(estado);
			console.log(error);

		}
	});

});

$('#btn_confirOK').click(function() {
	location.href = "listaestadoobra.html";
});

$('#btn_error_modificar').click(function() {

	location.href = "listaestadoobra.html";
	$('#modal-error').modal('close');

});

/* Obteniendo rubro seleccionado */
$(document).on('click', '#tableObras tbody tr', function(){
	var tr = $(this).closest('tr');
	var id = tr.find('.item').data('id');
	$("#tableObras  tbody tr").addClass('tr-no-selected');
	tr.removeClass('tr-no-selected');
	tr.addClass('tr-selected');
	console.log("ID en onselect: "+id);
	getprevision(id);
	
});

function getprevision(id){
	$("#divprevision").show();
	console.log("ID: "+id);
	
	$.ajax({
		url: 'listarprevision.html',
		data: {id:id},
		dataType: 'json',
		method: 'get',
		beforeSend: function(){
		},
		complete: function(){},
		success: function(data){
			tableprevision.clear().draw();
			
			$.each(data, function(indice){
				var obrasprevision_id = data[indice].obrasprevision_id;
				var codigo = data[indice].codigo;
				var periodo = data[indice].periodos_id;
				var rubro = data[indice].rubro_id;
				var sub_rubro = data[indice].sub_rubro_id;
				var elemento = data[indice].elemento_id;
				var obras = data[indice].obras_id;
				var descripcion = data[indice].obras_id;
				console.log("llena la descripcion: "+descripcion);
				var medida = data[indice].medida;
				
				var costosugerido = data[indice].costosugerido;
				var unidades = data[indice].unidades;
				var costo = data[indice].costo;
				var nombrerubro = data[indice].nombrerubro;
				var nombreperiodo = data[indice].nombreperiodo;
				var nombreobra = data[indice].nombreobra;
				
				console.log("llena la obrasprevision_id: "+obrasprevision_id);
			
				
				var acciones = '<a  href="irDetallePrevision.html?id='+obrasprevision_id+'" > <i class="material-icons small icon-custom blue-text">open_in_new</i> '+
						'</a> <a class="obras_prevision_edit item" href="#" '+
						'data-id="'+obrasprevision_id+'" '+
						'data-codigo="'+codigo+'" '+
						'data-periodos="'+periodo+'"'+
						'data-rubro-id="'+rubro+'"'+
						'data-sub-rubro-id="'+sub_rubro+'" '+
						'data-elemento-id="'+elemento+'" '+
						'data-medida="'+medida+'"'+
						'data-costosugerido="'+costosugerido+'"'+
						'data-unidades="'+unidades+'"'+
						'data-costo="'+costo+'"'+
						'data-descripcion="'+descripcion+'">'+
						'<i class="material-icons small icon-custom">edit</i> '+
					'</a> <a class="obrasprevision_del" href="#" '+
						'data-id="'+obrasprevision_id+'" '+
						'data-name="'+descripcion+'"> <i class="material-icons small icon-custom red-text">delete</i> '+
					'</a>';
				
				tableprevision.row.add([codigo, nombreperiodo, nombrerubro,  nombreobra, acciones]).draw();
				
			});
			
		}, 
		error: function(error){
			console.log(error);
		}
	});
}



// function editar(){

// }
