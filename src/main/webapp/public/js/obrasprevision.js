$(document).ready(function(){
	
      $('.modal').modal();
      // $('select').material_select();
      

      
      $('select').attr("style", "display:initial !important;");
      
      $('.datepicker').pickadate({
     	 format: 'dd/mm/yyyy',
     	monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 		monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
 		weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 weekdaysShort: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
 
     	
     	selectMonths: true, // Creates a dropdown to control month
     	selectYears: 15, // Creates a dropdown of 15 years to control year,
     	
     	
     	today: 'Hoy',
 		clear: 'Limpiar',
 close: 'Ok',
     	closeOnSelect: false // Close upon selecting a date,
     	});
     

	   $("#divsubrubro").hide();
	   $("#divelemento").hide();
      
 
  });



    $('#cmbrubro').on('change',function(){
        var rubroID = $(this).val();
     	$("#divsubrubro").show();
    	console.log("rubroID: " + rubroID);
        if(rubroID){
        	$("#divsubrubro").show();
        	console.log("1rubroID: " + rubroID);
        	$.ajax({
        		url: 'listarsubrubros.html',
        		data: {id:rubroID},
        		dataType: 'json',
        		method: 'get',
        		beforeSend: function(){
        		},
        		complete: function(){},
        		success: function(data){
        			$('#cmbsubrubro').html("");
        			
        			   var idd ;
        			data.forEach(function(subrubros, index) {
        			
        				  console.log("subrubros " + index + " | codigo: " + subrubros.codigo + " descripcion: " + subrubros.descripcion)
        				  var option = $('#cmbsubrubro').append('<option value="'+subrubros.sub_rubro_id+'" selected="selected">'+subrubros.codigo+'</option>');
        				         			$('#cmbsubrubro').append('option');
        				         			idd=subrubros.sub_rubro_id;
        				});
        			
        			
        		//	getElementos(idd);
        			
                }
            }); 
        }else{
        }
    });
    
    
    
    $('#cmbsubrubro').on('change',function(){
        var elementoID = $(this).val();
    	console.log("elementoID: " + elementoID);
        if(elementoID){
        	$("#divelemento").show();
        	console.log("1elementoID: " + elementoID);
        	$.ajax({
      
        		url: 'listarelementos.html',
        		data: {id:elementoID},
        		dataType: 'json',
        		method: 'get',
        		beforeSend: function(){
        		},
        		complete: function(){},
        		success: function(data){
        			
        			$('#cmbelemento').html("");
        			data.forEach(function(elementos, index) {
        			
        				  console.log("elementos " + index + " | subrubro: " + elementos.subrubro + " descripcion: " + elementos.descripcion)
        				  var option = $('#cmbelemento').append('<option value='+elementos.elemento_id+' selected="selected">'+elementos.descripcion+'</option>');
        				         			$('#cmbelemento').append('option');
        				});
        			
        			
                	
                }
        	
            }); 
        }else{
        }
    });

    function getRubros(rubro_id,subrubro_id){
    
		
		 	$.ajax({
        		url: 'listarsubrubros.html',
        		data: {id:rubro_id},
        		dataType: 'json',
        		method: 'get',
        		beforeSend: function(){
        		},
        		complete: function(){},
        		success: function(data){
        			$('#cmbsubrubros').html("");
        			var iddd;
        			data.forEach(function(subrubros, index) {
        				
        				 var option = $('#cmbsubrubros').append('<option value="'+subrubros.sub_rubro_id+'" selected="selected">'+subrubros.codigo+'</option>');
        					$('#cmbsubrubros').val(subrubro_id);
		         			$('#cmbsubrubros').append('option');
		         			iddd=subrubros.sub_rubro_id;
		         
		         			
		         		
        				
        				//  console.log("subrubros " + index + " | rubro_id recibido: " + rubro_id + " sub_rubro_id: " + subrubros.sub_rubro_id)
        				
        				});
        			console.log("rubro_id recibido para buscar  para buscar sub rubros : " + rubro_id);
        			//getElementos(iddd);
        		
                }
            }); 
 
    
    }
    
    function getElementos(subrubro_id, elemento_id){
    	$("#divelemento").show();
     	$.ajax({
    		url: 'listarelementos.html',
    		data: {id:subrubro_id},
    		dataType: 'json',
    		method: 'get',
    		beforeSend: function(){
    		},
    		complete: function(){},
    		success: function(data){
    			$('#cmbelementos').html("");
    			data.forEach(function(elementos, index) {
    			
    			//	  console.log("elementos " + index + " | subrubro: " + elementos.subrubro + " descripcion: " + elementos.descripcion)
    				  var option = $('#cmbelementos').append('<option value="'+elementos.elemento_id+'" selected="selected">'+elementos.descripcion+'</option>');
    					$('#cmbelementos').val(elemento_id);
    				         			$('#cmbelementos').append('option');
    				});
    			console.log("subrubro_id recibido para buscar  para buscar elementos : " + subrubro_id);
            }
        }); 


}



    $('#cmbrubros').on('change',function(){
        var rubroID = $(this).val();
    	console.log("rubroID: " + rubroID);
        if(rubroID){
        	console.log("1rubroID: " + rubroID);
        	$.ajax({
        		url: 'listarsubrubrosobrasprevision.html',
        		data: {id:rubroID},
        		dataType: 'json',
        		method: 'get',
        		beforeSend: function(){
        		},
        		complete: function(){},
        		success: function(data){
        			$('#cmbsubrubros').html("");
        			var idddd;
        			data.forEach(function(subrubros, index) {
        			
        				  console.log("subrubros " + index + " | codigo: " + subrubros.codigo + " descripcion: " + subrubros.descripcion)
        				  var option = $('#cmbsubrubros').append('<option value="'+subrubros.sub_rubro_id+'" selected="selected">'+subrubros.codigo+'</option>');
        				         			$('#cmbsubrubros').append('option');
        				         			idddd=subrubros.sub_rubro_id;
        				});
        	var elemento_idd;
        		getElementos(idddd,elemento_idd);
                }
            }); 
        }else{
        }
    });

    $('#cmbsubrubros').on('change',function(){
    	
        var subrubrosID = $(this).val();
    	console.log("subrubrosID: " + subrubrosID);
        if(subrubrosID){
        	console.log("1subrubrosID: " + subrubrosID);
        	$.ajax({
      
        		url: 'listarelementos.html',
        		data: {id:subrubrosID},
        		dataType: 'json',
        		method: 'get',
        		beforeSend: function(){
        		},
        		complete: function(){},
        		success: function(data){
        			
        			$('#cmbelementos').html("");
        			data.forEach(function(elementos, index) {
        			
        				  console.log("elementos " + index + " | subrubro: " + elementos.elemento_id + " descripcion: " + elementos.descripcion)
        				  var option = $('#cmbelementos').append('<option value='+elementos.elemento_id+' selected="selected">'+elementos.descripcion+'</option>');
        				         			$('#cmbelementos').append('option');
        				});
        			
        			
                	
                }
        	
            }); 
        }else{
        }
    });







$(document).on(
		'click',
		'.obrasprevision_del',
		function() {
			var id = $(this).attr('data-id');
			var name = $(this).attr('data-name');
			console.log("Nombre: " + name);
			console.log("id: " + id);
			// $('#btn_del').attr('href', "irTiposObras.html");
			$('#btn_del_semanas').attr('href',
					"ireliminaobrasprevision.html?id=" + id);
			$("#text_body").text(name);
			$('#modal_actions').modal('open');
		});

$(document).on('click', '.obras_prevision_edit', function() {
	
	var id = $(this).attr('data-id');
	var codigo = $(this).attr('data-codigo');
	var descripcion = $(this).attr('data-descripcion');
	var periodos_id = $(this).attr('data-periodos');
	var rubro_id = $(this).attr('data-rubro-id');
	var subrubro_id = $(this).attr('data-sub-rubro-id');
	var elemento_id = $(this).attr('data-elemento-id');
	var medida = $(this).attr('data-medida');
	var costosugerido = $(this).attr('data-costosugerido');
	var unidades = $(this).attr('data-unidades');
	var costo = $(this).attr('data-costo');
	//console.log("periodos_id: " + periodos_id);
	//console.log("id: " + id);

	
	
	
	$('#txtcodigo').val(codigo);
	$('#txtIdobrasprevision').val(id);

	$('#cmbperiodo').val(periodos_id);
	$('#cmbrubros').val(rubro_id);


	
	
	$('#txtmedida').val(medida);
	$('#txtcostosugerido').val(costosugerido);
	$('#txtunidades').val(unidades);
	$('#txtcosto').val(costo);
	$('#txtdescripcion').val(descripcion);
	

	$('#modalmodificar').modal('open');
	
	console.log("rubro_id enviado a buscar sub rubros: " +rubro_id);
	console.log("subrubro_id enviado a buscar elementos: " +subrubro_id);
	
	getRubros(rubro_id,subrubro_id);

	
	getElementos(subrubro_id,elemento_id);
	// editar();

});

$('#btn_editar').click(function(e) {
	e.preventDefault();

	$.ajax({
		type : "POST",
		url : "modificarobrasprevision.html",
		data : $("#formedit").serialize(),
		success : function(data) {
			console.log(data);

			if (data == true) {
				$('#modal_confirm').modal('open');

			} else {
				$('#modal-error').modal('open');
			}

		},
		error : function(jqxhr, estado, error) {
			console.log(estado);
			console.log(error);
		}
	});

});

$('#btn_confirOK').click(function() {
	location.href = "listaobra.html";
});

$('#btn_error_modificar').click(function() {
	// location.href = "irZonas.html";

	location.href = "listaobra.html";
	$('#modal-error').modal('close');

});