var tableSubrubros = null;
var tableElementos = null;

$(document).ready(function(){
      $('.modal').modal();
      $('.tooltipped').tooltip();
      //$('select').material_select();
      
      $('#tablapersona').DataTable();
      
      $('#tableRubros').DataTable(     {
          "language": {
                  "sProcessing":     "Procesando...",
                  "sLengthMenu":     "Mostrar _MENU_ registros",
                  "sZeroRecords":    "No se encontraron resultados",
                  "sEmptyTable":     "Ningun dato disponible en esta tabla",
                  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                  "sInfoPostFix":    "",
                  "sSearch":         "Buscar:",
                  "sUrl":            "",
                  "sInfoThousands":  ",",
                  "sLoadingRecords": "Cargando...",
                  "oPaginate": {
                      "sFirst":    "Primero",
                      "sLast":     "�ltimo",
                      "sNext":     "Siguiente",
                      "sPrevious": "Anterior"
                  },
                  "oAria": {
                      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                  }
              },
          responsive: true
      });
      
      tableSubrubros = $('#tableSubRubros').DataTable(     {
          "language": {
                  "sProcessing":     "Procesando...",
                  "sLengthMenu":     "Mostrar _MENU_ registros",
                  "sZeroRecords":    "No se encontraron resultados",
                  "sEmptyTable":     "Ningun dato disponible en esta tabla",
                  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                  "sInfoPostFix":    "",
                  "sSearch":         "Buscar:",
                  "sUrl":            "",
                  "sInfoThousands":  ",",
                  "sLoadingRecords": "Cargando...",
                  "oPaginate": {
                      "sFirst":    "Primero",
                      "sLast":     "�ltimo",
                      "sNext":     "Siguiente",
                      "sPrevious": "Anterior"
                  },
                  "oAria": {
                      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                  }
              },
          responsive: true
      });
      
      tableElementos = $('#tableElementos').DataTable(     {
          "language": {
                  "sProcessing":     "Procesando...",
                  "sLengthMenu":     "Mostrar _MENU_ registros",
                  "sZeroRecords":    "No se encontraron resultados",
                  "sEmptyTable":     "Ningun dato disponible en esta tabla",
                  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                  "sInfoPostFix":    "",
                  "sSearch":         "Buscar:",
                  "sUrl":            "",
                  "sInfoThousands":  ",",
                  "sLoadingRecords": "Cargando...",
                  "oPaginate": {
                      "sFirst":    "Primero",
                      "sLast":     "�ltimo",
                      "sNext":     "Siguiente",
                      "sPrevious": "Anterior"
                  },
                  "oAria": {
                      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                  }
              },
          responsive: true
      });
      
      $('select').attr("style", "display:initial !important;");
      
      
      $('.datepicker').pickadate({
     	 format: 'dd/mm/yyyy',
     	monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 		monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
 		weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 weekdaysShort: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
 
     	
     	selectMonths: true, // Creates a dropdown to control month
     	selectYears: 15, // Creates a dropdown of 15 years to control year,
     	
     	
     	today: 'Hoy',
 		clear: 'Limpiar',
 close: 'Ok',
     	closeOnSelect: false // Close upon selecting a date,
     	});
     
      
      /* Ocultando div contenedor de subrubros y elementos */
      $("#divSubRubros").hide();
      $("#divElementos").hide();
      
  });

$(document).on('click', '.rubro_del', function(){
    var id = $(this).attr('data-id');
    var name = $(this).attr('data-name');
    console.log("Nombre: " + name);
    console.log("id: " + id);
 // $('#btn_del_rubro').attr('href', "ListRubro.html");
    $('#btn_del_rubro').attr('href', "irEliminarRubros.html?id="+id);
    $("#text_body").text(name);
    $('#modal_actions').modal('open');
});


$(document).on('click', '.rubro_edit', function(){
    var id = $(this).attr('data-id');
    var codigo = $(this).attr('data-codigo');
    var name = $(this).attr('data-name');
    console.log("Nombre: " + name);
    console.log("id: " + id);
    console.log("codigo: " + codigo);

    $('#txtcodigo').val(codigo);
    $('#txtdescripcion').val(name);
    $('#txtidrubro').val(id);

    
    $('#rubro_edit').modal('open');
    
    //editar();
    

});


$('#btn_editar_rubro').click(function(e){
	 e.preventDefault();
	
	 $.ajax({
			type:"POST",
			
			url:"irEditarRubros.html",
			data:$("#formedit_rubros").serialize(),
			success:function(data){
				console.log(data);
				
				
				if(data==true){
					$('#modal_confirm_rubro').modal('open');
					
				}else{
					$('#modal-error').modal('open');
				}
			},
			error:function(jqxhr,estado,error){
				console.log(estado);
				console.log(error);
			}
		});
	 
});

$('#btn_confirOK_rubro').click(function(){
	 location.href="ListRubro.html";
});

$('#btn_error_modificar').click(function() {
	location.href="ListRubro.html";
	 $('#modal-error').modal('close');
	 
	 
});

/* Obteniendo rubro seleccionado */
$(document).on('click', '#tableRubros tbody tr', function(){
	var tr = $(this).closest('tr');
	var id = tr.find('.item').data('id');
	$("#tableRubros tbody tr").addClass('tr-no-selected');
	tr.removeClass('tr-no-selected');
	tr.addClass('tr-selected');
	
	$("#divElementos").hide();
	getSubRubros(id);
	
});

/* Obteniendo subrubro seleccionado */
$(document).on('click', '#tableSubRubros tbody tr', function(){
	var tr = $(this).closest('tr');
	var id = tr.find('.item').data('id-sub');
	
	$("#tableSubRubros tbody tr").addClass('tr-no-selected');
	tr.removeClass('tr-no-selected');
	tr.addClass('tr-selected');
	
	getElementos(id);
	
});

/*
 * Permite obtener los subrubros
 * 
 * @param
 * @id : Id del rubro
 * */
function getSubRubros(id){
	$("#divSubRubros").show();
	console.log("ID: "+id);
	
	$.ajax({
		url: 'listarsubrubros.html',
		data: {id:id},
		dataType: 'json',
		method: 'get',
		beforeSend: function(){
		},
		complete: function(){},
		success: function(data){
			tableSubrubros.clear().draw();
			
			$.each(data, function(indice){
				var codigo = data[indice].codigo;
				var descripcion = data[indice].descripcion;
				var desplegable = data[indice].desplegable;
				var rubro_id = data[indice].rubro_id;
				var sub_rubro_id = data[indice].sub_rubro_id;
				var descrpcion_rubro = data[indice].nombresubrubro;
				
				var acciones = '<a class="zona_edit_subrubro item" href="#" '+
						'data-codigo-sub="'+codigo+'" '+
						'data-id-sub="'+sub_rubro_id+'" '+
						'data-name-sub="'+descripcion+'" '+
						'data-desplegable-sub="'+desplegable+'" '+
						'data-rubro-sub="'+rubro_id+'"> '+
							'<i class="material-icons small icon-custom">edit</i> '+
					'</a> <a class="zona_del_subrubro" href="#" '+
						'data-id="'+sub_rubro_id+'" '+
						'data-name="'+descripcion+'"> <i class="material-icons small icon-custom red-text">delete</i> '+
					'</a>';
				
				tableSubrubros.row.add([codigo, descripcion, descrpcion_rubro, desplegable, acciones]).draw();
				
			});
			
		}, 
		error: function(error){
			console.log(error);
		}
	});
}

/*
 * Permite obtener los elementos
 * 
 * @param
 * @id : Id del subrubro
 * */
function getElementos(id){
	$("#divElementos").show();
	console.log("ID: "+id);
	
	$.ajax({
		url: 'listarelementos.html',
		data: {id:id},
		dataType: 'json',
		method: 'get',
		beforeSend: function(){
		},
		complete: function(){},
		success: function(data){
			tableElementos.clear().draw();
			
			$.each(data, function(indice){
				var elemento_id = data[indice].elemento_id;
				var codigo = data[indice].codigo;
				var descripcion = data[indice].descripcion;
				var unidad = data[indice].unidad;
				var subrubro = data[indice].subrubro;
				var costo = data[indice].costo;
				var nombre_subrubro = data[indice].nombre_subrubro;
				
				
				var acciones = '<a class="elemento_edit" href="#" '+
						'data-id-ele="'+elemento_id+'" '+
						'data-codigo-ele="'+codigo+'" '+
						'data-name-ele="'+descripcion+'" '+
						'data-subrubro-ele="'+subrubro+'" '+
						'data-unidad-ele="'+unidad+'" '+
						'data-costo-ele="'+costo+'"> '+
						'	<i class="material-icons small icon-customE">edit</i> '+
					'</a> <a class="elemento_del" href="#" '+
						'data-id-ele="'+elemento_id+'" '+
						'data-name-ele="'+descripcion+'"> <i class="material-icons small icon-customE red-text">delete</i>'+
					'</a>';
				
				tableElementos.row.add([codigo, descripcion, nombre_subrubro, unidad, costo, acciones]).draw();
				
			});
			
		}, 
		error: function(error){
			
		}
	});
}
