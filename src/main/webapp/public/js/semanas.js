$(document).ready(function(){
	
      $('.modal').modal();
      // $('select').material_select();
      
      
      $('#tableSemanas').DataTable(     {
          "language": {
                  "sProcessing":     "Procesando...",
                  "sLengthMenu":     "Mostrar _MENU_ registros",
                  "sZeroRecords":    "No se encontraron resultados",
                  "sEmptyTable":     "Ningun dato disponible en esta tabla",
                  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                  "sInfoPostFix":    "",
                  "sSearch":         "Buscar:",
                  "sUrl":            "",
                  "sInfoThousands":  ",",
                  "sLoadingRecords": "Cargando...",
                  "oPaginate": {
                      "sFirst":    "Primero",
                      "sLast":     "�ltimo",
                      "sNext":     "Siguiente",
                      "sPrevious": "Anterior"
                  },
                  "oAria": {
                      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                  }
              },
          responsive: true
      });
      
      $('select').attr("style", "display:initial !important;");
      
      $('.datepicker').pickadate({
     	 format: 'dd/mm/yyyy',
     	monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 		monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
 		weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 weekdaysShort: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
 
     	
     	selectMonths: true, // Creates a dropdown to control month
     	selectYears: 15, // Creates a dropdown of 15 years to control year,
     	
     	
     	today: 'Hoy',
 		clear: 'Limpiar',
 close: 'Ok',
     	closeOnSelect: false // Close upon selecting a date,
     	});
     
      
      
      
      $('.numeric').numeric();
      $('.decimal-2-places').numeric({decimalPlaces: 2, decimal: '.'});
      
      $('.alternative-decimal-separator').numeric({altDecimal: '.'});
      
      $('.alternative-decimal-separator-reverse').numeric({altDecimal: '.', decimal: ','});
      
      $('#horast').numeric('.');
  });



$(document).on(
		'click',
		'.semanas_del',
		function() {
			var id = $(this).attr('data-id');
			var name = $(this).attr('data-name');
			console.log("Nombre: " + name);
			console.log("id: " + id);
			// $('#btn_del').attr('href', "irTiposObras.html");
			$('#btn_del_semanas').attr('href',
					"ireliminaSemanas.html?id=" + id);
			$("#text_body").text(name);
			$('#modal_actions').modal('open');
		});

$(document).on('click', '.semanas_edit', function() {
	var id = $(this).attr('data-id');
	var codigo = $(this).attr('data-codigo');
	var periodos_id = $(this).attr('data-periodos');
	var horainicio = $(this).attr('data-hora-inicio');
	var horafin = $(this).attr('data-hora-fin');
	var estado = $(this).attr('data-estado');
	console.log("periodos_id: " + periodos_id);
	console.log("id: " + id);

	$('#txtcodigo').val(codigo);
	$('#txtIdSemanas').val(id);
	$('#txtfechainicia').val(horainicio);
	$('#txtfechafin').val(horafin);
	$('#cmbperiodo').val(periodos_id);
	$('#cmbestado').val(estado);
	
	

	$('#modalmodificar').modal('open');

	// editar();

});

$('#btn_editar').click(function(e) {
	e.preventDefault();

	$.ajax({
		type : "POST",
		url : "modificarsemanas.html",
		data : $("#formedit").serialize(),
		success : function(data) {
			console.log(data);

			if (data == true) {
				$('#modal_confirm').modal('open');

			} else {
				$('#modal-error').modal('open');
			}

		},
		error : function(jqxhr, estado, error) {
			console.log(estado);
			console.log(error);
		}
	});

});

$('#btn_confirOK').click(function() {
	location.href = "ListSemanas.html";
});

$('#btn_error_modificar').click(function() {
	// location.href = "irZonas.html";

	location.href = "ListSemanas.html";
	$('#modal-error').modal('close');

});