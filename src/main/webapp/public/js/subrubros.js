
$(document).ready(function(){
      $('.modal').modal();
      //$('select').material_select();
      
      $('#tablapersona').DataTable();
      
      
      
      $('select').attr("style", "display:initial !important;");
      
      
      $('.datepicker').pickadate({
     	 format: 'dd/mm/yyyy',
     	monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 		monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
 		weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 weekdaysShort: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
 
     	
     	selectMonths: true, // Creates a dropdown to control month
     	selectYears: 15, // Creates a dropdown of 15 years to control year,
     	
     	
     	today: 'Hoy',
 		clear: 'Limpiar',
 close: 'Ok',
     	closeOnSelect: false // Close upon selecting a date,
     	});
     
      
  });

$(document).on('click', '.zona_del_subrubro', function(){
    var id = $(this).attr('data-id');
    var name = $(this).attr('data-name');
    console.log("Nombre: " + name);
    console.log("id: " + id);
 // $('#btn_del_subrubro').attr('href', "ListRubro.html");
    $('#btn_del_subrubro').attr('href', "irEliminarSubRubros.html?id="+id);
    $("#text_body").text(name);
    $('#modal_actions_subrubro').modal('open');
});


$(document).on('click', '.zona_edit_subrubro', function(){
    var id = $(this).attr('data-id-sub');
    var codigo = $(this).attr('data-codigo-sub');
    var name = $(this).attr('data-name-sub');
    var desplegable = $(this).attr('data-desplegable-sub');
    var rubro = $(this).attr('data-rubro-sub');
    
    console.log("Nombre: " + name);
    console.log("id: " + id);
    console.log("rubro: " + rubro);
    
    
 // $('#btn_editar').attr('href', "irZonas.html");
  
 
 // $.post("irEditarObras" ,  {codigo: "178", descripcion: "Focus", moneda: "rojo"}, function(htmlexterno),dataType : "json")
  
   // $('#btn_del').attr('href', "irEliminarZonas.html?id="+id);

    $('#txtcodigo_sub').val(codigo);
    $('#txtdescripcion_sub').val(name);
    $('#txtdesplegable_sub').val(desplegable);
    $('#cmbrubro').val(rubro);
    $('#txtidsubrubro').val(id);

   
    
    $('#modal_edit_subrubro').modal('open');
    
    //editar();
    
});



$('#btn_editar_subrubro').click(function(e){
	 e.preventDefault();
	 
	 $.ajax({
			type:"POST",
			url:"irEditarSubRubros.html",
			data:$("#formedit_subrubro").serialize(),
			success:function(data){
				console.log(data);
			
				
				if(data==true){
					$('#modal_confirm_subrubro').modal('open');
					
				}else{
					$('#modal-error').modal('open');
				}
			},
			error:function(jqxhr,estado,error){
				console.log(estado);
				console.log(error);
			}
		});
	 
});



$('#btn_confirOK_subrubro').click(function(){
	// getSubRubros(id);
	// $('#modal_confirm_elemento').modal('close');
	 location.href="ListRubro.html";
});

$('#btn_error_modificar').click(function() {
	location.href="ListRubro.html";
	 $('#modal-error').modal('close');
	 
	 
});

//function editar(){
	
//}
