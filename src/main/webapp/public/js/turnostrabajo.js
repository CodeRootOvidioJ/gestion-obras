$(document).ready(function(){
	
      $('.modal').modal();
      // $('select').material_select();
      
      $('#tablapersona').DataTable();
      
      $('#tableTurnosTrabajo').DataTable(     {
          "language": {
                  "sProcessing":     "Procesando...",
                  "sLengthMenu":     "Mostrar _MENU_ registros",
                  "sZeroRecords":    "No se encontraron resultados",
                  "sEmptyTable":     "Ningun dato disponible en esta tabla",
                  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                  "sInfoPostFix":    "",
                  "sSearch":         "Buscar:",
                  "sUrl":            "",
                  "sInfoThousands":  ",",
                  "sLoadingRecords": "Cargando...",
                  "oPaginate": {
                      "sFirst":    "Primero",
                      "sLast":     "�ltimo",
                      "sNext":     "Siguiente",
                      "sPrevious": "Anterior"
                  },
                  "oAria": {
                      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                  }
              },
          responsive: true
      });
      
      $('select').attr("style", "display:initial !important;");
      
      
      
      
      
      $('.datepicker').pickadate({
     	 format: 'dd/mm/yyyy',
     	monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 		monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
 		weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 weekdaysShort: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
 
     	
     	selectMonths: true, // Creates a dropdown to control month
     	selectYears: 15, // Creates a dropdown of 15 years to control year,
     	
     	
     	today: 'Hoy',
 		clear: 'Limpiar',
 close: 'Ok',
     	closeOnSelect: false // Close upon selecting a date,
     	});
     
      
      
      $('#timepickerr').pickatime({
      default: 'now', // Set default time: 'now', '1:30AM', '16:30'
      fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
      twelvehour: false, // Use AM/PM or 24-hour format
      donetext: 'OK', // text for done-button
      cleartext: 'Clear', // text for clear-button
      canceltext: 'Cancel', // Text for cancel-button,
      container: undefined, // ex. 'body' will append picker to body
      autoclose: false, // automatic close timepicker
      ampmclickable: true, // make AM PM clickable
      aftershow: function(){} //Function for after opening timepicker
    });
      
      $('#timepicker').pickatime({
      default: 'now', // Set default time: 'now', '1:30AM', '16:30'
      fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
      twelvehour: false, // Use AM/PM or 24-hour format
      donetext: 'OK', // text for done-button
      cleartext: 'Clear', // text for clear-button
      canceltext: 'Cancel', // Text for cancel-button,
      container: undefined, // ex. 'body' will append picker to body
      autoclose: false, // automatic close timepicker
      ampmclickable: true, // make AM PM clickable
      aftershow: function(){} //Function for after opening timepicker
    });
      
      $('.timepickerrr').pickatime({
      default: 'now', // Set default time: 'now', '1:30AM', '16:30'
      fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
      twelvehour: false, // Use AM/PM or 24-hour format
      donetext: 'OK', // text for done-button
      cleartext: 'Clear', // text for clear-button
      canceltext: 'Cancel', // Text for cancel-button,
      container: undefined, // ex. 'body' will append picker to body
      autoclose: false, // automatic close timepicker
      ampmclickable: true, // make AM PM clickable
      aftershow: function(){} //Function for after opening timepicker
    });
      
      
      $('.numeric').numeric();
      
      $('.decimal-2-texbox').numeric({decimalPlaces: 2, decimal: '.'});
      
      $('.alternative-decimal-separator').numeric({altDecimal: '.'});
      
      $('.alternative-decimal-separator-reverse').numeric({altDecimal: '.', decimal: ','});
      
      $('.numeric-punto').numeric('.');
  });



$(document).on(
		'click',
		'.turnos_trabajo_del',
		function() {
			var id = $(this).attr('data-id');
			var name = $(this).attr('data-name');
			console.log("Nombre: " + name);
			console.log("id: " + id);
			// $('#btn_del').attr('href', "irTiposObras.html");
			$('#btn_del_turnos_trabajo').attr('href',
					"ireliminarturnostrabajo.html?id=" + id);
			$("#text_body").text(name);
			$('#modal_actions').modal('open');
		});

$(document).on('click', '.turnos_trabajo_edit', function() {
	var id = $(this).attr('data-id');
	var codigo = $(this).attr('data-codigo');
	var name = $(this).attr('data-name');
	var horainicio = $(this).attr('data-hora-inicio');
	var horafin = $(this).attr('data-hora-fin');
	var horatrabajadas = $(this).attr('data-hora-trabajadas');
	console.log("Nombre: " + name);
	console.log("id: " + id);

	$('#txtcodigo').val(codigo);
	$('#txtdescripcion').val(name);
	$('#txtIdTurnosTrabajo').val(id);
	$('#txthorainicia').val(horainicio);
	$('#txthorafin').val(horafin);
	$('#txthoratrabajadas').val(horatrabajadas);
	
	

	$('#modalmodificar').modal('open');

	// editar();

});

$('#btn_editar').click(function(e) {
	e.preventDefault();

	$.ajax({
		type : "POST",
		url : "modificarturnostrabajo.html",
		data : $("#formedit").serialize(),
		success : function(data) {
			console.log(data);

			if (data == true) {
				$('#modal_confirm').modal('open');

			} else {
				$('#modal-error').modal('open');
			}

		},
		error : function(jqxhr, estado, error) {
			console.log(estado);
			console.log(error);
		}
	});

});

$('#btn_confirOK').click(function() {
	location.href = "ListTurnosTrabajo.html";
});

$('#btn_error_modificar').click(function() {
	// location.href = "irZonas.html";

	location.href = "ListTurnosTrabajo.html";
	$('#modal-error').modal('close');

});