
var tableZonas = null;

$(document).ready(function(){
      $('.modal').modal();
      //$('select').material_select();
      
      $('#tablapersona').DataTable();
      
      tableZonas = $('#tableZonas').DataTable(     {
          "language": {
                  "sProcessing":     "Procesando...",
                  "sLengthMenu":     "Mostrar _MENU_ registros",
                  "sZeroRecords":    "No se encontraron resultados",
                  "sEmptyTable":     "Ningun dato disponible en esta tabla",
                  "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                  "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                  "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                  "sInfoPostFix":    "",
                  "sSearch":         "Buscar:",
                  "sUrl":            "",
                  "sInfoThousands":  ",",
                  "sLoadingRecords": "Cargando...",
                  "oPaginate": {
                      "sFirst":    "Primero",
                      "sLast":     "Ultimo",
                      "sNext":     "Siguiente",
                      "sPrevious": "Anterior"
                  },
                  "oAria": {
                      "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                      "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                  }
              },
          responsive: true
      });
      
      $('select').attr("style", "display:initial !important;");
      
      
      $('.datepicker').pickadate({
     	 format: 'dd/mm/yyyy',
     	monthsFull: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
 		monthsShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
 		weekdaysFull: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
 weekdaysShort: ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
 
     	
     	selectMonths: true, // Creates a dropdown to control month
     	selectYears: 15, // Creates a dropdown of 15 years to control year,
     	
     	
     	today: 'Hoy',
 		clear: 'Limpiar',
 close: 'Ok',
     	closeOnSelect: false // Close upon selecting a date,
     	});
     
      $("#divFiliales").hide();
      
  });

$(document).on('click', '.zona_del', function(){
	var id = $(this).attr('data-id');
    var codigo = $(this).attr('data-codigo');
    var name = $(this).attr('data-name');
    console.log("Nombre: " + name);
    console.log("id: " + id);
  //$('#btn_del').attr('href', "irZonas.html");
   $('#btn_del').attr('href', "irEliminarZonas.html?id="+id);
    $("#text_body").text(name);
    $('#modal_actions').modal('open');
});


$(document).on('click', '.zona_edit', function(){
    var id = $(this).attr('data-id');
    var codigo = $(this).attr('data-codigo');
    var name = $(this).attr('data-name');
    var moneda = $(this).attr('data-moneda');
    console.log("Nombre: " + name);
    console.log("id: " + id);

    $('#txtcodigo').val(codigo);
    $('#txtdescripcion').val(name);
    $('#txtmoneda').val(moneda);
    $('#txtid').val(id);
    

    $('#modal1').modal('open');
    
    //editar();
    
});

$('#btn_editar').click(function(e){
	 
	 e.preventDefault();
	 $.ajax({
			type:"POST",
			
			url:"irEditarzonas.html",
			data:$("#formedit").serialize(),
			success:function(data){
				console.log(data);
				
				
				if(data==true){
					$('#modal_confirm').modal('open');
					
				}else{
					$('#modal-error').modal('open');
				}
				
			},
			error:function(jqxhr,estado,error){
				console.log(estado);
				console.log(error);
			}
		});
	 
});


$('#btn_confirOK').click(function(){
	 location.href="irZonas.html";
});

$('#btn_error_modificar').click(function() {
	//location.href = "irZonas.html";
	location.href="irZonas.html";
	 $('#modal-error').modal('close');
	 
	 
});

/* Obteniendo zona seleccionado */
$(document).on('click', '#tableZonas tbody tr', function(){
	var tr = $(this).closest('tr');
	var id = tr.find('.item').data('id');
	
	$("#tableZonas tbody tr").addClass('tr-no-selected');
	tr.removeClass('tr-no-selected');
	tr.addClass('tr-selected');
	
	getFiliales(id);
	
});

/*
 * Obteniendo filiales
 * */
function getFiliales(id){
	$("#divFiliales").show();
	console.log("ID: "+id);
	
	//console.log("descripcion_zonas: "+descripcion_zonas);
	
	$.ajax({
		url: 'listarfiliales.html',
		data: {id:id},
		dataType: 'json',
		method: 'get',
		beforeSend: function(){
		},
		complete: function(){},
		success: function(data){

			tableFiliales.clear().draw();
			
			$.each(data, function(indice){
				var filiales_id = data[indice].filiales_id;
				var codigo = data[indice].codigo;
				var descripcion = data[indice].descripcion;
				var moneda = data[indice].moneda;
				var zona = data[indice].zonas_id;
				var descripcionzonas = data[indice].descripcion_zonas;
				console.log("descripcionzonas: "+descripcionzonas);
				
				var acciones = '<a class="filiales_edit" href="#" '+
				'data-filiales_id="'+filiales_id+'" '+
						'data-codigo="'+codigo+'" '+
						'data-name="'+descripcion+'" '+
						'data-zonas_id="'+zona+'" '+
						'data-moneda="'+moneda+'"> '+ 
						'<i class="material-icons small icon-custom">edit</i> '+
					'</a> '+
					'<a class="filiales_del" href="#" '+
						'data-filiales_id="'+filiales_id+'" '+
						'data-name="'+descripcion+'"> <i class="material-icons small icon-custom red-text">delete</i>'+
					'</a>';
				
				tableFiliales.row.add([codigo, descripcion, moneda, descripcionzonas, acciones]).draw();
				
			});
			
		}, 
		error: function(error){
			console.log(error);
		}
	});
}
